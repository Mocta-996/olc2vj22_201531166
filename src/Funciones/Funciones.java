package Funciones;

import Entorno.Environment;
import Entorno.Symbol;
import grammar.JGrammarParser;

import java.util.ArrayList;

public class Funciones {
    public String nombre;
    public ArrayList<Symbol> parametros;
    public Object lista_instrucciones;
    public Environment entorno;
    // lista de parametros a declarar
    public JGrammarParser.DeclararParametrosContext lista_parametros;
    public Funciones(String nombre, ArrayList<Symbol> lparametros, Object linstrucciones, JGrammarParser.DeclararParametrosContext lista_parametros,Environment entorno) {
        this.nombre = nombre;
        this.parametros = lparametros;
        this.lista_instrucciones = linstrucciones;
        this.lista_parametros = lista_parametros;
        this.entorno = entorno;
    }

    public Environment getEntorno() {
        return entorno;
    }

    public void setEntorno(Environment entorno) {
        this.entorno = entorno;
    }
}
