// Generated from C:/Users/Pilo Tuy/Desktop/Compiladores 2/olc2vj22_201531166/src/Main\JGrammar.g4 by ANTLR 4.10.1
package grammar;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link JGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface JGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(JGrammarParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#inicioFuncion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInicioFuncion(JGrammarParser.InicioFuncionContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#inicioSubRutina}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInicioSubRutina(JGrammarParser.InicioSubRutinaContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#declararParametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclararParametros(JGrammarParser.DeclararParametrosContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#parametros}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParametros(JGrammarParser.ParametrosContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#callSubrutina}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallSubrutina(JGrammarParser.CallSubrutinaContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#bloqueFunciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueFunciones(JGrammarParser.BloqueFuncionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#bloqueFunciones2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueFunciones2(JGrammarParser.BloqueFunciones2Context ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#comentario}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComentario(JGrammarParser.ComentarioContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#linstrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLinstrucciones(JGrammarParser.LinstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstrucciones(JGrammarParser.InstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion(JGrammarParser.DeclaracionContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaDeclarada}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeclarada(JGrammarParser.ListaDeclaradaContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#variables}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariables(JGrammarParser.VariablesContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#asignacionvar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacionvar(JGrammarParser.AsignacionvarContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(JGrammarParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaprint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaprint(JGrammarParser.ListaprintContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#arreglosestaticos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglosestaticos(JGrammarParser.ArreglosestaticosContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#idarrays}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdarrays(JGrammarParser.IdarraysContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaArraSimple}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaArraSimple(JGrammarParser.ListaArraSimpleContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#dim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim(JGrammarParser.DimContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#arreglosDinamicos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArreglosDinamicos(JGrammarParser.ArreglosDinamicosContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#idArrayDin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdArrayDin(JGrammarParser.IdArrayDinContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaArraBid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaArraBid(JGrammarParser.ListaArraBidContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#dimBid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDimBid(JGrammarParser.DimBidContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#asignarArrayUni}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignarArrayUni(JGrammarParser.AsignarArrayUniContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaExpr(JGrammarParser.ListaExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#asignarArrayBid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignarArrayBid(JGrammarParser.AsignarArrayBidContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#funAllocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunAllocate(JGrammarParser.FunAllocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#funDeAllocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunDeAllocate(JGrammarParser.FunDeAllocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaDeallocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaDeallocate(JGrammarParser.ListaDeallocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#sentenciaIf}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentenciaIf(JGrammarParser.SentenciaIfContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#bloqueElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueElse(JGrammarParser.BloqueElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaElse(JGrammarParser.ListaElseContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#cicloDo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCicloDo(JGrammarParser.CicloDoContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#listaCondiciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaCondiciones(JGrammarParser.ListaCondicionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#cicloDoWhile}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCicloDoWhile(JGrammarParser.CicloDoWhileContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo(JGrammarParser.TipoContext ctx);
	/**
	 * Visit a parse tree produced by {@link JGrammarParser#controlCiclos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitControlCiclos(JGrammarParser.ControlCiclosContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opAritmetica}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpAritmetica(JGrammarParser.OpAritmeticaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logico}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogico(JGrammarParser.LogicoContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrBiExp}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrBiExp(JGrammarParser.ArrBiExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cad}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCad(JGrammarParser.CadContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callFunction}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallFunction(JGrammarParser.CallFunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code integer}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(JGrammarParser.IntegerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrUniExp}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrUniExp(JGrammarParser.ArrUniExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code float}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloat(JGrammarParser.FloatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(JGrammarParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varExp}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarExp(JGrammarParser.VarExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code complex}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplex(JGrammarParser.ComplexContext ctx);
	/**
	 * Visit a parse tree produced by the {@code char}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChar(JGrammarParser.CharContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opRelacional}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpRelacional(JGrammarParser.OpRelacionalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opSize}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpSize(JGrammarParser.OpSizeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opLogica}
	 * labeled alternative in {@link JGrammarParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpLogica(JGrammarParser.OpLogicaContext ctx);
}