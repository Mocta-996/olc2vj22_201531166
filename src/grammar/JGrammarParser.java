// Generated from C:/Users/Pilo Tuy/Desktop/Compiladores 2/olc2vj22_201531166/src/Main\JGrammar.g4 by ANTLR 4.10.1
package grammar;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, PUNTO=2, PTCOMA=3, PARIZQ=4, PARDER=5, LLAVEIZQ=6, LLAVEDER=7, 
		COMA=8, CORIZQ=9, CORDER=10, KLEENE=11, IGUAL=12, DOSPUNTOS=13, EXCLA=14, 
		PROGRAM=15, END=16, IMPLICIT=17, NONE=18, PRINT=19, TRUE=20, FALSE=21, 
		DIMENSION=22, SIZE=23, ALLOCATABLE=24, ALLOCATE=25, DEALLOCATE=26, IF=27, 
		THEN=28, ELSE=29, DO=30, WHILE=31, EXIT=32, CYCLE=33, RINTEGER=34, RREAL=35, 
		RLOGICAL=36, RCOMPLEX=37, RCHARACTER=38, AND=39, OR=40, NOT=41, IGUALACION=42, 
		DIFERENCIACION=43, MENORQUE=44, MAYORQUE=45, MENORIGUAL=46, MAYORIGUAL=47, 
		POTENCIA=48, MULTIPLICACION=49, DIVISION=50, SUMA=51, RESTA=52, SUBRUTINE=53, 
		INTENT=54, IN=55, CALL=56, FUNCTION=57, RESULT=58, INTEGER=59, FLOAT=60, 
		COMPLEX=61, CHARACTER=62, ID=63, CADENA=64, WHITESPACE=65, COMENTARIO_LINEA=66;
	public static final int
		RULE_start = 0, RULE_inicioFuncion = 1, RULE_inicioSubRutina = 2, RULE_declararParametros = 3, 
		RULE_parametros = 4, RULE_callSubrutina = 5, RULE_bloqueFunciones = 6, 
		RULE_bloqueFunciones2 = 7, RULE_comentario = 8, RULE_linstrucciones = 9, 
		RULE_instrucciones = 10, RULE_declaracion = 11, RULE_listaDeclarada = 12, 
		RULE_variables = 13, RULE_asignacionvar = 14, RULE_print = 15, RULE_listaprint = 16, 
		RULE_arreglosestaticos = 17, RULE_idarrays = 18, RULE_listaArraSimple = 19, 
		RULE_dim = 20, RULE_arreglosDinamicos = 21, RULE_idArrayDin = 22, RULE_listaArraBid = 23, 
		RULE_dimBid = 24, RULE_asignarArrayUni = 25, RULE_listaExpr = 26, RULE_asignarArrayBid = 27, 
		RULE_funAllocate = 28, RULE_funDeAllocate = 29, RULE_listaDeallocate = 30, 
		RULE_sentenciaIf = 31, RULE_bloqueElse = 32, RULE_listaElse = 33, RULE_cicloDo = 34, 
		RULE_listaCondiciones = 35, RULE_cicloDoWhile = 36, RULE_tipo = 37, RULE_controlCiclos = 38, 
		RULE_expresion = 39;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "inicioFuncion", "inicioSubRutina", "declararParametros", "parametros", 
			"callSubrutina", "bloqueFunciones", "bloqueFunciones2", "comentario", 
			"linstrucciones", "instrucciones", "declaracion", "listaDeclarada", "variables", 
			"asignacionvar", "print", "listaprint", "arreglosestaticos", "idarrays", 
			"listaArraSimple", "dim", "arreglosDinamicos", "idArrayDin", "listaArraBid", 
			"dimBid", "asignarArrayUni", "listaExpr", "asignarArrayBid", "funAllocate", 
			"funDeAllocate", "listaDeallocate", "sentenciaIf", "bloqueElse", "listaElse", 
			"cicloDo", "listaCondiciones", "cicloDoWhile", "tipo", "controlCiclos", 
			"expresion"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'::'", "'.'", "';'", "'('", "')'", "'{'", "'}'", "','", "'['", 
			"']'", "'?'", "'='", "':'", "'!'", "'program'", "'end'", "'implicit'", 
			"'none'", "'print'", "'.true.'", "'.false.'", "'dimension'", "'size'", 
			"'allocatable'", "'allocate'", "'deallocate'", "'if'", "'then'", "'else'", 
			"'do'", "'while'", "'exit'", "'cycle'", "'integer'", "'real'", "'logical'", 
			"'complex'", "'character'", "'.and.'", "'.or.'", "'.not.'", null, null, 
			null, null, null, null, "'**'", "'*'", "'/'", "'+'", "'-'", "'subroutine'", 
			"'intent'", "'in'", "'call'", "'function'", "'result'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, "PUNTO", "PTCOMA", "PARIZQ", "PARDER", "LLAVEIZQ", "LLAVEDER", 
			"COMA", "CORIZQ", "CORDER", "KLEENE", "IGUAL", "DOSPUNTOS", "EXCLA", 
			"PROGRAM", "END", "IMPLICIT", "NONE", "PRINT", "TRUE", "FALSE", "DIMENSION", 
			"SIZE", "ALLOCATABLE", "ALLOCATE", "DEALLOCATE", "IF", "THEN", "ELSE", 
			"DO", "WHILE", "EXIT", "CYCLE", "RINTEGER", "RREAL", "RLOGICAL", "RCOMPLEX", 
			"RCHARACTER", "AND", "OR", "NOT", "IGUALACION", "DIFERENCIACION", "MENORQUE", 
			"MAYORQUE", "MENORIGUAL", "MAYORIGUAL", "POTENCIA", "MULTIPLICACION", 
			"DIVISION", "SUMA", "RESTA", "SUBRUTINE", "INTENT", "IN", "CALL", "FUNCTION", 
			"RESULT", "INTEGER", "FLOAT", "COMPLEX", "CHARACTER", "ID", "CADENA", 
			"WHITESPACE", "COMENTARIO_LINEA"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "JGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public Token init;
		public Token end;
		public BloqueFuncionesContext bloqueFunciones() {
			return getRuleContext(BloqueFuncionesContext.class,0);
		}
		public BloqueFunciones2Context bloqueFunciones2() {
			return getRuleContext(BloqueFunciones2Context.class,0);
		}
		public List<TerminalNode> PROGRAM() { return getTokens(JGrammarParser.PROGRAM); }
		public TerminalNode PROGRAM(int i) {
			return getToken(JGrammarParser.PROGRAM, i);
		}
		public TerminalNode IMPLICIT() { return getToken(JGrammarParser.IMPLICIT, 0); }
		public TerminalNode NONE() { return getToken(JGrammarParser.NONE, 0); }
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public TerminalNode EOF() { return getToken(JGrammarParser.EOF, 0); }
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public List<ComentarioContext> comentario() {
			return getRuleContexts(ComentarioContext.class);
		}
		public ComentarioContext comentario(int i) {
			return getRuleContext(ComentarioContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		try {
			setState(104);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				bloqueFunciones();
				setState(81);
				bloqueFunciones2();
				setState(82);
				match(PROGRAM);
				setState(83);
				((StartContext)_localctx).init = match(ID);
				setState(84);
				match(IMPLICIT);
				setState(85);
				match(NONE);
				setState(86);
				linstrucciones();
				setState(87);
				match(END);
				setState(88);
				match(PROGRAM);
				setState(89);
				((StartContext)_localctx).end = match(ID);
				setState(90);
				match(EOF);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(92);
				comentario();
				setState(93);
				bloqueFunciones();
				setState(94);
				bloqueFunciones2();
				setState(95);
				match(PROGRAM);
				setState(96);
				((StartContext)_localctx).init = match(ID);
				setState(97);
				linstrucciones();
				setState(98);
				match(END);
				setState(99);
				match(PROGRAM);
				setState(100);
				((StartContext)_localctx).end = match(ID);
				setState(101);
				comentario();
				setState(102);
				match(EOF);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InicioFuncionContext extends ParserRuleContext {
		public Token idf1;
		public ExpresionContext retorno;
		public Token idf2;
		public List<TerminalNode> FUNCTION() { return getTokens(JGrammarParser.FUNCTION); }
		public TerminalNode FUNCTION(int i) {
			return getToken(JGrammarParser.FUNCTION, i);
		}
		public List<TerminalNode> PARIZQ() { return getTokens(JGrammarParser.PARIZQ); }
		public TerminalNode PARIZQ(int i) {
			return getToken(JGrammarParser.PARIZQ, i);
		}
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public List<TerminalNode> PARDER() { return getTokens(JGrammarParser.PARDER); }
		public TerminalNode PARDER(int i) {
			return getToken(JGrammarParser.PARDER, i);
		}
		public TerminalNode RESULT() { return getToken(JGrammarParser.RESULT, 0); }
		public TerminalNode IMPLICIT() { return getToken(JGrammarParser.IMPLICIT, 0); }
		public TerminalNode NONE() { return getToken(JGrammarParser.NONE, 0); }
		public DeclararParametrosContext declararParametros() {
			return getRuleContext(DeclararParametrosContext.class,0);
		}
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public InicioFuncionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inicioFuncion; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitInicioFuncion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InicioFuncionContext inicioFuncion() throws RecognitionException {
		InicioFuncionContext _localctx = new InicioFuncionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_inicioFuncion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(FUNCTION);
			setState(107);
			((InicioFuncionContext)_localctx).idf1 = match(ID);
			setState(108);
			match(PARIZQ);
			setState(109);
			listaExpr();
			setState(110);
			match(PARDER);
			setState(111);
			match(RESULT);
			setState(112);
			match(PARIZQ);
			setState(113);
			((InicioFuncionContext)_localctx).retorno = expresion(0);
			setState(114);
			match(PARDER);
			setState(115);
			match(IMPLICIT);
			setState(116);
			match(NONE);
			setState(117);
			declararParametros();
			setState(118);
			linstrucciones();
			setState(119);
			match(END);
			setState(120);
			match(FUNCTION);
			setState(121);
			((InicioFuncionContext)_localctx).idf2 = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InicioSubRutinaContext extends ParserRuleContext {
		public Token idsb1;
		public Token idsb2;
		public List<TerminalNode> SUBRUTINE() { return getTokens(JGrammarParser.SUBRUTINE); }
		public TerminalNode SUBRUTINE(int i) {
			return getToken(JGrammarParser.SUBRUTINE, i);
		}
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode IMPLICIT() { return getToken(JGrammarParser.IMPLICIT, 0); }
		public TerminalNode NONE() { return getToken(JGrammarParser.NONE, 0); }
		public DeclararParametrosContext declararParametros() {
			return getRuleContext(DeclararParametrosContext.class,0);
		}
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public InicioSubRutinaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inicioSubRutina; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitInicioSubRutina(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InicioSubRutinaContext inicioSubRutina() throws RecognitionException {
		InicioSubRutinaContext _localctx = new InicioSubRutinaContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_inicioSubRutina);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			match(SUBRUTINE);
			setState(124);
			((InicioSubRutinaContext)_localctx).idsb1 = match(ID);
			setState(125);
			match(PARIZQ);
			setState(126);
			listaExpr();
			setState(127);
			match(PARDER);
			setState(128);
			match(IMPLICIT);
			setState(129);
			match(NONE);
			setState(130);
			declararParametros();
			setState(131);
			linstrucciones();
			setState(132);
			match(END);
			setState(133);
			match(SUBRUTINE);
			setState(134);
			((InicioSubRutinaContext)_localctx).idsb2 = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclararParametrosContext extends ParserRuleContext {
		public List<ParametrosContext> parametros() {
			return getRuleContexts(ParametrosContext.class);
		}
		public ParametrosContext parametros(int i) {
			return getRuleContext(ParametrosContext.class,i);
		}
		public DeclararParametrosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declararParametros; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitDeclararParametros(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclararParametrosContext declararParametros() throws RecognitionException {
		DeclararParametrosContext _localctx = new DeclararParametrosContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_declararParametros);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(139);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(136);
					parametros();
					}
					} 
				}
				setState(141);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametrosContext extends ParserRuleContext {
		public Token var;
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public TerminalNode COMA() { return getToken(JGrammarParser.COMA, 0); }
		public TerminalNode INTENT() { return getToken(JGrammarParser.INTENT, 0); }
		public List<TerminalNode> PARIZQ() { return getTokens(JGrammarParser.PARIZQ); }
		public TerminalNode PARIZQ(int i) {
			return getToken(JGrammarParser.PARIZQ, i);
		}
		public TerminalNode IN() { return getToken(JGrammarParser.IN, 0); }
		public List<TerminalNode> PARDER() { return getTokens(JGrammarParser.PARDER); }
		public TerminalNode PARDER(int i) {
			return getToken(JGrammarParser.PARDER, i);
		}
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public ParametrosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametros; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitParametros(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametrosContext parametros() throws RecognitionException {
		ParametrosContext _localctx = new ParametrosContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_parametros);
		try {
			setState(163);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(142);
				tipo();
				setState(143);
				match(COMA);
				setState(144);
				match(INTENT);
				setState(145);
				match(PARIZQ);
				setState(146);
				match(IN);
				setState(147);
				match(PARDER);
				setState(148);
				match(T__0);
				setState(149);
				((ParametrosContext)_localctx).var = match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(151);
				tipo();
				setState(152);
				match(COMA);
				setState(153);
				match(INTENT);
				setState(154);
				match(PARIZQ);
				setState(155);
				match(IN);
				setState(156);
				match(PARDER);
				setState(157);
				match(T__0);
				setState(158);
				((ParametrosContext)_localctx).var = match(ID);
				setState(159);
				match(PARIZQ);
				setState(160);
				listaExpr();
				setState(161);
				match(PARDER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallSubrutinaContext extends ParserRuleContext {
		public Token id;
		public TerminalNode CALL() { return getToken(JGrammarParser.CALL, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public CallSubrutinaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callSubrutina; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitCallSubrutina(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallSubrutinaContext callSubrutina() throws RecognitionException {
		CallSubrutinaContext _localctx = new CallSubrutinaContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_callSubrutina);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			match(CALL);
			setState(166);
			((CallSubrutinaContext)_localctx).id = match(ID);
			setState(167);
			match(PARIZQ);
			setState(168);
			listaExpr();
			setState(169);
			match(PARDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueFuncionesContext extends ParserRuleContext {
		public List<InicioSubRutinaContext> inicioSubRutina() {
			return getRuleContexts(InicioSubRutinaContext.class);
		}
		public InicioSubRutinaContext inicioSubRutina(int i) {
			return getRuleContext(InicioSubRutinaContext.class,i);
		}
		public BloqueFuncionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueFunciones; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitBloqueFunciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueFuncionesContext bloqueFunciones() throws RecognitionException {
		BloqueFuncionesContext _localctx = new BloqueFuncionesContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_bloqueFunciones);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SUBRUTINE) {
				{
				{
				setState(171);
				inicioSubRutina();
				}
				}
				setState(176);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueFunciones2Context extends ParserRuleContext {
		public List<InicioFuncionContext> inicioFuncion() {
			return getRuleContexts(InicioFuncionContext.class);
		}
		public InicioFuncionContext inicioFuncion(int i) {
			return getRuleContext(InicioFuncionContext.class,i);
		}
		public BloqueFunciones2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueFunciones2; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitBloqueFunciones2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueFunciones2Context bloqueFunciones2() throws RecognitionException {
		BloqueFunciones2Context _localctx = new BloqueFunciones2Context(_ctx, getState());
		enterRule(_localctx, 14, RULE_bloqueFunciones2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FUNCTION) {
				{
				{
				setState(177);
				inicioFuncion();
				}
				}
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComentarioContext extends ParserRuleContext {
		public List<TerminalNode> COMENTARIO_LINEA() { return getTokens(JGrammarParser.COMENTARIO_LINEA); }
		public TerminalNode COMENTARIO_LINEA(int i) {
			return getToken(JGrammarParser.COMENTARIO_LINEA, i);
		}
		public ComentarioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comentario; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitComentario(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComentarioContext comentario() throws RecognitionException {
		ComentarioContext _localctx = new ComentarioContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_comentario);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMENTARIO_LINEA) {
				{
				{
				setState(183);
				match(COMENTARIO_LINEA);
				}
				}
				setState(188);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LinstruccionesContext extends ParserRuleContext {
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public LinstruccionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_linstrucciones; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitLinstrucciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LinstruccionesContext linstrucciones() throws RecognitionException {
		LinstruccionesContext _localctx = new LinstruccionesContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_linstrucciones);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 19)) & ~0x3f) == 0 && ((1L << (_la - 19)) & ((1L << (PRINT - 19)) | (1L << (ALLOCATE - 19)) | (1L << (DEALLOCATE - 19)) | (1L << (IF - 19)) | (1L << (DO - 19)) | (1L << (EXIT - 19)) | (1L << (CYCLE - 19)) | (1L << (RINTEGER - 19)) | (1L << (RREAL - 19)) | (1L << (RLOGICAL - 19)) | (1L << (RCOMPLEX - 19)) | (1L << (RCHARACTER - 19)) | (1L << (CALL - 19)) | (1L << (ID - 19)) | (1L << (COMENTARIO_LINEA - 19)))) != 0)) {
				{
				{
				setState(189);
				instrucciones();
				}
				}
				setState(194);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstruccionesContext extends ParserRuleContext {
		public DeclaracionContext declaracion() {
			return getRuleContext(DeclaracionContext.class,0);
		}
		public TerminalNode COMENTARIO_LINEA() { return getToken(JGrammarParser.COMENTARIO_LINEA, 0); }
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public AsignacionvarContext asignacionvar() {
			return getRuleContext(AsignacionvarContext.class,0);
		}
		public ArreglosestaticosContext arreglosestaticos() {
			return getRuleContext(ArreglosestaticosContext.class,0);
		}
		public AsignarArrayUniContext asignarArrayUni() {
			return getRuleContext(AsignarArrayUniContext.class,0);
		}
		public AsignarArrayBidContext asignarArrayBid() {
			return getRuleContext(AsignarArrayBidContext.class,0);
		}
		public ArreglosDinamicosContext arreglosDinamicos() {
			return getRuleContext(ArreglosDinamicosContext.class,0);
		}
		public FunAllocateContext funAllocate() {
			return getRuleContext(FunAllocateContext.class,0);
		}
		public FunDeAllocateContext funDeAllocate() {
			return getRuleContext(FunDeAllocateContext.class,0);
		}
		public SentenciaIfContext sentenciaIf() {
			return getRuleContext(SentenciaIfContext.class,0);
		}
		public CicloDoContext cicloDo() {
			return getRuleContext(CicloDoContext.class,0);
		}
		public CicloDoWhileContext cicloDoWhile() {
			return getRuleContext(CicloDoWhileContext.class,0);
		}
		public ControlCiclosContext controlCiclos() {
			return getRuleContext(ControlCiclosContext.class,0);
		}
		public CallSubrutinaContext callSubrutina() {
			return getRuleContext(CallSubrutinaContext.class,0);
		}
		public InstruccionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instrucciones; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitInstrucciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstruccionesContext instrucciones() throws RecognitionException {
		InstruccionesContext _localctx = new InstruccionesContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_instrucciones);
		try {
			setState(210);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(195);
				declaracion();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(196);
				match(COMENTARIO_LINEA);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(197);
				print();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(198);
				asignacionvar();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(199);
				arreglosestaticos();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(200);
				asignarArrayUni();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(201);
				asignarArrayBid();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(202);
				arreglosDinamicos();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(203);
				funAllocate();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(204);
				funDeAllocate();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(205);
				sentenciaIf();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(206);
				cicloDo();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(207);
				cicloDoWhile();
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(208);
				controlCiclos();
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(209);
				callSubrutina();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaracionContext extends ParserRuleContext {
		public TipoContext t;
		public ListaDeclaradaContext l;
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public ListaDeclaradaContext listaDeclarada() {
			return getRuleContext(ListaDeclaradaContext.class,0);
		}
		public DeclaracionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaracion; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitDeclaracion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclaracionContext declaracion() throws RecognitionException {
		DeclaracionContext _localctx = new DeclaracionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_declaracion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			((DeclaracionContext)_localctx).t = tipo();
			setState(213);
			match(T__0);
			setState(214);
			((DeclaracionContext)_localctx).l = listaDeclarada();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaDeclaradaContext extends ParserRuleContext {
		public List<VariablesContext> variables() {
			return getRuleContexts(VariablesContext.class);
		}
		public VariablesContext variables(int i) {
			return getRuleContext(VariablesContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public ListaDeclaradaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaDeclarada; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaDeclarada(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaDeclaradaContext listaDeclarada() throws RecognitionException {
		ListaDeclaradaContext _localctx = new ListaDeclaradaContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_listaDeclarada);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			variables();
			setState(221);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(217);
				match(COMA);
				setState(218);
				variables();
				}
				}
				setState(223);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariablesContext extends ParserRuleContext {
		public Token i;
		public ExpresionContext e;
		public TerminalNode IGUAL() { return getToken(JGrammarParser.IGUAL, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public VariablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variables; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitVariables(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariablesContext variables() throws RecognitionException {
		VariablesContext _localctx = new VariablesContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_variables);
		try {
			setState(228);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(224);
				((VariablesContext)_localctx).i = match(ID);
				setState(225);
				match(IGUAL);
				setState(226);
				((VariablesContext)_localctx).e = expresion(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(227);
				((VariablesContext)_localctx).i = match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignacionvarContext extends ParserRuleContext {
		public Token i;
		public ExpresionContext l;
		public TerminalNode IGUAL() { return getToken(JGrammarParser.IGUAL, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public AsignacionvarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignacionvar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitAsignacionvar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsignacionvarContext asignacionvar() throws RecognitionException {
		AsignacionvarContext _localctx = new AsignacionvarContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_asignacionvar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			((AsignacionvarContext)_localctx).i = match(ID);
			setState(231);
			match(IGUAL);
			setState(232);
			((AsignacionvarContext)_localctx).l = expresion(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public ListaprintContext l;
		public TerminalNode PRINT() { return getToken(JGrammarParser.PRINT, 0); }
		public TerminalNode MULTIPLICACION() { return getToken(JGrammarParser.MULTIPLICACION, 0); }
		public TerminalNode COMA() { return getToken(JGrammarParser.COMA, 0); }
		public ListaprintContext listaprint() {
			return getRuleContext(ListaprintContext.class,0);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_print);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			match(PRINT);
			setState(235);
			match(MULTIPLICACION);
			setState(236);
			match(COMA);
			setState(237);
			((PrintContext)_localctx).l = listaprint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaprintContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public ListaprintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaprint; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaprint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaprintContext listaprint() throws RecognitionException {
		ListaprintContext _localctx = new ListaprintContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_listaprint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			expresion(0);
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(240);
				match(COMA);
				setState(241);
				expresion(0);
				}
				}
				setState(246);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArreglosestaticosContext extends ParserRuleContext {
		public TipoContext t;
		public DimContext d;
		public IdarraysContext l;
		public TerminalNode COMA() { return getToken(JGrammarParser.COMA, 0); }
		public TerminalNode DIMENSION() { return getToken(JGrammarParser.DIMENSION, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public DimContext dim() {
			return getRuleContext(DimContext.class,0);
		}
		public IdarraysContext idarrays() {
			return getRuleContext(IdarraysContext.class,0);
		}
		public ArreglosestaticosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arreglosestaticos; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitArreglosestaticos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArreglosestaticosContext arreglosestaticos() throws RecognitionException {
		ArreglosestaticosContext _localctx = new ArreglosestaticosContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_arreglosestaticos);
		try {
			setState(260);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(247);
				((ArreglosestaticosContext)_localctx).t = tipo();
				setState(248);
				match(COMA);
				setState(249);
				match(DIMENSION);
				setState(250);
				match(PARIZQ);
				setState(251);
				((ArreglosestaticosContext)_localctx).d = dim();
				setState(252);
				match(PARDER);
				setState(253);
				match(T__0);
				setState(254);
				((ArreglosestaticosContext)_localctx).l = idarrays();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(256);
				((ArreglosestaticosContext)_localctx).t = tipo();
				setState(257);
				match(T__0);
				setState(258);
				((ArreglosestaticosContext)_localctx).l = idarrays();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdarraysContext extends ParserRuleContext {
		public List<ListaArraSimpleContext> listaArraSimple() {
			return getRuleContexts(ListaArraSimpleContext.class);
		}
		public ListaArraSimpleContext listaArraSimple(int i) {
			return getRuleContext(ListaArraSimpleContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public IdarraysContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idarrays; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitIdarrays(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdarraysContext idarrays() throws RecognitionException {
		IdarraysContext _localctx = new IdarraysContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_idarrays);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			listaArraSimple();
			setState(267);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(263);
				match(COMA);
				setState(264);
				listaArraSimple();
				}
				}
				setState(269);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaArraSimpleContext extends ParserRuleContext {
		public Token i;
		public DimContext d;
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public DimContext dim() {
			return getRuleContext(DimContext.class,0);
		}
		public ListaArraSimpleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaArraSimple; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaArraSimple(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaArraSimpleContext listaArraSimple() throws RecognitionException {
		ListaArraSimpleContext _localctx = new ListaArraSimpleContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_listaArraSimple);
		try {
			setState(276);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(270);
				((ListaArraSimpleContext)_localctx).i = match(ID);
				setState(271);
				match(PARIZQ);
				setState(272);
				((ListaArraSimpleContext)_localctx).d = dim();
				setState(273);
				match(PARDER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(275);
				((ListaArraSimpleContext)_localctx).i = match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public DimContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitDim(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DimContext dim() throws RecognitionException {
		DimContext _localctx = new DimContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_dim);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(278);
			expresion(0);
			setState(283);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(279);
				match(COMA);
				setState(280);
				expresion(0);
				}
				}
				setState(285);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArreglosDinamicosContext extends ParserRuleContext {
		public TipoContext t;
		public IdArrayDinContext l;
		public TerminalNode COMA() { return getToken(JGrammarParser.COMA, 0); }
		public TerminalNode ALLOCATABLE() { return getToken(JGrammarParser.ALLOCATABLE, 0); }
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public IdArrayDinContext idArrayDin() {
			return getRuleContext(IdArrayDinContext.class,0);
		}
		public ArreglosDinamicosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arreglosDinamicos; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitArreglosDinamicos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArreglosDinamicosContext arreglosDinamicos() throws RecognitionException {
		ArreglosDinamicosContext _localctx = new ArreglosDinamicosContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_arreglosDinamicos);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			((ArreglosDinamicosContext)_localctx).t = tipo();
			setState(287);
			match(COMA);
			setState(288);
			match(ALLOCATABLE);
			setState(289);
			match(T__0);
			setState(290);
			((ArreglosDinamicosContext)_localctx).l = idArrayDin();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdArrayDinContext extends ParserRuleContext {
		public List<ListaArraBidContext> listaArraBid() {
			return getRuleContexts(ListaArraBidContext.class);
		}
		public ListaArraBidContext listaArraBid(int i) {
			return getRuleContext(ListaArraBidContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public IdArrayDinContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idArrayDin; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitIdArrayDin(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdArrayDinContext idArrayDin() throws RecognitionException {
		IdArrayDinContext _localctx = new IdArrayDinContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_idArrayDin);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(292);
			listaArraBid();
			setState(297);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(293);
				match(COMA);
				setState(294);
				listaArraBid();
				}
				}
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaArraBidContext extends ParserRuleContext {
		public Token i;
		public DimBidContext d;
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public DimBidContext dimBid() {
			return getRuleContext(DimBidContext.class,0);
		}
		public ListaArraBidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaArraBid; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaArraBid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaArraBidContext listaArraBid() throws RecognitionException {
		ListaArraBidContext _localctx = new ListaArraBidContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_listaArraBid);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			((ListaArraBidContext)_localctx).i = match(ID);
			setState(301);
			match(PARIZQ);
			setState(302);
			((ListaArraBidContext)_localctx).d = dimBid();
			setState(303);
			match(PARDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimBidContext extends ParserRuleContext {
		public List<TerminalNode> DOSPUNTOS() { return getTokens(JGrammarParser.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(JGrammarParser.DOSPUNTOS, i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public DimBidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimBid; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitDimBid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DimBidContext dimBid() throws RecognitionException {
		DimBidContext _localctx = new DimBidContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_dimBid);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(305);
			match(DOSPUNTOS);
			setState(310);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(306);
				match(COMA);
				setState(307);
				match(DOSPUNTOS);
				}
				}
				setState(312);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignarArrayUniContext extends ParserRuleContext {
		public Token i;
		public ListaExprContext l;
		public ExpresionContext pos;
		public ExpresionContext val;
		public TerminalNode IGUAL() { return getToken(JGrammarParser.IGUAL, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public List<TerminalNode> DIVISION() { return getTokens(JGrammarParser.DIVISION); }
		public TerminalNode DIVISION(int i) {
			return getToken(JGrammarParser.DIVISION, i);
		}
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public TerminalNode CORIZQ() { return getToken(JGrammarParser.CORIZQ, 0); }
		public TerminalNode CORDER() { return getToken(JGrammarParser.CORDER, 0); }
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public AsignarArrayUniContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignarArrayUni; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitAsignarArrayUni(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsignarArrayUniContext asignarArrayUni() throws RecognitionException {
		AsignarArrayUniContext _localctx = new AsignarArrayUniContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_asignarArrayUni);
		try {
			setState(328);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(313);
				((AsignarArrayUniContext)_localctx).i = match(ID);
				setState(314);
				match(IGUAL);
				setState(315);
				match(PARIZQ);
				setState(316);
				match(DIVISION);
				setState(317);
				((AsignarArrayUniContext)_localctx).l = listaExpr();
				setState(318);
				match(DIVISION);
				setState(319);
				match(PARDER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(321);
				((AsignarArrayUniContext)_localctx).i = match(ID);
				setState(322);
				match(CORIZQ);
				setState(323);
				((AsignarArrayUniContext)_localctx).pos = expresion(0);
				setState(324);
				match(CORDER);
				setState(325);
				match(IGUAL);
				setState(326);
				((AsignarArrayUniContext)_localctx).val = expresion(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaExprContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public ListaExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaExprContext listaExpr() throws RecognitionException {
		ListaExprContext _localctx = new ListaExprContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_listaExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 4)) & ~0x3f) == 0 && ((1L << (_la - 4)) & ((1L << (PARIZQ - 4)) | (1L << (TRUE - 4)) | (1L << (FALSE - 4)) | (1L << (SIZE - 4)) | (1L << (NOT - 4)) | (1L << (RESTA - 4)) | (1L << (INTEGER - 4)) | (1L << (FLOAT - 4)) | (1L << (COMPLEX - 4)) | (1L << (CHARACTER - 4)) | (1L << (ID - 4)) | (1L << (CADENA - 4)))) != 0)) {
				{
				{
				setState(330);
				expresion(0);
				}
				}
				setState(335);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(340);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(336);
				match(COMA);
				setState(337);
				expresion(0);
				}
				}
				setState(342);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignarArrayBidContext extends ParserRuleContext {
		public Token i;
		public DimContext d;
		public ExpresionContext val;
		public TerminalNode CORIZQ() { return getToken(JGrammarParser.CORIZQ, 0); }
		public TerminalNode CORDER() { return getToken(JGrammarParser.CORDER, 0); }
		public TerminalNode IGUAL() { return getToken(JGrammarParser.IGUAL, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public DimContext dim() {
			return getRuleContext(DimContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public AsignarArrayBidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignarArrayBid; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitAsignarArrayBid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsignarArrayBidContext asignarArrayBid() throws RecognitionException {
		AsignarArrayBidContext _localctx = new AsignarArrayBidContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_asignarArrayBid);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(343);
			((AsignarArrayBidContext)_localctx).i = match(ID);
			setState(344);
			match(CORIZQ);
			setState(345);
			((AsignarArrayBidContext)_localctx).d = dim();
			setState(346);
			match(CORDER);
			setState(347);
			match(IGUAL);
			setState(348);
			((AsignarArrayBidContext)_localctx).val = expresion(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunAllocateContext extends ParserRuleContext {
		public IdarraysContext l;
		public TerminalNode ALLOCATE() { return getToken(JGrammarParser.ALLOCATE, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public IdarraysContext idarrays() {
			return getRuleContext(IdarraysContext.class,0);
		}
		public FunAllocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funAllocate; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitFunAllocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunAllocateContext funAllocate() throws RecognitionException {
		FunAllocateContext _localctx = new FunAllocateContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_funAllocate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350);
			match(ALLOCATE);
			setState(351);
			match(PARIZQ);
			setState(352);
			((FunAllocateContext)_localctx).l = idarrays();
			setState(353);
			match(PARDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunDeAllocateContext extends ParserRuleContext {
		public ListaDeallocateContext l;
		public TerminalNode DEALLOCATE() { return getToken(JGrammarParser.DEALLOCATE, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public ListaDeallocateContext listaDeallocate() {
			return getRuleContext(ListaDeallocateContext.class,0);
		}
		public FunDeAllocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funDeAllocate; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitFunDeAllocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunDeAllocateContext funDeAllocate() throws RecognitionException {
		FunDeAllocateContext _localctx = new FunDeAllocateContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_funDeAllocate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			match(DEALLOCATE);
			setState(356);
			match(PARIZQ);
			setState(357);
			((FunDeAllocateContext)_localctx).l = listaDeallocate();
			setState(358);
			match(PARDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaDeallocateContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public ListaDeallocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaDeallocate; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaDeallocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaDeallocateContext listaDeallocate() throws RecognitionException {
		ListaDeallocateContext _localctx = new ListaDeallocateContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_listaDeallocate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			match(ID);
			setState(365);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(361);
				match(COMA);
				setState(362);
				match(ID);
				}
				}
				setState(367);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SentenciaIfContext extends ParserRuleContext {
		public ExpresionContext ep;
		public LinstruccionesContext bp;
		public LinstruccionesContext bfinal;
		public BloqueElseContext bElse;
		public List<TerminalNode> IF() { return getTokens(JGrammarParser.IF); }
		public TerminalNode IF(int i) {
			return getToken(JGrammarParser.IF, i);
		}
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode THEN() { return getToken(JGrammarParser.THEN, 0); }
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public List<LinstruccionesContext> linstrucciones() {
			return getRuleContexts(LinstruccionesContext.class);
		}
		public LinstruccionesContext linstrucciones(int i) {
			return getRuleContext(LinstruccionesContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(JGrammarParser.ELSE, 0); }
		public BloqueElseContext bloqueElse() {
			return getRuleContext(BloqueElseContext.class,0);
		}
		public SentenciaIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentenciaIf; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitSentenciaIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SentenciaIfContext sentenciaIf() throws RecognitionException {
		SentenciaIfContext _localctx = new SentenciaIfContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_sentenciaIf);
		try {
			setState(410);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(368);
				match(IF);
				setState(369);
				match(PARIZQ);
				setState(370);
				((SentenciaIfContext)_localctx).ep = expresion(0);
				setState(371);
				match(PARDER);
				setState(372);
				match(THEN);
				setState(373);
				((SentenciaIfContext)_localctx).bp = linstrucciones();
				setState(374);
				match(END);
				setState(375);
				match(IF);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(377);
				match(IF);
				setState(378);
				match(PARIZQ);
				setState(379);
				((SentenciaIfContext)_localctx).ep = expresion(0);
				setState(380);
				match(PARDER);
				setState(381);
				match(THEN);
				setState(382);
				((SentenciaIfContext)_localctx).bp = linstrucciones();
				setState(383);
				match(ELSE);
				setState(384);
				((SentenciaIfContext)_localctx).bfinal = linstrucciones();
				setState(385);
				match(END);
				setState(386);
				match(IF);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(388);
				match(IF);
				setState(389);
				match(PARIZQ);
				setState(390);
				((SentenciaIfContext)_localctx).ep = expresion(0);
				setState(391);
				match(PARDER);
				setState(392);
				match(THEN);
				setState(393);
				((SentenciaIfContext)_localctx).bp = linstrucciones();
				setState(394);
				((SentenciaIfContext)_localctx).bElse = bloqueElse();
				setState(395);
				match(END);
				setState(396);
				match(IF);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(398);
				match(IF);
				setState(399);
				match(PARIZQ);
				setState(400);
				((SentenciaIfContext)_localctx).ep = expresion(0);
				setState(401);
				match(PARDER);
				setState(402);
				match(THEN);
				setState(403);
				((SentenciaIfContext)_localctx).bp = linstrucciones();
				setState(404);
				((SentenciaIfContext)_localctx).bElse = bloqueElse();
				setState(405);
				match(ELSE);
				setState(406);
				((SentenciaIfContext)_localctx).bfinal = linstrucciones();
				setState(407);
				match(END);
				setState(408);
				match(IF);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueElseContext extends ParserRuleContext {
		public List<ListaElseContext> listaElse() {
			return getRuleContexts(ListaElseContext.class);
		}
		public ListaElseContext listaElse(int i) {
			return getRuleContext(ListaElseContext.class,i);
		}
		public BloqueElseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueElse; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitBloqueElse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueElseContext bloqueElse() throws RecognitionException {
		BloqueElseContext _localctx = new BloqueElseContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_bloqueElse);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(412);
			listaElse();
			setState(416);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(413);
					listaElse();
					}
					} 
				}
				setState(418);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaElseContext extends ParserRuleContext {
		public ExpresionContext ep;
		public LinstruccionesContext bp;
		public TerminalNode ELSE() { return getToken(JGrammarParser.ELSE, 0); }
		public TerminalNode IF() { return getToken(JGrammarParser.IF, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode THEN() { return getToken(JGrammarParser.THEN, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public ListaElseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaElse; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaElse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaElseContext listaElse() throws RecognitionException {
		ListaElseContext _localctx = new ListaElseContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_listaElse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419);
			match(ELSE);
			setState(420);
			match(IF);
			setState(421);
			match(PARIZQ);
			setState(422);
			((ListaElseContext)_localctx).ep = expresion(0);
			setState(423);
			match(PARDER);
			setState(424);
			match(THEN);
			setState(425);
			((ListaElseContext)_localctx).bp = linstrucciones();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CicloDoContext extends ParserRuleContext {
		public Token var;
		public ListaCondicionesContext lc;
		public LinstruccionesContext bp;
		public Token idDOi;
		public Token idDOf;
		public List<TerminalNode> DO() { return getTokens(JGrammarParser.DO); }
		public TerminalNode DO(int i) {
			return getToken(JGrammarParser.DO, i);
		}
		public TerminalNode IGUAL() { return getToken(JGrammarParser.IGUAL, 0); }
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public ListaCondicionesContext listaCondiciones() {
			return getRuleContext(ListaCondicionesContext.class,0);
		}
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public TerminalNode DOSPUNTOS() { return getToken(JGrammarParser.DOSPUNTOS, 0); }
		public CicloDoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cicloDo; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitCicloDo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CicloDoContext cicloDo() throws RecognitionException {
		CicloDoContext _localctx = new CicloDoContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_cicloDo);
		try {
			setState(446);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DO:
				enterOuterAlt(_localctx, 1);
				{
				setState(427);
				match(DO);
				setState(428);
				((CicloDoContext)_localctx).var = match(ID);
				setState(429);
				match(IGUAL);
				setState(430);
				((CicloDoContext)_localctx).lc = listaCondiciones();
				setState(431);
				((CicloDoContext)_localctx).bp = linstrucciones();
				setState(432);
				match(END);
				setState(433);
				match(DO);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(435);
				((CicloDoContext)_localctx).idDOi = match(ID);
				setState(436);
				match(DOSPUNTOS);
				setState(437);
				match(DO);
				setState(438);
				((CicloDoContext)_localctx).var = match(ID);
				setState(439);
				match(IGUAL);
				setState(440);
				((CicloDoContext)_localctx).lc = listaCondiciones();
				setState(441);
				((CicloDoContext)_localctx).bp = linstrucciones();
				setState(442);
				match(END);
				setState(443);
				match(DO);
				setState(444);
				((CicloDoContext)_localctx).idDOf = match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaCondicionesContext extends ParserRuleContext {
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(JGrammarParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(JGrammarParser.COMA, i);
		}
		public ListaCondicionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaCondiciones; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitListaCondiciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaCondicionesContext listaCondiciones() throws RecognitionException {
		ListaCondicionesContext _localctx = new ListaCondicionesContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_listaCondiciones);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(448);
			expresion(0);
			setState(453);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(449);
				match(COMA);
				setState(450);
				expresion(0);
				}
				}
				setState(455);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CicloDoWhileContext extends ParserRuleContext {
		public ExpresionContext cond;
		public LinstruccionesContext bp;
		public Token idDOi;
		public Token idDOf;
		public List<TerminalNode> DO() { return getTokens(JGrammarParser.DO); }
		public TerminalNode DO(int i) {
			return getToken(JGrammarParser.DO, i);
		}
		public TerminalNode WHILE() { return getToken(JGrammarParser.WHILE, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode END() { return getToken(JGrammarParser.END, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public TerminalNode DOSPUNTOS() { return getToken(JGrammarParser.DOSPUNTOS, 0); }
		public List<TerminalNode> ID() { return getTokens(JGrammarParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JGrammarParser.ID, i);
		}
		public CicloDoWhileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cicloDoWhile; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitCicloDoWhile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CicloDoWhileContext cicloDoWhile() throws RecognitionException {
		CicloDoWhileContext _localctx = new CicloDoWhileContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_cicloDoWhile);
		try {
			setState(477);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DO:
				enterOuterAlt(_localctx, 1);
				{
				setState(456);
				match(DO);
				setState(457);
				match(WHILE);
				setState(458);
				match(PARIZQ);
				setState(459);
				((CicloDoWhileContext)_localctx).cond = expresion(0);
				setState(460);
				match(PARDER);
				setState(461);
				((CicloDoWhileContext)_localctx).bp = linstrucciones();
				setState(462);
				match(END);
				setState(463);
				match(DO);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(465);
				((CicloDoWhileContext)_localctx).idDOi = match(ID);
				setState(466);
				match(DOSPUNTOS);
				setState(467);
				match(DO);
				setState(468);
				match(WHILE);
				setState(469);
				match(PARIZQ);
				setState(470);
				((CicloDoWhileContext)_localctx).cond = expresion(0);
				setState(471);
				match(PARDER);
				setState(472);
				((CicloDoWhileContext)_localctx).bp = linstrucciones();
				setState(473);
				match(END);
				setState(474);
				match(DO);
				setState(475);
				((CicloDoWhileContext)_localctx).idDOf = match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TipoContext extends ParserRuleContext {
		public TerminalNode RINTEGER() { return getToken(JGrammarParser.RINTEGER, 0); }
		public TerminalNode RREAL() { return getToken(JGrammarParser.RREAL, 0); }
		public TerminalNode RLOGICAL() { return getToken(JGrammarParser.RLOGICAL, 0); }
		public TerminalNode RCOMPLEX() { return getToken(JGrammarParser.RCOMPLEX, 0); }
		public TerminalNode RCHARACTER() { return getToken(JGrammarParser.RCHARACTER, 0); }
		public TipoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitTipo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TipoContext tipo() throws RecognitionException {
		TipoContext _localctx = new TipoContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_tipo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(479);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RINTEGER) | (1L << RREAL) | (1L << RLOGICAL) | (1L << RCOMPLEX) | (1L << RCHARACTER))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ControlCiclosContext extends ParserRuleContext {
		public Token tk;
		public Token idcycle;
		public Token idexit;
		public TerminalNode EXIT() { return getToken(JGrammarParser.EXIT, 0); }
		public TerminalNode CYCLE() { return getToken(JGrammarParser.CYCLE, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public ControlCiclosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_controlCiclos; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitControlCiclos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ControlCiclosContext controlCiclos() throws RecognitionException {
		ControlCiclosContext _localctx = new ControlCiclosContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_controlCiclos);
		try {
			setState(487);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(481);
				((ControlCiclosContext)_localctx).tk = match(EXIT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(482);
				((ControlCiclosContext)_localctx).tk = match(CYCLE);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(483);
				((ControlCiclosContext)_localctx).tk = match(CYCLE);
				setState(484);
				((ControlCiclosContext)_localctx).idcycle = match(ID);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(485);
				((ControlCiclosContext)_localctx).tk = match(EXIT);
				setState(486);
				((ControlCiclosContext)_localctx).idexit = match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionContext extends ParserRuleContext {
		public ExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresion; }
	 
		public ExpresionContext() { }
		public void copyFrom(ExpresionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OpAritmeticaContext extends ExpresionContext {
		public ExpresionContext left;
		public Token op;
		public ExpresionContext right;
		public TerminalNode RESTA() { return getToken(JGrammarParser.RESTA, 0); }
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public TerminalNode POTENCIA() { return getToken(JGrammarParser.POTENCIA, 0); }
		public TerminalNode MULTIPLICACION() { return getToken(JGrammarParser.MULTIPLICACION, 0); }
		public TerminalNode DIVISION() { return getToken(JGrammarParser.DIVISION, 0); }
		public TerminalNode SUMA() { return getToken(JGrammarParser.SUMA, 0); }
		public OpAritmeticaContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitOpAritmetica(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogicoContext extends ExpresionContext {
		public TerminalNode TRUE() { return getToken(JGrammarParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(JGrammarParser.FALSE, 0); }
		public LogicoContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitLogico(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrBiExpContext extends ExpresionContext {
		public DimContext pos;
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public TerminalNode CORIZQ() { return getToken(JGrammarParser.CORIZQ, 0); }
		public TerminalNode CORDER() { return getToken(JGrammarParser.CORDER, 0); }
		public DimContext dim() {
			return getRuleContext(DimContext.class,0);
		}
		public ArrBiExpContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitArrBiExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CadContext extends ExpresionContext {
		public TerminalNode CADENA() { return getToken(JGrammarParser.CADENA, 0); }
		public CadContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitCad(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CallFunctionContext extends ExpresionContext {
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public ListaExprContext listaExpr() {
			return getRuleContext(ListaExprContext.class,0);
		}
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public CallFunctionContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitCallFunction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntegerContext extends ExpresionContext {
		public TerminalNode INTEGER() { return getToken(JGrammarParser.INTEGER, 0); }
		public IntegerContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitInteger(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrUniExpContext extends ExpresionContext {
		public ExpresionContext pos;
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public TerminalNode CORIZQ() { return getToken(JGrammarParser.CORIZQ, 0); }
		public TerminalNode CORDER() { return getToken(JGrammarParser.CORDER, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public ArrUniExpContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitArrUniExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FloatContext extends ExpresionContext {
		public TerminalNode FLOAT() { return getToken(JGrammarParser.FLOAT, 0); }
		public FloatContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitFloat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExprContext extends ExpresionContext {
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public ParenExprContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitParenExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarExpContext extends ExpresionContext {
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public VarExpContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitVarExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComplexContext extends ExpresionContext {
		public TerminalNode COMPLEX() { return getToken(JGrammarParser.COMPLEX, 0); }
		public ComplexContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitComplex(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CharContext extends ExpresionContext {
		public TerminalNode CHARACTER() { return getToken(JGrammarParser.CHARACTER, 0); }
		public CharContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitChar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OpRelacionalContext extends ExpresionContext {
		public ExpresionContext left;
		public Token op;
		public ExpresionContext right;
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public TerminalNode MAYORIGUAL() { return getToken(JGrammarParser.MAYORIGUAL, 0); }
		public TerminalNode MENORIGUAL() { return getToken(JGrammarParser.MENORIGUAL, 0); }
		public TerminalNode IGUALACION() { return getToken(JGrammarParser.IGUALACION, 0); }
		public TerminalNode DIFERENCIACION() { return getToken(JGrammarParser.DIFERENCIACION, 0); }
		public TerminalNode MAYORQUE() { return getToken(JGrammarParser.MAYORQUE, 0); }
		public TerminalNode MENORQUE() { return getToken(JGrammarParser.MENORQUE, 0); }
		public OpRelacionalContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitOpRelacional(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OpSizeContext extends ExpresionContext {
		public Token i;
		public TerminalNode SIZE() { return getToken(JGrammarParser.SIZE, 0); }
		public TerminalNode PARIZQ() { return getToken(JGrammarParser.PARIZQ, 0); }
		public TerminalNode PARDER() { return getToken(JGrammarParser.PARDER, 0); }
		public TerminalNode ID() { return getToken(JGrammarParser.ID, 0); }
		public OpSizeContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitOpSize(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OpLogicaContext extends ExpresionContext {
		public ExpresionContext left;
		public Token op;
		public ExpresionContext right;
		public TerminalNode NOT() { return getToken(JGrammarParser.NOT, 0); }
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public TerminalNode AND() { return getToken(JGrammarParser.AND, 0); }
		public TerminalNode OR() { return getToken(JGrammarParser.OR, 0); }
		public OpLogicaContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JGrammarVisitor ) return ((JGrammarVisitor<? extends T>)visitor).visitOpLogica(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionContext expresion() throws RecognitionException {
		return expresion(0);
	}

	private ExpresionContext expresion(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpresionContext _localctx = new ExpresionContext(_ctx, _parentState);
		ExpresionContext _prevctx = _localctx;
		int _startState = 78;
		enterRecursionRule(_localctx, 78, RULE_expresion, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(525);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				_localctx = new OpAritmeticaContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(490);
				((OpAritmeticaContext)_localctx).op = match(RESTA);
				setState(491);
				((OpAritmeticaContext)_localctx).right = expresion(20);
				}
				break;
			case 2:
				{
				_localctx = new OpLogicaContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(492);
				((OpLogicaContext)_localctx).op = match(NOT);
				setState(493);
				((OpLogicaContext)_localctx).right = expresion(19);
				}
				break;
			case 3:
				{
				_localctx = new ParenExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(494);
				match(PARIZQ);
				setState(495);
				expresion(0);
				setState(496);
				match(PARDER);
				}
				break;
			case 4:
				{
				_localctx = new IntegerContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(498);
				match(INTEGER);
				}
				break;
			case 5:
				{
				_localctx = new FloatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(499);
				match(FLOAT);
				}
				break;
			case 6:
				{
				_localctx = new ComplexContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(500);
				match(COMPLEX);
				}
				break;
			case 7:
				{
				_localctx = new CharContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(501);
				match(CHARACTER);
				}
				break;
			case 8:
				{
				_localctx = new LogicoContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(502);
				match(TRUE);
				}
				break;
			case 9:
				{
				_localctx = new LogicoContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(503);
				match(FALSE);
				}
				break;
			case 10:
				{
				_localctx = new VarExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(504);
				match(ID);
				}
				break;
			case 11:
				{
				_localctx = new CadContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(505);
				match(CADENA);
				}
				break;
			case 12:
				{
				_localctx = new OpSizeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(506);
				match(SIZE);
				setState(507);
				match(PARIZQ);
				setState(508);
				((OpSizeContext)_localctx).i = match(ID);
				setState(509);
				match(PARDER);
				}
				break;
			case 13:
				{
				_localctx = new ArrUniExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(510);
				match(ID);
				setState(511);
				match(CORIZQ);
				setState(512);
				((ArrUniExpContext)_localctx).pos = expresion(0);
				setState(513);
				match(CORDER);
				}
				break;
			case 14:
				{
				_localctx = new ArrBiExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(515);
				match(ID);
				setState(516);
				match(CORIZQ);
				setState(517);
				((ArrBiExpContext)_localctx).pos = dim();
				setState(518);
				match(CORDER);
				}
				break;
			case 15:
				{
				_localctx = new CallFunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(520);
				match(ID);
				setState(521);
				match(PARIZQ);
				setState(522);
				listaExpr();
				setState(523);
				match(PARDER);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(544);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(542);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
					case 1:
						{
						_localctx = new OpAritmeticaContext(new ExpresionContext(_parentctx, _parentState));
						((OpAritmeticaContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expresion);
						setState(527);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(528);
						((OpAritmeticaContext)_localctx).op = match(POTENCIA);
						setState(529);
						((OpAritmeticaContext)_localctx).right = expresion(18);
						}
						break;
					case 2:
						{
						_localctx = new OpAritmeticaContext(new ExpresionContext(_parentctx, _parentState));
						((OpAritmeticaContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expresion);
						setState(530);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(531);
						((OpAritmeticaContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MULTIPLICACION || _la==DIVISION) ) {
							((OpAritmeticaContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(532);
						((OpAritmeticaContext)_localctx).right = expresion(17);
						}
						break;
					case 3:
						{
						_localctx = new OpAritmeticaContext(new ExpresionContext(_parentctx, _parentState));
						((OpAritmeticaContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expresion);
						setState(533);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(534);
						((OpAritmeticaContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==SUMA || _la==RESTA) ) {
							((OpAritmeticaContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(535);
						((OpAritmeticaContext)_localctx).right = expresion(16);
						}
						break;
					case 4:
						{
						_localctx = new OpRelacionalContext(new ExpresionContext(_parentctx, _parentState));
						((OpRelacionalContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expresion);
						setState(536);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(537);
						((OpRelacionalContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IGUALACION) | (1L << DIFERENCIACION) | (1L << MENORQUE) | (1L << MAYORQUE) | (1L << MENORIGUAL) | (1L << MAYORIGUAL))) != 0)) ) {
							((OpRelacionalContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(538);
						((OpRelacionalContext)_localctx).right = expresion(15);
						}
						break;
					case 5:
						{
						_localctx = new OpLogicaContext(new ExpresionContext(_parentctx, _parentState));
						((OpLogicaContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expresion);
						setState(539);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(540);
						((OpLogicaContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((OpLogicaContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(541);
						((OpLogicaContext)_localctx).right = expresion(14);
						}
						break;
					}
					} 
				}
				setState(546);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 39:
			return expresion_sempred((ExpresionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expresion_sempred(ExpresionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 17);
		case 1:
			return precpred(_ctx, 16);
		case 2:
			return precpred(_ctx, 15);
		case 3:
			return precpred(_ctx, 14);
		case 4:
			return precpred(_ctx, 13);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001B\u0224\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007\u0018"+
		"\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007\u001b"+
		"\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007\u001e"+
		"\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007\"\u0002"+
		"#\u0007#\u0002$\u0007$\u0002%\u0007%\u0002&\u0007&\u0002\'\u0007\'\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0003"+
		"\u0000i\b\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0003\u0005\u0003\u008a\b\u0003\n\u0003\f\u0003"+
		"\u008d\t\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0003\u0004\u00a4\b\u0004"+
		"\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005"+
		"\u0001\u0006\u0005\u0006\u00ad\b\u0006\n\u0006\f\u0006\u00b0\t\u0006\u0001"+
		"\u0007\u0005\u0007\u00b3\b\u0007\n\u0007\f\u0007\u00b6\t\u0007\u0001\b"+
		"\u0005\b\u00b9\b\b\n\b\f\b\u00bc\t\b\u0001\t\u0005\t\u00bf\b\t\n\t\f\t"+
		"\u00c2\t\t\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001"+
		"\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0003\n\u00d3"+
		"\b\n\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\f\u0001\f\u0001"+
		"\f\u0005\f\u00dc\b\f\n\f\f\f\u00df\t\f\u0001\r\u0001\r\u0001\r\u0001\r"+
		"\u0003\r\u00e5\b\r\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001"+
		"\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0005\u0010\u00f3\b\u0010\n\u0010\f\u0010\u00f6\t\u0010"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0003\u0011\u0105\b\u0011\u0001\u0012\u0001\u0012\u0001\u0012"+
		"\u0005\u0012\u010a\b\u0012\n\u0012\f\u0012\u010d\t\u0012\u0001\u0013\u0001"+
		"\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0003\u0013\u0115"+
		"\b\u0013\u0001\u0014\u0001\u0014\u0001\u0014\u0005\u0014\u011a\b\u0014"+
		"\n\u0014\f\u0014\u011d\t\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0001"+
		"\u0015\u0001\u0015\u0001\u0015\u0001\u0016\u0001\u0016\u0001\u0016\u0005"+
		"\u0016\u0128\b\u0016\n\u0016\f\u0016\u012b\t\u0016\u0001\u0017\u0001\u0017"+
		"\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0018\u0001\u0018\u0001\u0018"+
		"\u0005\u0018\u0135\b\u0018\n\u0018\f\u0018\u0138\t\u0018\u0001\u0019\u0001"+
		"\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001"+
		"\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001"+
		"\u0019\u0001\u0019\u0003\u0019\u0149\b\u0019\u0001\u001a\u0005\u001a\u014c"+
		"\b\u001a\n\u001a\f\u001a\u014f\t\u001a\u0001\u001a\u0001\u001a\u0005\u001a"+
		"\u0153\b\u001a\n\u001a\f\u001a\u0156\t\u001a\u0001\u001b\u0001\u001b\u0001"+
		"\u001b\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001c\u0001"+
		"\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0001\u001d\u0001\u001d\u0001"+
		"\u001d\u0001\u001d\u0001\u001d\u0001\u001e\u0001\u001e\u0001\u001e\u0005"+
		"\u001e\u016c\b\u001e\n\u001e\f\u001e\u016f\t\u001e\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0003\u001f\u019b\b\u001f"+
		"\u0001 \u0001 \u0005 \u019f\b \n \f \u01a2\t \u0001!\u0001!\u0001!\u0001"+
		"!\u0001!\u0001!\u0001!\u0001!\u0001\"\u0001\"\u0001\"\u0001\"\u0001\""+
		"\u0001\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001"+
		"\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001\"\u0003\"\u01bf\b\"\u0001#\u0001"+
		"#\u0001#\u0005#\u01c4\b#\n#\f#\u01c7\t#\u0001$\u0001$\u0001$\u0001$\u0001"+
		"$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001"+
		"$\u0001$\u0001$\u0001$\u0001$\u0001$\u0001$\u0003$\u01de\b$\u0001%\u0001"+
		"%\u0001&\u0001&\u0001&\u0001&\u0001&\u0001&\u0003&\u01e8\b&\u0001\'\u0001"+
		"\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001"+
		"\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001"+
		"\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001"+
		"\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0003\'\u020e"+
		"\b\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001"+
		"\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0001\'\u0005\'\u021f\b\'\n"+
		"\'\f\'\u0222\t\'\u0001\'\u0000\u0001N(\u0000\u0002\u0004\u0006\b\n\f\u000e"+
		"\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e \"$&(*,.02468:<>@BDF"+
		"HJLN\u0000\u0005\u0001\u0000\"&\u0001\u000012\u0001\u000034\u0001\u0000"+
		"*/\u0001\u0000\'(\u023a\u0000h\u0001\u0000\u0000\u0000\u0002j\u0001\u0000"+
		"\u0000\u0000\u0004{\u0001\u0000\u0000\u0000\u0006\u008b\u0001\u0000\u0000"+
		"\u0000\b\u00a3\u0001\u0000\u0000\u0000\n\u00a5\u0001\u0000\u0000\u0000"+
		"\f\u00ae\u0001\u0000\u0000\u0000\u000e\u00b4\u0001\u0000\u0000\u0000\u0010"+
		"\u00ba\u0001\u0000\u0000\u0000\u0012\u00c0\u0001\u0000\u0000\u0000\u0014"+
		"\u00d2\u0001\u0000\u0000\u0000\u0016\u00d4\u0001\u0000\u0000\u0000\u0018"+
		"\u00d8\u0001\u0000\u0000\u0000\u001a\u00e4\u0001\u0000\u0000\u0000\u001c"+
		"\u00e6\u0001\u0000\u0000\u0000\u001e\u00ea\u0001\u0000\u0000\u0000 \u00ef"+
		"\u0001\u0000\u0000\u0000\"\u0104\u0001\u0000\u0000\u0000$\u0106\u0001"+
		"\u0000\u0000\u0000&\u0114\u0001\u0000\u0000\u0000(\u0116\u0001\u0000\u0000"+
		"\u0000*\u011e\u0001\u0000\u0000\u0000,\u0124\u0001\u0000\u0000\u0000."+
		"\u012c\u0001\u0000\u0000\u00000\u0131\u0001\u0000\u0000\u00002\u0148\u0001"+
		"\u0000\u0000\u00004\u014d\u0001\u0000\u0000\u00006\u0157\u0001\u0000\u0000"+
		"\u00008\u015e\u0001\u0000\u0000\u0000:\u0163\u0001\u0000\u0000\u0000<"+
		"\u0168\u0001\u0000\u0000\u0000>\u019a\u0001\u0000\u0000\u0000@\u019c\u0001"+
		"\u0000\u0000\u0000B\u01a3\u0001\u0000\u0000\u0000D\u01be\u0001\u0000\u0000"+
		"\u0000F\u01c0\u0001\u0000\u0000\u0000H\u01dd\u0001\u0000\u0000\u0000J"+
		"\u01df\u0001\u0000\u0000\u0000L\u01e7\u0001\u0000\u0000\u0000N\u020d\u0001"+
		"\u0000\u0000\u0000PQ\u0003\f\u0006\u0000QR\u0003\u000e\u0007\u0000RS\u0005"+
		"\u000f\u0000\u0000ST\u0005?\u0000\u0000TU\u0005\u0011\u0000\u0000UV\u0005"+
		"\u0012\u0000\u0000VW\u0003\u0012\t\u0000WX\u0005\u0010\u0000\u0000XY\u0005"+
		"\u000f\u0000\u0000YZ\u0005?\u0000\u0000Z[\u0005\u0000\u0000\u0001[i\u0001"+
		"\u0000\u0000\u0000\\]\u0003\u0010\b\u0000]^\u0003\f\u0006\u0000^_\u0003"+
		"\u000e\u0007\u0000_`\u0005\u000f\u0000\u0000`a\u0005?\u0000\u0000ab\u0003"+
		"\u0012\t\u0000bc\u0005\u0010\u0000\u0000cd\u0005\u000f\u0000\u0000de\u0005"+
		"?\u0000\u0000ef\u0003\u0010\b\u0000fg\u0005\u0000\u0000\u0001gi\u0001"+
		"\u0000\u0000\u0000hP\u0001\u0000\u0000\u0000h\\\u0001\u0000\u0000\u0000"+
		"i\u0001\u0001\u0000\u0000\u0000jk\u00059\u0000\u0000kl\u0005?\u0000\u0000"+
		"lm\u0005\u0004\u0000\u0000mn\u00034\u001a\u0000no\u0005\u0005\u0000\u0000"+
		"op\u0005:\u0000\u0000pq\u0005\u0004\u0000\u0000qr\u0003N\'\u0000rs\u0005"+
		"\u0005\u0000\u0000st\u0005\u0011\u0000\u0000tu\u0005\u0012\u0000\u0000"+
		"uv\u0003\u0006\u0003\u0000vw\u0003\u0012\t\u0000wx\u0005\u0010\u0000\u0000"+
		"xy\u00059\u0000\u0000yz\u0005?\u0000\u0000z\u0003\u0001\u0000\u0000\u0000"+
		"{|\u00055\u0000\u0000|}\u0005?\u0000\u0000}~\u0005\u0004\u0000\u0000~"+
		"\u007f\u00034\u001a\u0000\u007f\u0080\u0005\u0005\u0000\u0000\u0080\u0081"+
		"\u0005\u0011\u0000\u0000\u0081\u0082\u0005\u0012\u0000\u0000\u0082\u0083"+
		"\u0003\u0006\u0003\u0000\u0083\u0084\u0003\u0012\t\u0000\u0084\u0085\u0005"+
		"\u0010\u0000\u0000\u0085\u0086\u00055\u0000\u0000\u0086\u0087\u0005?\u0000"+
		"\u0000\u0087\u0005\u0001\u0000\u0000\u0000\u0088\u008a\u0003\b\u0004\u0000"+
		"\u0089\u0088\u0001\u0000\u0000\u0000\u008a\u008d\u0001\u0000\u0000\u0000"+
		"\u008b\u0089\u0001\u0000\u0000\u0000\u008b\u008c\u0001\u0000\u0000\u0000"+
		"\u008c\u0007\u0001\u0000\u0000\u0000\u008d\u008b\u0001\u0000\u0000\u0000"+
		"\u008e\u008f\u0003J%\u0000\u008f\u0090\u0005\b\u0000\u0000\u0090\u0091"+
		"\u00056\u0000\u0000\u0091\u0092\u0005\u0004\u0000\u0000\u0092\u0093\u0005"+
		"7\u0000\u0000\u0093\u0094\u0005\u0005\u0000\u0000\u0094\u0095\u0005\u0001"+
		"\u0000\u0000\u0095\u0096\u0005?\u0000\u0000\u0096\u00a4\u0001\u0000\u0000"+
		"\u0000\u0097\u0098\u0003J%\u0000\u0098\u0099\u0005\b\u0000\u0000\u0099"+
		"\u009a\u00056\u0000\u0000\u009a\u009b\u0005\u0004\u0000\u0000\u009b\u009c"+
		"\u00057\u0000\u0000\u009c\u009d\u0005\u0005\u0000\u0000\u009d\u009e\u0005"+
		"\u0001\u0000\u0000\u009e\u009f\u0005?\u0000\u0000\u009f\u00a0\u0005\u0004"+
		"\u0000\u0000\u00a0\u00a1\u00034\u001a\u0000\u00a1\u00a2\u0005\u0005\u0000"+
		"\u0000\u00a2\u00a4\u0001\u0000\u0000\u0000\u00a3\u008e\u0001\u0000\u0000"+
		"\u0000\u00a3\u0097\u0001\u0000\u0000\u0000\u00a4\t\u0001\u0000\u0000\u0000"+
		"\u00a5\u00a6\u00058\u0000\u0000\u00a6\u00a7\u0005?\u0000\u0000\u00a7\u00a8"+
		"\u0005\u0004\u0000\u0000\u00a8\u00a9\u00034\u001a\u0000\u00a9\u00aa\u0005"+
		"\u0005\u0000\u0000\u00aa\u000b\u0001\u0000\u0000\u0000\u00ab\u00ad\u0003"+
		"\u0004\u0002\u0000\u00ac\u00ab\u0001\u0000\u0000\u0000\u00ad\u00b0\u0001"+
		"\u0000\u0000\u0000\u00ae\u00ac\u0001\u0000\u0000\u0000\u00ae\u00af\u0001"+
		"\u0000\u0000\u0000\u00af\r\u0001\u0000\u0000\u0000\u00b0\u00ae\u0001\u0000"+
		"\u0000\u0000\u00b1\u00b3\u0003\u0002\u0001\u0000\u00b2\u00b1\u0001\u0000"+
		"\u0000\u0000\u00b3\u00b6\u0001\u0000\u0000\u0000\u00b4\u00b2\u0001\u0000"+
		"\u0000\u0000\u00b4\u00b5\u0001\u0000\u0000\u0000\u00b5\u000f\u0001\u0000"+
		"\u0000\u0000\u00b6\u00b4\u0001\u0000\u0000\u0000\u00b7\u00b9\u0005B\u0000"+
		"\u0000\u00b8\u00b7\u0001\u0000\u0000\u0000\u00b9\u00bc\u0001\u0000\u0000"+
		"\u0000\u00ba\u00b8\u0001\u0000\u0000\u0000\u00ba\u00bb\u0001\u0000\u0000"+
		"\u0000\u00bb\u0011\u0001\u0000\u0000\u0000\u00bc\u00ba\u0001\u0000\u0000"+
		"\u0000\u00bd\u00bf\u0003\u0014\n\u0000\u00be\u00bd\u0001\u0000\u0000\u0000"+
		"\u00bf\u00c2\u0001\u0000\u0000\u0000\u00c0\u00be\u0001\u0000\u0000\u0000"+
		"\u00c0\u00c1\u0001\u0000\u0000\u0000\u00c1\u0013\u0001\u0000\u0000\u0000"+
		"\u00c2\u00c0\u0001\u0000\u0000\u0000\u00c3\u00d3\u0003\u0016\u000b\u0000"+
		"\u00c4\u00d3\u0005B\u0000\u0000\u00c5\u00d3\u0003\u001e\u000f\u0000\u00c6"+
		"\u00d3\u0003\u001c\u000e\u0000\u00c7\u00d3\u0003\"\u0011\u0000\u00c8\u00d3"+
		"\u00032\u0019\u0000\u00c9\u00d3\u00036\u001b\u0000\u00ca\u00d3\u0003*"+
		"\u0015\u0000\u00cb\u00d3\u00038\u001c\u0000\u00cc\u00d3\u0003:\u001d\u0000"+
		"\u00cd\u00d3\u0003>\u001f\u0000\u00ce\u00d3\u0003D\"\u0000\u00cf\u00d3"+
		"\u0003H$\u0000\u00d0\u00d3\u0003L&\u0000\u00d1\u00d3\u0003\n\u0005\u0000"+
		"\u00d2\u00c3\u0001\u0000\u0000\u0000\u00d2\u00c4\u0001\u0000\u0000\u0000"+
		"\u00d2\u00c5\u0001\u0000\u0000\u0000\u00d2\u00c6\u0001\u0000\u0000\u0000"+
		"\u00d2\u00c7\u0001\u0000\u0000\u0000\u00d2\u00c8\u0001\u0000\u0000\u0000"+
		"\u00d2\u00c9\u0001\u0000\u0000\u0000\u00d2\u00ca\u0001\u0000\u0000\u0000"+
		"\u00d2\u00cb\u0001\u0000\u0000\u0000\u00d2\u00cc\u0001\u0000\u0000\u0000"+
		"\u00d2\u00cd\u0001\u0000\u0000\u0000\u00d2\u00ce\u0001\u0000\u0000\u0000"+
		"\u00d2\u00cf\u0001\u0000\u0000\u0000\u00d2\u00d0\u0001\u0000\u0000\u0000"+
		"\u00d2\u00d1\u0001\u0000\u0000\u0000\u00d3\u0015\u0001\u0000\u0000\u0000"+
		"\u00d4\u00d5\u0003J%\u0000\u00d5\u00d6\u0005\u0001\u0000\u0000\u00d6\u00d7"+
		"\u0003\u0018\f\u0000\u00d7\u0017\u0001\u0000\u0000\u0000\u00d8\u00dd\u0003"+
		"\u001a\r\u0000\u00d9\u00da\u0005\b\u0000\u0000\u00da\u00dc\u0003\u001a"+
		"\r\u0000\u00db\u00d9\u0001\u0000\u0000\u0000\u00dc\u00df\u0001\u0000\u0000"+
		"\u0000\u00dd\u00db\u0001\u0000\u0000\u0000\u00dd\u00de\u0001\u0000\u0000"+
		"\u0000\u00de\u0019\u0001\u0000\u0000\u0000\u00df\u00dd\u0001\u0000\u0000"+
		"\u0000\u00e0\u00e1\u0005?\u0000\u0000\u00e1\u00e2\u0005\f\u0000\u0000"+
		"\u00e2\u00e5\u0003N\'\u0000\u00e3\u00e5\u0005?\u0000\u0000\u00e4\u00e0"+
		"\u0001\u0000\u0000\u0000\u00e4\u00e3\u0001\u0000\u0000\u0000\u00e5\u001b"+
		"\u0001\u0000\u0000\u0000\u00e6\u00e7\u0005?\u0000\u0000\u00e7\u00e8\u0005"+
		"\f\u0000\u0000\u00e8\u00e9\u0003N\'\u0000\u00e9\u001d\u0001\u0000\u0000"+
		"\u0000\u00ea\u00eb\u0005\u0013\u0000\u0000\u00eb\u00ec\u00051\u0000\u0000"+
		"\u00ec\u00ed\u0005\b\u0000\u0000\u00ed\u00ee\u0003 \u0010\u0000\u00ee"+
		"\u001f\u0001\u0000\u0000\u0000\u00ef\u00f4\u0003N\'\u0000\u00f0\u00f1"+
		"\u0005\b\u0000\u0000\u00f1\u00f3\u0003N\'\u0000\u00f2\u00f0\u0001\u0000"+
		"\u0000\u0000\u00f3\u00f6\u0001\u0000\u0000\u0000\u00f4\u00f2\u0001\u0000"+
		"\u0000\u0000\u00f4\u00f5\u0001\u0000\u0000\u0000\u00f5!\u0001\u0000\u0000"+
		"\u0000\u00f6\u00f4\u0001\u0000\u0000\u0000\u00f7\u00f8\u0003J%\u0000\u00f8"+
		"\u00f9\u0005\b\u0000\u0000\u00f9\u00fa\u0005\u0016\u0000\u0000\u00fa\u00fb"+
		"\u0005\u0004\u0000\u0000\u00fb\u00fc\u0003(\u0014\u0000\u00fc\u00fd\u0005"+
		"\u0005\u0000\u0000\u00fd\u00fe\u0005\u0001\u0000\u0000\u00fe\u00ff\u0003"+
		"$\u0012\u0000\u00ff\u0105\u0001\u0000\u0000\u0000\u0100\u0101\u0003J%"+
		"\u0000\u0101\u0102\u0005\u0001\u0000\u0000\u0102\u0103\u0003$\u0012\u0000"+
		"\u0103\u0105\u0001\u0000\u0000\u0000\u0104\u00f7\u0001\u0000\u0000\u0000"+
		"\u0104\u0100\u0001\u0000\u0000\u0000\u0105#\u0001\u0000\u0000\u0000\u0106"+
		"\u010b\u0003&\u0013\u0000\u0107\u0108\u0005\b\u0000\u0000\u0108\u010a"+
		"\u0003&\u0013\u0000\u0109\u0107\u0001\u0000\u0000\u0000\u010a\u010d\u0001"+
		"\u0000\u0000\u0000\u010b\u0109\u0001\u0000\u0000\u0000\u010b\u010c\u0001"+
		"\u0000\u0000\u0000\u010c%\u0001\u0000\u0000\u0000\u010d\u010b\u0001\u0000"+
		"\u0000\u0000\u010e\u010f\u0005?\u0000\u0000\u010f\u0110\u0005\u0004\u0000"+
		"\u0000\u0110\u0111\u0003(\u0014\u0000\u0111\u0112\u0005\u0005\u0000\u0000"+
		"\u0112\u0115\u0001\u0000\u0000\u0000\u0113\u0115\u0005?\u0000\u0000\u0114"+
		"\u010e\u0001\u0000\u0000\u0000\u0114\u0113\u0001\u0000\u0000\u0000\u0115"+
		"\'\u0001\u0000\u0000\u0000\u0116\u011b\u0003N\'\u0000\u0117\u0118\u0005"+
		"\b\u0000\u0000\u0118\u011a\u0003N\'\u0000\u0119\u0117\u0001\u0000\u0000"+
		"\u0000\u011a\u011d\u0001\u0000\u0000\u0000\u011b\u0119\u0001\u0000\u0000"+
		"\u0000\u011b\u011c\u0001\u0000\u0000\u0000\u011c)\u0001\u0000\u0000\u0000"+
		"\u011d\u011b\u0001\u0000\u0000\u0000\u011e\u011f\u0003J%\u0000\u011f\u0120"+
		"\u0005\b\u0000\u0000\u0120\u0121\u0005\u0018\u0000\u0000\u0121\u0122\u0005"+
		"\u0001\u0000\u0000\u0122\u0123\u0003,\u0016\u0000\u0123+\u0001\u0000\u0000"+
		"\u0000\u0124\u0129\u0003.\u0017\u0000\u0125\u0126\u0005\b\u0000\u0000"+
		"\u0126\u0128\u0003.\u0017\u0000\u0127\u0125\u0001\u0000\u0000\u0000\u0128"+
		"\u012b\u0001\u0000\u0000\u0000\u0129\u0127\u0001\u0000\u0000\u0000\u0129"+
		"\u012a\u0001\u0000\u0000\u0000\u012a-\u0001\u0000\u0000\u0000\u012b\u0129"+
		"\u0001\u0000\u0000\u0000\u012c\u012d\u0005?\u0000\u0000\u012d\u012e\u0005"+
		"\u0004\u0000\u0000\u012e\u012f\u00030\u0018\u0000\u012f\u0130\u0005\u0005"+
		"\u0000\u0000\u0130/\u0001\u0000\u0000\u0000\u0131\u0136\u0005\r\u0000"+
		"\u0000\u0132\u0133\u0005\b\u0000\u0000\u0133\u0135\u0005\r\u0000\u0000"+
		"\u0134\u0132\u0001\u0000\u0000\u0000\u0135\u0138\u0001\u0000\u0000\u0000"+
		"\u0136\u0134\u0001\u0000\u0000\u0000\u0136\u0137\u0001\u0000\u0000\u0000"+
		"\u01371\u0001\u0000\u0000\u0000\u0138\u0136\u0001\u0000\u0000\u0000\u0139"+
		"\u013a\u0005?\u0000\u0000\u013a\u013b\u0005\f\u0000\u0000\u013b\u013c"+
		"\u0005\u0004\u0000\u0000\u013c\u013d\u00052\u0000\u0000\u013d\u013e\u0003"+
		"4\u001a\u0000\u013e\u013f\u00052\u0000\u0000\u013f\u0140\u0005\u0005\u0000"+
		"\u0000\u0140\u0149\u0001\u0000\u0000\u0000\u0141\u0142\u0005?\u0000\u0000"+
		"\u0142\u0143\u0005\t\u0000\u0000\u0143\u0144\u0003N\'\u0000\u0144\u0145"+
		"\u0005\n\u0000\u0000\u0145\u0146\u0005\f\u0000\u0000\u0146\u0147\u0003"+
		"N\'\u0000\u0147\u0149\u0001\u0000\u0000\u0000\u0148\u0139\u0001\u0000"+
		"\u0000\u0000\u0148\u0141\u0001\u0000\u0000\u0000\u01493\u0001\u0000\u0000"+
		"\u0000\u014a\u014c\u0003N\'\u0000\u014b\u014a\u0001\u0000\u0000\u0000"+
		"\u014c\u014f\u0001\u0000\u0000\u0000\u014d\u014b\u0001\u0000\u0000\u0000"+
		"\u014d\u014e\u0001\u0000\u0000\u0000\u014e\u0154\u0001\u0000\u0000\u0000"+
		"\u014f\u014d\u0001\u0000\u0000\u0000\u0150\u0151\u0005\b\u0000\u0000\u0151"+
		"\u0153\u0003N\'\u0000\u0152\u0150\u0001\u0000\u0000\u0000\u0153\u0156"+
		"\u0001\u0000\u0000\u0000\u0154\u0152\u0001\u0000\u0000\u0000\u0154\u0155"+
		"\u0001\u0000\u0000\u0000\u01555\u0001\u0000\u0000\u0000\u0156\u0154\u0001"+
		"\u0000\u0000\u0000\u0157\u0158\u0005?\u0000\u0000\u0158\u0159\u0005\t"+
		"\u0000\u0000\u0159\u015a\u0003(\u0014\u0000\u015a\u015b\u0005\n\u0000"+
		"\u0000\u015b\u015c\u0005\f\u0000\u0000\u015c\u015d\u0003N\'\u0000\u015d"+
		"7\u0001\u0000\u0000\u0000\u015e\u015f\u0005\u0019\u0000\u0000\u015f\u0160"+
		"\u0005\u0004\u0000\u0000\u0160\u0161\u0003$\u0012\u0000\u0161\u0162\u0005"+
		"\u0005\u0000\u0000\u01629\u0001\u0000\u0000\u0000\u0163\u0164\u0005\u001a"+
		"\u0000\u0000\u0164\u0165\u0005\u0004\u0000\u0000\u0165\u0166\u0003<\u001e"+
		"\u0000\u0166\u0167\u0005\u0005\u0000\u0000\u0167;\u0001\u0000\u0000\u0000"+
		"\u0168\u016d\u0005?\u0000\u0000\u0169\u016a\u0005\b\u0000\u0000\u016a"+
		"\u016c\u0005?\u0000\u0000\u016b\u0169\u0001\u0000\u0000\u0000\u016c\u016f"+
		"\u0001\u0000\u0000\u0000\u016d\u016b\u0001\u0000\u0000\u0000\u016d\u016e"+
		"\u0001\u0000\u0000\u0000\u016e=\u0001\u0000\u0000\u0000\u016f\u016d\u0001"+
		"\u0000\u0000\u0000\u0170\u0171\u0005\u001b\u0000\u0000\u0171\u0172\u0005"+
		"\u0004\u0000\u0000\u0172\u0173\u0003N\'\u0000\u0173\u0174\u0005\u0005"+
		"\u0000\u0000\u0174\u0175\u0005\u001c\u0000\u0000\u0175\u0176\u0003\u0012"+
		"\t\u0000\u0176\u0177\u0005\u0010\u0000\u0000\u0177\u0178\u0005\u001b\u0000"+
		"\u0000\u0178\u019b\u0001\u0000\u0000\u0000\u0179\u017a\u0005\u001b\u0000"+
		"\u0000\u017a\u017b\u0005\u0004\u0000\u0000\u017b\u017c\u0003N\'\u0000"+
		"\u017c\u017d\u0005\u0005\u0000\u0000\u017d\u017e\u0005\u001c\u0000\u0000"+
		"\u017e\u017f\u0003\u0012\t\u0000\u017f\u0180\u0005\u001d\u0000\u0000\u0180"+
		"\u0181\u0003\u0012\t\u0000\u0181\u0182\u0005\u0010\u0000\u0000\u0182\u0183"+
		"\u0005\u001b\u0000\u0000\u0183\u019b\u0001\u0000\u0000\u0000\u0184\u0185"+
		"\u0005\u001b\u0000\u0000\u0185\u0186\u0005\u0004\u0000\u0000\u0186\u0187"+
		"\u0003N\'\u0000\u0187\u0188\u0005\u0005\u0000\u0000\u0188\u0189\u0005"+
		"\u001c\u0000\u0000\u0189\u018a\u0003\u0012\t\u0000\u018a\u018b\u0003@"+
		" \u0000\u018b\u018c\u0005\u0010\u0000\u0000\u018c\u018d\u0005\u001b\u0000"+
		"\u0000\u018d\u019b\u0001\u0000\u0000\u0000\u018e\u018f\u0005\u001b\u0000"+
		"\u0000\u018f\u0190\u0005\u0004\u0000\u0000\u0190\u0191\u0003N\'\u0000"+
		"\u0191\u0192\u0005\u0005\u0000\u0000\u0192\u0193\u0005\u001c\u0000\u0000"+
		"\u0193\u0194\u0003\u0012\t\u0000\u0194\u0195\u0003@ \u0000\u0195\u0196"+
		"\u0005\u001d\u0000\u0000\u0196\u0197\u0003\u0012\t\u0000\u0197\u0198\u0005"+
		"\u0010\u0000\u0000\u0198\u0199\u0005\u001b\u0000\u0000\u0199\u019b\u0001"+
		"\u0000\u0000\u0000\u019a\u0170\u0001\u0000\u0000\u0000\u019a\u0179\u0001"+
		"\u0000\u0000\u0000\u019a\u0184\u0001\u0000\u0000\u0000\u019a\u018e\u0001"+
		"\u0000\u0000\u0000\u019b?\u0001\u0000\u0000\u0000\u019c\u01a0\u0003B!"+
		"\u0000\u019d\u019f\u0003B!\u0000\u019e\u019d\u0001\u0000\u0000\u0000\u019f"+
		"\u01a2\u0001\u0000\u0000\u0000\u01a0\u019e\u0001\u0000\u0000\u0000\u01a0"+
		"\u01a1\u0001\u0000\u0000\u0000\u01a1A\u0001\u0000\u0000\u0000\u01a2\u01a0"+
		"\u0001\u0000\u0000\u0000\u01a3\u01a4\u0005\u001d\u0000\u0000\u01a4\u01a5"+
		"\u0005\u001b\u0000\u0000\u01a5\u01a6\u0005\u0004\u0000\u0000\u01a6\u01a7"+
		"\u0003N\'\u0000\u01a7\u01a8\u0005\u0005\u0000\u0000\u01a8\u01a9\u0005"+
		"\u001c\u0000\u0000\u01a9\u01aa\u0003\u0012\t\u0000\u01aaC\u0001\u0000"+
		"\u0000\u0000\u01ab\u01ac\u0005\u001e\u0000\u0000\u01ac\u01ad\u0005?\u0000"+
		"\u0000\u01ad\u01ae\u0005\f\u0000\u0000\u01ae\u01af\u0003F#\u0000\u01af"+
		"\u01b0\u0003\u0012\t\u0000\u01b0\u01b1\u0005\u0010\u0000\u0000\u01b1\u01b2"+
		"\u0005\u001e\u0000\u0000\u01b2\u01bf\u0001\u0000\u0000\u0000\u01b3\u01b4"+
		"\u0005?\u0000\u0000\u01b4\u01b5\u0005\r\u0000\u0000\u01b5\u01b6\u0005"+
		"\u001e\u0000\u0000\u01b6\u01b7\u0005?\u0000\u0000\u01b7\u01b8\u0005\f"+
		"\u0000\u0000\u01b8\u01b9\u0003F#\u0000\u01b9\u01ba\u0003\u0012\t\u0000"+
		"\u01ba\u01bb\u0005\u0010\u0000\u0000\u01bb\u01bc\u0005\u001e\u0000\u0000"+
		"\u01bc\u01bd\u0005?\u0000\u0000\u01bd\u01bf\u0001\u0000\u0000\u0000\u01be"+
		"\u01ab\u0001\u0000\u0000\u0000\u01be\u01b3\u0001\u0000\u0000\u0000\u01bf"+
		"E\u0001\u0000\u0000\u0000\u01c0\u01c5\u0003N\'\u0000\u01c1\u01c2\u0005"+
		"\b\u0000\u0000\u01c2\u01c4\u0003N\'\u0000\u01c3\u01c1\u0001\u0000\u0000"+
		"\u0000\u01c4\u01c7\u0001\u0000\u0000\u0000\u01c5\u01c3\u0001\u0000\u0000"+
		"\u0000\u01c5\u01c6\u0001\u0000\u0000\u0000\u01c6G\u0001\u0000\u0000\u0000"+
		"\u01c7\u01c5\u0001\u0000\u0000\u0000\u01c8\u01c9\u0005\u001e\u0000\u0000"+
		"\u01c9\u01ca\u0005\u001f\u0000\u0000\u01ca\u01cb\u0005\u0004\u0000\u0000"+
		"\u01cb\u01cc\u0003N\'\u0000\u01cc\u01cd\u0005\u0005\u0000\u0000\u01cd"+
		"\u01ce\u0003\u0012\t\u0000\u01ce\u01cf\u0005\u0010\u0000\u0000\u01cf\u01d0"+
		"\u0005\u001e\u0000\u0000\u01d0\u01de\u0001\u0000\u0000\u0000\u01d1\u01d2"+
		"\u0005?\u0000\u0000\u01d2\u01d3\u0005\r\u0000\u0000\u01d3\u01d4\u0005"+
		"\u001e\u0000\u0000\u01d4\u01d5\u0005\u001f\u0000\u0000\u01d5\u01d6\u0005"+
		"\u0004\u0000\u0000\u01d6\u01d7\u0003N\'\u0000\u01d7\u01d8\u0005\u0005"+
		"\u0000\u0000\u01d8\u01d9\u0003\u0012\t\u0000\u01d9\u01da\u0005\u0010\u0000"+
		"\u0000\u01da\u01db\u0005\u001e\u0000\u0000\u01db\u01dc\u0005?\u0000\u0000"+
		"\u01dc\u01de\u0001\u0000\u0000\u0000\u01dd\u01c8\u0001\u0000\u0000\u0000"+
		"\u01dd\u01d1\u0001\u0000\u0000\u0000\u01deI\u0001\u0000\u0000\u0000\u01df"+
		"\u01e0\u0007\u0000\u0000\u0000\u01e0K\u0001\u0000\u0000\u0000\u01e1\u01e8"+
		"\u0005 \u0000\u0000\u01e2\u01e8\u0005!\u0000\u0000\u01e3\u01e4\u0005!"+
		"\u0000\u0000\u01e4\u01e8\u0005?\u0000\u0000\u01e5\u01e6\u0005 \u0000\u0000"+
		"\u01e6\u01e8\u0005?\u0000\u0000\u01e7\u01e1\u0001\u0000\u0000\u0000\u01e7"+
		"\u01e2\u0001\u0000\u0000\u0000\u01e7\u01e3\u0001\u0000\u0000\u0000\u01e7"+
		"\u01e5\u0001\u0000\u0000\u0000\u01e8M\u0001\u0000\u0000\u0000\u01e9\u01ea"+
		"\u0006\'\uffff\uffff\u0000\u01ea\u01eb\u00054\u0000\u0000\u01eb\u020e"+
		"\u0003N\'\u0014\u01ec\u01ed\u0005)\u0000\u0000\u01ed\u020e\u0003N\'\u0013"+
		"\u01ee\u01ef\u0005\u0004\u0000\u0000\u01ef\u01f0\u0003N\'\u0000\u01f0"+
		"\u01f1\u0005\u0005\u0000\u0000\u01f1\u020e\u0001\u0000\u0000\u0000\u01f2"+
		"\u020e\u0005;\u0000\u0000\u01f3\u020e\u0005<\u0000\u0000\u01f4\u020e\u0005"+
		"=\u0000\u0000\u01f5\u020e\u0005>\u0000\u0000\u01f6\u020e\u0005\u0014\u0000"+
		"\u0000\u01f7\u020e\u0005\u0015\u0000\u0000\u01f8\u020e\u0005?\u0000\u0000"+
		"\u01f9\u020e\u0005@\u0000\u0000\u01fa\u01fb\u0005\u0017\u0000\u0000\u01fb"+
		"\u01fc\u0005\u0004\u0000\u0000\u01fc\u01fd\u0005?\u0000\u0000\u01fd\u020e"+
		"\u0005\u0005\u0000\u0000\u01fe\u01ff\u0005?\u0000\u0000\u01ff\u0200\u0005"+
		"\t\u0000\u0000\u0200\u0201\u0003N\'\u0000\u0201\u0202\u0005\n\u0000\u0000"+
		"\u0202\u020e\u0001\u0000\u0000\u0000\u0203\u0204\u0005?\u0000\u0000\u0204"+
		"\u0205\u0005\t\u0000\u0000\u0205\u0206\u0003(\u0014\u0000\u0206\u0207"+
		"\u0005\n\u0000\u0000\u0207\u020e\u0001\u0000\u0000\u0000\u0208\u0209\u0005"+
		"?\u0000\u0000\u0209\u020a\u0005\u0004\u0000\u0000\u020a\u020b\u00034\u001a"+
		"\u0000\u020b\u020c\u0005\u0005\u0000\u0000\u020c\u020e\u0001\u0000\u0000"+
		"\u0000\u020d\u01e9\u0001\u0000\u0000\u0000\u020d\u01ec\u0001\u0000\u0000"+
		"\u0000\u020d\u01ee\u0001\u0000\u0000\u0000\u020d\u01f2\u0001\u0000\u0000"+
		"\u0000\u020d\u01f3\u0001\u0000\u0000\u0000\u020d\u01f4\u0001\u0000\u0000"+
		"\u0000\u020d\u01f5\u0001\u0000\u0000\u0000\u020d\u01f6\u0001\u0000\u0000"+
		"\u0000\u020d\u01f7\u0001\u0000\u0000\u0000\u020d\u01f8\u0001\u0000\u0000"+
		"\u0000\u020d\u01f9\u0001\u0000\u0000\u0000\u020d\u01fa\u0001\u0000\u0000"+
		"\u0000\u020d\u01fe\u0001\u0000\u0000\u0000\u020d\u0203\u0001\u0000\u0000"+
		"\u0000\u020d\u0208\u0001\u0000\u0000\u0000\u020e\u0220\u0001\u0000\u0000"+
		"\u0000\u020f\u0210\n\u0011\u0000\u0000\u0210\u0211\u00050\u0000\u0000"+
		"\u0211\u021f\u0003N\'\u0012\u0212\u0213\n\u0010\u0000\u0000\u0213\u0214"+
		"\u0007\u0001\u0000\u0000\u0214\u021f\u0003N\'\u0011\u0215\u0216\n\u000f"+
		"\u0000\u0000\u0216\u0217\u0007\u0002\u0000\u0000\u0217\u021f\u0003N\'"+
		"\u0010\u0218\u0219\n\u000e\u0000\u0000\u0219\u021a\u0007\u0003\u0000\u0000"+
		"\u021a\u021f\u0003N\'\u000f\u021b\u021c\n\r\u0000\u0000\u021c\u021d\u0007"+
		"\u0004\u0000\u0000\u021d\u021f\u0003N\'\u000e\u021e\u020f\u0001\u0000"+
		"\u0000\u0000\u021e\u0212\u0001\u0000\u0000\u0000\u021e\u0215\u0001\u0000"+
		"\u0000\u0000\u021e\u0218\u0001\u0000\u0000\u0000\u021e\u021b\u0001\u0000"+
		"\u0000\u0000\u021f\u0222\u0001\u0000\u0000\u0000\u0220\u021e\u0001\u0000"+
		"\u0000\u0000\u0220\u0221\u0001\u0000\u0000\u0000\u0221O\u0001\u0000\u0000"+
		"\u0000\u0222\u0220\u0001\u0000\u0000\u0000\u001eh\u008b\u00a3\u00ae\u00b4"+
		"\u00ba\u00c0\u00d2\u00dd\u00e4\u00f4\u0104\u010b\u0114\u011b\u0129\u0136"+
		"\u0148\u014d\u0154\u016d\u019a\u01a0\u01be\u01c5\u01dd\u01e7\u020d\u021e"+
		"\u0220";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}