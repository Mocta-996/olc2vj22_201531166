package Entorno;

public enum Type {
    NULL(0),
    INTEGER(1),
    REAL(2),
    COMPLEX(3),
    CHAR(4),
    LOGICAL(5),
    ARRAY(6),
    SUBRUTINA(7),
    FUNCION(7);

    private final int tipo;
    private Type(int t){
        this.tipo = t;
    }

    public int getTipo() {
        return tipo;
    }
}
