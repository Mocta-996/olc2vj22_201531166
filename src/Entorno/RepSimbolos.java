package Entorno;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static Main.Global.*;

public class RepSimbolos
{
    private String tipo;
    private String id;
    private String linea;
    private String columna;
    private String tipo_dato;
    private String entorno;

    public RepSimbolos(String tipo, String id, String linea, String columna, String tipo_dato, String entorno) {
        this.tipo = tipo;
        this.id = id;
        this.linea = linea;
        this.columna = columna;
        this.tipo_dato = tipo_dato;
        this.entorno = entorno;
    }
    public static  void AgregarTSimbolo(String tipo, String id, String linea, String columna, String tipo_dato, String entorno){

        RepSimbolos s = new RepSimbolos(tipo, id, linea, columna, tipo_dato,entorno);
        _listaSimbolos.add(s);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getColumna() {
        return columna;
    }

    public void setColumna(String columna) {
        this.columna = columna;
    }

    public String getTipo_dato() {
        return tipo_dato;
    }

    public void setTipo_dato(String tipo_dato) {
        this.tipo_dato = tipo_dato;
    }

    public String getEntorno() {
        return entorno;
    }

    public void setEntorno(String entorno) {
        this.entorno = entorno;
    }
}
