package Entorno;

public enum Dato {
    PRIMITIVO(0),
    VARIABLE(1),
    PARAMETRO(2),
    SUBRUTINA(3),
    RETORNO (4),
    FUNCION (5),
    CODIGO (6);
    private final int dato;
    private Dato(int t){
        this.dato = t;
    }

    public int getDato() {
        return dato;
    }
}
