package Entorno;

import java.util.ArrayList;

public class ArrayS {

    private int line;
    private int column;
    private String id;
    public ArrayList value;
    private Type type;
    private Type typeData;
    private int  size1 ;
    private int size2  ;
    private  int dim;
    private   boolean allocatable;
    private  boolean allocate;
    private int pos;

    public ArrayS( int l, int c,String id, ArrayList value, Type t,Type td, int s1, int s2, int dim , boolean allocatable, boolean allocate,int pos) {
        this.id = id;
        this.type = t;
        this.value = value;
        this.typeData = td;
        this.size1 = s1;
        this.size2 = s2;
        this.dim = dim;
        this.line = l;
        this.column = c;
        this.allocatable = allocatable;
        this.allocate = allocate;
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public boolean isAllocatable() {
        return allocatable;
    }

    public void setAllocatable(boolean allocatable) {
        this.allocatable = allocatable;
    }

    public boolean isAllocate() {
        return allocate;
    }

    public void setAllocate(boolean allocate) {
        this.allocate = allocate;
    }
    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(ArrayList value) {
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getTypeData() {
        return typeData;
    }

    public void setTypeData(Type typeData) {
        this.typeData = typeData;
    }

    public int getSize1() {
        return size1;
    }

    public void setSize1(int size1) {
        this.size1 = size1;
    }

    public int getSize2() {
        return size2;
    }

    public void setSize2(int size2) {
        this.size2 = size2;
    }

    public int getDim() {
        return dim;
    }

    public void setDim(int dim) {
        this.dim = dim;
    }


}
