package Entorno;


public class Symbol {
    private int line;
    private int column;
    private String id;
    private Object value;
    private Type type;
    private Dato dato;
    private int pos;


    public Symbol(int l, int c, String id, Object value, Type t,Dato d, int pos) {
        this.line = l;
        this.column = c;
        this.id = id;
        this.type = t;
        this.value = value;
        this.dato = d;
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    public Dato getDato() {
        return dato;
    }

    public void setDato(Dato dato) {
        this.dato = dato;
    }

}
