package Entorno;

public class Exit {
    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private int line;
    private int column;
    private String id;
    private  boolean value;


    public Exit(int l, int c,String id, boolean value) {
        this.line = l;
        this.column = c;
        this.id = id;
        this.value = value;
    }

}
