package Entorno;

public class Cycle {
    private int line;
    private int column;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getIdCiclo() {
        return idCiclo;
    }

    public void setIdCiclo(String idCiclo) {
        this.idCiclo = idCiclo;
    }

    private String idCiclo;


    public Cycle(int l, int c,String id) {
        this.line = l;
        this.column = c;
        this.idCiclo = id;
    }
}
