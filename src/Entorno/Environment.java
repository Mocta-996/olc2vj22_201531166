package Entorno;

import java.util.ArrayList;
import java.util.HashMap;
import Error.Errores;

public class Environment {
    public HashMap<String, Symbol> TablaSimbolo;
    public HashMap<String, ArrayS> TablaSimboloArrays;
    public Environment global;
    public String nombre;
    public Environment siguiente;       // c3d
    public int ul_pos;                  // cod 3d

    public Environment(Environment padre, String nombre) {
        this.global = padre;
        this.nombre = nombre;
        TablaSimbolo = new HashMap<String, Symbol>();
        TablaSimboloArrays = new HashMap<String, ArrayS>();
        this.ul_pos =0;
    }
    /*public static String getNombre() {
        return nombre;
    }*/
    /**
     * SECCION DE SIMBOLOS
     **/
    // agregar nueva variable
    public void nuevoSimbolo(String nombre, Symbol nuevo)
    {
        if (TablaSimbolo.containsKey(nombre.toUpperCase())) {
            // agregar a la lista de error
            //System.out.println("La variable " + nombre + " ya existe.");
        } else {
            TablaSimbolo.put(nombre.toUpperCase(), nuevo);
            //System.out.println("La variable " + nombre + " se agrego con exito.");
        }
    }
    // buscar variable
    public Symbol Buscar(String nombre)
    {
        for (Environment ent = this; ent != null; ent = ent.global) {
            if (ent.TablaSimbolo.containsKey(nombre.toUpperCase()))
                return ent.TablaSimbolo.get(nombre.toUpperCase());
        }
        return null;
    }
    // asignar una variable
    public boolean AsignarVar(String nombre, Symbol valor)
    {
        String noento ="";
        try
        {
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                noento = ent.nombre;
                if (ent.TablaSimbolo.containsKey(nombre.toUpperCase()))
                {
                    if (ent.TablaSimbolo.get(nombre.toUpperCase()).getType() ==valor.getType())
                    {
                        ent.TablaSimbolo.get(nombre.toUpperCase()).setValue(valor.getValue());
                        return true;
                    }
                    /*else if (ent.TablaSimbolo.get(nombre.toUpperCase()).getType() == Type.INTEGER && valor.getType() ==Type.REAL) {
                        Double r = Double.parseDouble(valor.getValue().toString());
                        ent.TablaSimbolo.get(nombre.toUpperCase()).setValue(r.intValue());
                        return true;
                    }else if (ent.TablaSimbolo.get(nombre.toUpperCase()).getType() == Type.REAL && valor.getType() ==Type.INTEGER) {
                        double nuevoval = Double.valueOf(valor.getValue().toString());
                        ent.TablaSimbolo.get(nombre.toUpperCase()).setValue(nuevoval);
                        return true;
                    }*/
                    else
                    {
                        Errores.AgregarError("Ocurrio  un error al asignar la variable ",ent.nombre,String.valueOf(valor.getLine()),String.valueOf(valor.getColumn()));
                        return  true;
                    }

                }
            }
            return false;
        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al asignar valor: diferentes tipos",noento,String.valueOf(valor.getLine()),String.valueOf(valor.getColumn()));
            return false;
        }

    }
    /**
     * SECCION DE ARREGLOS ESTATICOS
     **/
    // agregar nuevo arreglo
    public void nuevoArrayS(String nombre, ArrayS nuevo) {
        if (TablaSimboloArrays.containsKey(nombre.toUpperCase())) {
            // agregar a la lista de error
            System.out.println("La variable " + nombre + " ya existe.");
        } else {
            TablaSimboloArrays.put(nombre.toUpperCase(), nuevo);
            System.out.println("La variable " + nombre + " se agrego con exito.");
        }
    }
    // buscar un arreglo
    public ArrayS BuscarArrays(String nombre) {
        for (Environment ent = this; ent != null; ent = ent.global) {
            if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                return ent.TablaSimboloArrays.get(nombre.toUpperCase());
        }
        return null;
    }

    // ASIGNACION DE ARREGLOS CON VALORES TIPO LISTA   UNIDIMENSIONALES
    // solo aplica para arreglos con una dimension
    public int AsignarArrayS(String nombre, ArrayList<Symbol> values)
    {
        /*
         *   0 = realizado con exito
         *   1 =  array no encontrado
         *   2 = dimension no valido
         *   3 = tamaño fuera de rango
         *   4 = tipo  no coinciden
         *   5 = otr eerror
         * */
        try
        {
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                {
                    // verificar el tipo de arreglo
                    boolean dim = (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 1) ? true : false;
                    int tamOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                    Type tipoOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                    boolean allocatable = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable();
                    // ARREGLO UNIDIMENSIONAL
                    if (dim)
                    {
                        // ARREGLO DINAMICO
                        if (allocatable)
                        {
                            boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                            if(allocate)
                            {
                                // comparar el tamaño
                                if (tamOriginal == values.size())
                                {
                                    // verificar si todos lo valores tiene el mismo tipo
                                    boolean accept = true;
                                    ArrayList<Symbol> cloneValues = new ArrayList<>();
                                    for (Symbol s : values)
                                    {
                                        /*if(tipoOriginal == Type.INTEGER && s.getType() == Type.REAL )
                                        {
                                            Double r = Double.parseDouble((s.getValue().toString()));
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),r.intValue(),
                                                    tipoOriginal,s.getDato(),-1);
                                            cloneValues.add(aux);

                                        }
                                        else if(tipoOriginal == Type.REAL && s.getType() == Type.INTEGER )
                                        {
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),Double.valueOf(s.getValue().toString())
                                                    ,tipoOriginal,s.getDato(),-1);
                                            cloneValues.add(aux);

                                        }*/
                                        if (s.getType() != tipoOriginal)
                                        {
                                            accept = false;
                                            System.out.println(" error de tipos de valores");
                                            break;
                                        }
                                        else
                                        {
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue(),tipoOriginal
                                                    ,s.getDato(),-1);
                                            cloneValues.add(aux);
                                        }
                                    }
                                    if (accept)
                                    {
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(values);
                                        //ArrayList<Symbol> vvv = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        return 0;
                                    }
                                    return 4;
                                }
                                else
                                {
                                    return 3;
                                }

                            }
                            else
                            {
                               return  6;
                            }
                        }
                        // ARREGLO  ESTATICO
                        else
                        {
                            // comparar el tamaño
                            if (tamOriginal == values.size())
                            {
                                // verificar si todos lo valores tiene el mismo tipo
                                boolean accept = true;
                                ArrayList<Symbol> cloneValues = new ArrayList<>();
                                for (Symbol s : values)
                                {
                                    /*if(tipoOriginal == Type.INTEGER && s.getType() == Type.REAL )
                                    {
                                        Double r = Double.parseDouble((s.getValue().toString()));
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),r.intValue(),
                                                tipoOriginal,s.getDato(),-1);
                                        cloneValues.add(aux);

                                    }
                                    else if(tipoOriginal == Type.REAL && s.getType() == Type.INTEGER )
                                    {
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),Double.valueOf(s.getValue().toString())
                                                ,tipoOriginal,s.getDato(),-1);
                                        cloneValues.add(aux);

                                    }*/
                                    if (s.getType() != tipoOriginal)
                                    {
                                        accept = false;
                                        System.out.println(" error de tipos de valores");
                                        break;
                                    }
                                    else
                                    {
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue(),tipoOriginal
                                                ,s.getDato(),-1);
                                        cloneValues.add(aux);
                                    }
                                }
                                if (accept)
                                {
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(cloneValues);
                                    return 0;
                                }
                                return 4;

                            }
                            else
                            {
                                return 3;
                            }
                        }
                    }
                    return 2;
                }
            }
            return 1;
        }
        catch (Exception e)
        {
            System.out.println(e);
            return 5;
        }

    }

    // ASIGNACION DE ARREGLOS CON UNICO VALOR  UNIDIMENSIONALES y BIDIMENSIONALES
    public int AsignarArrayS(String nombre, int[] pos, Symbol value)
    {
        /*
         *   0 = realizado con exito
         *   1 =  array no encontrado
         *   2 = dimension no valido
         *   3 = tamaño fuera de rango
         *   4 = tipo  no coinciden
         *   5 = otr eerror
         * */
        try
        {
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                {

                    boolean allocatable = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable();
                    // DINAMICO
                    if (allocatable)
                    {
                        // verificar el tipo de arreglo
                        boolean dim = (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 1) ? true : false;
                        boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                        Type tipoOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                        if(allocate)
                        {
                            // arreglo unidimensional
                            if (dim && pos.length == 1)
                            {
                                // comparar el tamaño
                                int tamOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                                int posval = pos[0] - 1;
                                if (posval < 0 || posval >= tamOriginal)
                                {
                                    return 3;
                                }
                                else
                                {
                                    // verificar tipo
                                    /*if(tipoOriginal == Type.INTEGER && value.getType() == Type.REAL )
                                    {
                                        Double r = Double.parseDouble((value.getValue().toString()));
                                        Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),r.intValue(),
                                                tipoOriginal,value.getDato(),-1);
                                        ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        update.set(posval,aux);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);

                                    }
                                    else if(tipoOriginal == Type.REAL && value.getType() == Type.INTEGER ){
                                        Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),Double.valueOf(value.getValue().toString())
                                                , tipoOriginal,value.getDato(),-1);
                                        ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        update.set(posval,aux);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);

                                    }*/
                                    if (tipoOriginal == value.getType())
                                    {
                                        Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),value.getValue(),tipoOriginal
                                                ,value.getDato(),-1);
                                        ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        update.set(posval, aux);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                        return 0;
                                    }
                                    else
                                    {
                                        return 4;
                                    }

                                }
                            }// arreglo bidimensional
                            else if (dim == false && pos.length == 2)
                            {
                                int fila = pos[0] - 1;
                                int columna = pos[1] - 1;
                                // comparar el tamaño
                                int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                                int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();

                                if ((fila < 0 || fila >= originalFila) || (columna < 0 || columna >= originalColumna)) {
                                    return 3;
                                }
                                else
                                {
                                    // verificar tipo
                                    if(tipoOriginal == Type.INTEGER && value.getType() == Type.REAL )
                                    {
                                        ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        ArrayList<Symbol> updateFila = update.get(fila);
                                        // copiar el contenido de la fila en una nueva lista
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        for(Symbol s: updateFila)
                                        {
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                    , s.getType(),s.getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        Double r = Double.parseDouble((value.getValue().toString()));
                                        Symbol  aux2= new Symbol(value.getLine(),value.getColumn(),value.getId(),r.intValue(),
                                                tipoOriginal,value.getDato(),-1);

                                        newUpdateFila.set(columna, aux2);
                                        update.set(fila, newUpdateFila);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                        return 0;

                                    }
                                    else if(tipoOriginal == Type.REAL && value.getType() == Type.INTEGER )
                                    {

                                        ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        ArrayList<Symbol> updateFila = update.get(fila);
                                        // copiar el contenido de la fila en una nueva lista
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        for(Symbol s: updateFila)
                                        {
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                    , s.getType(),s.getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        Symbol  aux2= new Symbol(value.getLine(),value.getColumn(),value.getId(),Double.valueOf(value.getValue().toString())
                                                , tipoOriginal,value.getDato(),-1);

                                        newUpdateFila.set(columna, aux2);
                                        update.set(fila, newUpdateFila);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                        return 0;


                                    }
                                    else if (tipoOriginal == value.getType())
                                    {
                                        ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        ArrayList<Symbol> updateFila = update.get(fila);
                                       // copiar el contenido de la fila en una nueva lista
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        for(Symbol s: updateFila)
                                        {
                                            Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                    , s.getType(),s.getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        newUpdateFila.set(columna, value);
                                        update.set(fila, newUpdateFila);
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                        return 0;
                                    }
                                    else
                                    {
                                        return 4;
                                    }
                                }
                            }
                            else
                                return 2;

                        }
                        else
                        {
                            return  6;
                        }

                    }
                    // ESTATICO
                    else
                    {
                        // verificar el tipo de arreglo
                        boolean dim = (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 1) ? true : false;
                        Type tipoOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                        // arreglo unidimensional
                        if (dim && pos.length == 1)
                        {
                            // comparar el tamaño
                            int tamOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                            int posval = pos[0] - 1;
                            if (posval < 0 || posval >= tamOriginal)
                            {
                                return 3;
                            }
                            else
                            {
                                // verificar tipo
                                /*if(tipoOriginal == Type.INTEGER && value.getType() == Type.REAL )
                                {
                                    Double r = Double.parseDouble((value.getValue().toString()));
                                    Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),r.intValue(),
                                            tipoOriginal,value.getDato(),-1);
                                    ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    update.set(posval,aux);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);

                                }
                                else if(tipoOriginal == Type.REAL && value.getType() == Type.INTEGER ){
                                    Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),Double.valueOf(value.getValue().toString())
                                            , tipoOriginal,value.getDato(),-1);
                                    ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    update.set(posval,aux);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);

                                }
                                */
                                if  (tipoOriginal == value.getType())
                                {
                                    Symbol  aux= new Symbol(value.getLine(),value.getColumn(),value.getId(),value.getValue(),tipoOriginal
                                            ,value.getDato(),-1);
                                    ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    update.set(posval, aux);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                    return 0;
                                }
                                else
                                {
                                    return 4;
                                }
                            }
                        }// arreglo bidimensional
                        else if (dim == false && pos.length == 2)
                        {
                            int fila = pos[0] - 1;
                            int columna = pos[1] - 1;
                            // comparar el tamaño
                            int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                            int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();

                            if ((fila < 0 || fila >= originalFila) || (columna < 0 || columna >= originalColumna)) {
                                return 3;
                            }
                            else
                            {
                                // verificar tipo
                                /*if(tipoOriginal == Type.INTEGER && value.getType() == Type.REAL )
                                {
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    ArrayList<Symbol> updateFila = update.get(fila);
                                    // copiar el contenido de la fila en una nueva lista
                                    ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                    for(Symbol s: updateFila)
                                    {
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                , s.getType(),s.getDato(),-1);
                                        newUpdateFila.add(aux);
                                    }
                                    Double r = Double.parseDouble((value.getValue().toString()));
                                    Symbol  aux2= new Symbol(value.getLine(),value.getColumn(),value.getId(),r.intValue(),
                                            tipoOriginal,value.getDato(),-1);

                                    newUpdateFila.set(columna, aux2);
                                    update.set(fila, newUpdateFila);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                    return 0;

                                }
                                else if(tipoOriginal == Type.REAL && value.getType() == Type.INTEGER )
                                {

                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    ArrayList<Symbol> updateFila = update.get(fila);
                                    // copiar el contenido de la fila en una nueva lista
                                    ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                    for(Symbol s: updateFila)
                                    {
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                , s.getType(),s.getDato(),-1);
                                        newUpdateFila.add(aux);
                                    }
                                    Symbol  aux2= new Symbol(value.getLine(),value.getColumn(),value.getId(),Double.valueOf(value.getValue().toString())
                                            , tipoOriginal,value.getDato(),-1);

                                    newUpdateFila.set(columna, aux2);
                                    update.set(fila, newUpdateFila);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                    return 0;


                                }
                                else */
                                if (tipoOriginal == value.getType())
                                {
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    ArrayList<Symbol> updateFila = update.get(fila);
                                    // copiar el contenido de la fila en una nueva lista
                                    ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                    for(Symbol s: updateFila)
                                    {
                                        Symbol  aux= new Symbol(s.getLine(),s.getColumn(),s.getId(),s.getValue()
                                                , s.getType(),s.getDato(),-1);
                                        newUpdateFila.add(aux);
                                    }
                                    newUpdateFila.set(columna, value);
                                    update.set(fila, newUpdateFila);
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                    return 0;
                                }
                                else
                                {
                                    return 4;
                                }
                            }
                        }
                        else
                            return 2;
                    }
                }
            }
            return 1;
        }
        catch (Exception e)
        {
            System.out.println(e);
            return 5;
        }
    }

    // ASIGNACION DE ARRAYS BIDIMENSIONALES POR ID
    public Object AsignarArraySBD(String nombre, ArrayS arrS,String nEntorno,String linea, String columna)
    {
        try {
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                {

                    boolean allocatable = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable();
                    // DINAMICO
                    if (allocatable)
                    {
                        // verificar el tipo de arreglo
                        int  dim = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim();
                        boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                        Type tipoOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                        if(allocate)
                        {
                            // COMPROBAR SI SON DE LA MISMA DIMENSION Y DE DOS DIMENSIONES
                            if (dim == 2 && dim == arrS.getDim())
                            {
                                // comparar el tamaño
                                int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                                int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();

                                // verificar el tamaño de filas y columnas
                                if (originalColumna == arrS.getSize2() && originalFila == arrS.getSize1())
                                {
                                    // verificar tipo
                                    /*if(tipoOriginal == Type.INTEGER && arrS.getTypeData() == Type.REAL )
                                    {
                                        // obtener los valores del array a copiar
                                        ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        //  crear nueva lista
                                        ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                        // recorrer la lista a copiar
                                        for(ArrayList<Symbol> up : update){
                                            // recorrer cada fila
                                            ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                            for(Symbol colum: up){
                                                Double r = Double.parseDouble((colum.getValue().toString()));
                                                Symbol  aux= new Symbol(colum.getLine(),colum.getColumn(),colum.getId(),r.intValue()
                                                        , tipoOriginal,colum.getDato(),-1);
                                                newUpdateFila.add(aux);
                                            }
                                            newArray.add(newUpdateFila);
                                        }
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                        return 0;

                                    }
                                    else if(tipoOriginal == Type.REAL && arrS.getType() == Type.INTEGER )
                                    {
                                            // obtener los valores del array a copiar
                                            ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                            //  crear nueva lista
                                            ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                            // recorrer la lista a copiar
                                            for(ArrayList<Symbol> up : update){
                                                // recorrer cada fila
                                                ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                                for(Symbol colum: up){
                                                    Symbol  aux= new Symbol(colum.getLine(),colum.getColumn(),colum.getId(),Double.valueOf(colum.getValue().toString())
                                                            , tipoOriginal,colum.getDato(),-1);
                                                    newUpdateFila.add(aux);
                                                }
                                                newArray.add(newUpdateFila);
                                            }
                                            ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                            return 0;
                                    }
                                    else*/
                                    if (tipoOriginal == arrS.getTypeData())
                                    {
                                        // obtener los valores del array a copiar
                                        ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                        //  crear nueva lista
                                        ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                        // lista por parametro
                                        ArrayList<ArrayList<Symbol>> arraValue = (ArrayList<ArrayList<Symbol>>) arrS.getValue();
                                        // recorrer la lista a copiar
                                        for(int i=0 ;i< update.size();i++)
                                        {
                                            ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                            ArrayList<Symbol> up = update.get(i);
                                            ArrayList<Symbol> copy = arraValue.get(i);
                                            for (int j=0;j< up.size();j++)
                                            {
                                                Symbol  aux= new Symbol(up.get(j).getLine(),up.get(j).getColumn(),up.get(j).getId(),copy.get(j).getValue()
                                                        , tipoOriginal,up.get(j).getDato(),-1);
                                                newUpdateFila.add(aux);
                                            }
                                            newArray.add(newUpdateFila);
                                        }
                                        /*for(ArrayList<Symbol> up : update){
                                            // recorrer cada fila

                                            for(Symbol colum: up){
                                                Symbol  aux= new Symbol(colum.getLine(),colum.getColumn(),colum.getId(),colum.getValue()
                                                        , tipoOriginal,colum.getDato(),-1);
                                                newUpdateFila.add(aux);
                                            }
                                            newArray.add(newUpdateFila);
                                        }*/
                                        ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                        return 0;
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " tipos no coinciden",
                                                nEntorno,linea,columna);
                                        return null;
                                    }
                                }
                                else
                                {
                                    Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " dimensiones no coinciden",
                                            nEntorno,linea,columna);
                                    return null;
                                }
                            }
                            else
                            {
                                Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " no es bidimensional",
                                        nEntorno,linea,columna);
                                return null;
                            }
                        }
                        else
                        {
                            Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " no tiene asignado dimensiones",
                                    nEntorno,linea,columna);
                            return null;
                        }
                    }
                    // ESTATICO
                    else
                    {
                        // verificar el tipo de arreglo
                        int  dim = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim();
                        boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                        Type tipoOriginal = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                        // COMPROBAR SI SON DE LA MISMA DIMENSION Y DE DOS DIMENSIONES
                        if (dim == 2 && dim == arrS.getDim())
                        {
                            // comparar el tamaño
                            int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                            int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();

                            // verificar el tamaño de filas y columnas
                            if (originalColumna == arrS.getSize2() && originalFila == arrS.getSize1())
                            {
                                // verificar tipo
                                /*if(tipoOriginal == Type.INTEGER && arrS.getTypeData() == Type.REAL )
                                {
                                    // obtener los valores del array a copiar
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    //  crear nueva lista
                                    ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                    // recorrer la lista a copiar
                                    for(ArrayList<Symbol> up : update){
                                        // recorrer cada fila
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        for(Symbol colum: up){
                                            Double r = Double.parseDouble((colum.getValue().toString()));
                                            Symbol  aux= new Symbol(colum.getLine(),colum.getColumn(),colum.getId(),r.intValue()
                                                    , tipoOriginal,colum.getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        newArray.add(newUpdateFila);
                                    }
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                    return 0;

                                }
                                else if(tipoOriginal == Type.REAL && arrS.getType() == Type.INTEGER )
                                {
                                    // obtener los valores del array a copiar
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    //  crear nueva lista
                                    ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                    // recorrer la lista a copiar
                                    for(ArrayList<Symbol> up : update){
                                        // recorrer cada fila
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        for(Symbol colum: up){
                                            Symbol  aux= new Symbol(colum.getLine(),colum.getColumn(),colum.getId(),Double.valueOf(colum.getValue().toString())
                                                    , tipoOriginal,colum.getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        newArray.add(newUpdateFila);
                                    }
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                    return 0;
                                }
                                else */
                                if (tipoOriginal == arrS.getTypeData())
                                {
                                    // obtener los valores del array a copiar
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    //  crear nueva lista
                                    ArrayList<ArrayList<Symbol>> newArray =new ArrayList<>();
                                    // lista por parametro
                                    ArrayList<ArrayList<Symbol>> arraValue = (ArrayList<ArrayList<Symbol>>) arrS.getValue();
                                    // recorrer la lista a copiar
                                    for(int i=0 ;i< update.size();i++)
                                    {
                                        ArrayList <Symbol> newUpdateFila = new ArrayList<>();
                                        ArrayList<Symbol> up = update.get(i);
                                        ArrayList<Symbol> copy = arraValue.get(i);
                                        for (int j=0;j< up.size();j++)
                                        {
                                            Symbol  aux= new Symbol(up.get(j).getLine(),up.get(j).getColumn(),up.get(j).getId(),copy.get(j).getValue()
                                                    , tipoOriginal,up.get(j).getDato(),-1);
                                            newUpdateFila.add(aux);
                                        }
                                        newArray.add(newUpdateFila);
                                    }
                                    ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(newArray);
                                    return true;
                                }
                                else
                                {
                                    Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " tipos  no coinciden",
                                            nEntorno,linea,columna);
                                    return null;
                                }
                            }
                            else
                            {
                                Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " tamaño de dimensiones no coinciden",
                                        nEntorno,linea,columna);
                                return null;
                            }
                        }
                        else
                        {
                            Errores.AgregarError("Error al asingar arreglo: "+ nombre+ " no es bidimensional",
                                    nEntorno,linea,columna);
                            return null;
                        }

                    }

                }
            }
            Errores.AgregarError("Error al asingar arreglo: "+ nombre+ ", no encontrado",
                    nEntorno,linea,columna);
            return null;

        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Ocurrio un error al asingar el  arreglo: "+ nombre ,
                    nEntorno,linea,columna);
            return null;
        }
    }

    // OBTENER VALOR DE UNA POSICION DE UN ARREGLO UNIDIMENSIONAL
    public Symbol ObtenerValorArrayS(String nombre, int pos) {
        try {
            for (Environment ent = this; ent != null; ent = ent.global) {
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase())) {
                    // ARREGLO DINAMICO
                    if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable()) {
                        boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                        if(allocate){
                            if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 1) {
                                int tamArr = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                                if ((pos - 1) < tamArr) {
                                    ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    return update.get(pos - 1);
                                    //ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                } else
                                    return null;
                            } else
                                return null;
                        }else{
                            ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                            return update.get(0);
                        }
                    }// ARREGLO ESTATICO
                    else {
                        if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 1) {
                            int tamArr = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                            if ((pos - 1) < tamArr) {
                                ArrayList<Symbol> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                return update.get(pos - 1);
                                //ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                            } else
                                return null;
                        } else
                            return null;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    // OBTENER VALOR DE UNA POSICION DE UN ARREGLO BIDIMENSIONAL
    public Symbol ObtenerValorArraySBid(String nombre, int[] pos) {
        try {
            for (Environment ent = this; ent != null; ent = ent.global) {
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase())) {
                    // ARREGLO DINAMICO
                    if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable()) {
                        boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                        if(allocate){
                            if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 2) {
                                int fila = pos[0] - 1;
                                int columna = pos[1] - 1;
                                // comparar el tamaño
                                int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                                int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();
                                if (fila >= originalFila || columna >= originalColumna) {
                                    return null;
                                } else {
                                    ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                    ArrayList<Symbol> updateFila = update.get(fila);
                                    return updateFila.get(columna);
                                }
                            } else
                                return null;
                        }else{
                            ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                            ArrayList<Symbol> updateFila = update.get(0);
                            return updateFila.get(0);
                        }
                    }// ARREGLO ESTATICO
                    else {
                        if (ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim() == 2) {
                            int fila = pos[0] - 1;
                            int columna = pos[1] - 1;
                            // comparar el tamaño
                            int originalFila = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize1();
                            int originalColumna = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getSize2();

                            if (fila >= originalFila || columna >= originalColumna) {
                                return null;
                            } else {
                                ArrayList<ArrayList<Symbol>> update = (ArrayList) ent.TablaSimboloArrays.get(nombre.toUpperCase()).getValue();
                                ArrayList<Symbol> updateFila = update.get(fila);
                                return updateFila.get(columna);
                            }
                        } else
                            return null;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    /**
     * SECCION DE ARREGLOS DINAMICOS
     **/
    // FUNCION ALLOCATE
    public void FuncAllocate(String nombre, ArrayList<Integer> dimensiones, String linea,String columna) {
        String entnombre = "";
        try
        {
            boolean validar = false;
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                entnombre = ent.nombre;
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                {
                    entnombre = ent.nombre;
                    boolean allocatable = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable();
                    boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                    int dim = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim();
                    // DINAMICO
                    if(allocatable)
                    {
                        if(!allocate)
                        {
                            //unidimensional
                            if(dim == 1 && dim == dimensiones.size())
                            {
                                int fila = dimensiones.get(0);
                                Type tipo = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                                ArrayList<Symbol> update = new ArrayList<>();
                                if(Type.INTEGER == tipo)
                                {
                                    for(int i=0; i< fila;i++)
                                    {
                                        update.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                    }
                                }
                                else if(Type.REAL == tipo)
                                {
                                    for(int i=0; i< fila;i++)
                                    {
                                        update.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                    }
                                }
                                else
                                {
                                    for(int i=0; i< fila;i++)
                                    {
                                        update.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                    }
                                }
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize1(fila);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setAllocate(true);
                                validar = true;
                                break;

                            }// bidimensional
                            else if(dim == 2 && dim == dimensiones.size())
                            {
                                int fila = dimensiones.get(0);
                                int col = dimensiones.get(1);
                                Type tipo = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                                ArrayList<ArrayList<Symbol>>  update = new ArrayList<>();
                                if(Type.INTEGER == tipo)
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    for (int i =0 ;i< col;i++)
                                    {
                                        newArray2.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                    }
                                    for (int i =0 ;i< fila;i++)
                                    {
                                        update.add(newArray2);
                                    }
                                }
                                else if(Type.REAL == tipo)
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    for (int i =0 ;i< col;i++)
                                    {
                                        newArray2.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                    }
                                    for (int i =0 ;i< fila;i++)
                                    {
                                        update.add(newArray2);
                                    }
                                }
                                else
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    for (int i =0 ;i< col;i++)
                                    {
                                        newArray2.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                    }
                                    for (int i =0 ;i< fila;i++)
                                    {
                                        update.add(newArray2);
                                    }
                                }
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize1(fila);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize2(col);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setAllocate(true);
                                validar = true;
                                break;

                            }
                            else
                            {
                                Errores.AgregarError("Error Allocate, dimensiones incorrectas para el arreglo: "+nombre,entnombre,linea,columna);
                                break;
                            }

                        }else{
                            Errores.AgregarError("Error Allocate, el arreglo: "+nombre+" ya esta asignado ",entnombre,linea,columna);
                            break;
                        }

                    } // ESTATICO
                    else {
                        Errores.AgregarError("Error al asingar dimension: arreglo estatico",entnombre,linea,columna);
                        break;
                    }
                }
            }
            if(!validar){
                Errores.AgregarError("Arregeglo no encontrado: " + nombre,entnombre,linea,columna);
            }

        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al asingar dimension del  arreglo: " + nombre, entnombre, linea, columna);
        }
    }

    // FUNCION DEALLOCATE
    public void FuncDeAllocate(String nombre, String linea,String columna)
    {
        String entnombre = "";
        try
        {
            boolean validar = false;
            for (Environment ent = this; ent != null; ent = ent.global)
            {
                entnombre = ent.nombre;
                if (ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                {
                    entnombre = ent.nombre;
                    boolean allocatable = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocatable();
                    boolean allocate = ent.TablaSimboloArrays.get(nombre.toUpperCase()).isAllocate();
                    int dim = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getDim();
                    // DINAMICO
                    if(allocatable)
                    {
                        if(allocate)
                        {
                            //unidimensional
                            if(dim == 1 )
                            {
                                Type tipo = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                                ArrayList<Symbol> update = new ArrayList<>();
                                if(Type.INTEGER == tipo)
                                {
                                    update.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                }
                                else if(Type.REAL == tipo)
                                {
                                    update.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                }else
                                {
                                    update.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                }
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize1(0);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setAllocate(false);
                                validar = true;
                                break;

                            }// bidimensional
                            else if(dim == 2 )
                            {

                                Type tipo = ent.TablaSimboloArrays.get(nombre.toUpperCase()).getTypeData();
                                ArrayList<ArrayList<Symbol>>  update = new ArrayList<>();
                                if(Type.INTEGER == tipo)
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                    update.add(newArray2);
                                }
                                else if(Type.REAL == tipo)
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                    update.add(newArray2);
                                }
                                else
                                {
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                    update.add(newArray2);
                                }
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setValue(update);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize1(0);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setSize2(0);
                                ent.TablaSimboloArrays.get(nombre.toUpperCase()).setAllocate(false);
                                validar = true;
                                break;
                            }
                        }
                        else
                        {
                            Errores.AgregarError("Error Allocate, el arreglo: "+nombre+" ya esta desasignado ",entnombre,linea,columna);
                            break;
                        }

                    } // ESTATICO
                    else
                    {
                        Errores.AgregarError("Error al desasingar dimension: arreglo estatico",entnombre,linea,columna);
                        break;
                    }
                }
            }
            if(!validar)
            {
                Errores.AgregarError("Arregeglo no encontrado: " + nombre,entnombre,linea,columna);
            }

        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al desasingar dimension del  arreglo: " + nombre, entnombre, linea, columna);
        }
    }

    public int ObtenerTamanio(String nombre)
    {
        int size = 0;
        boolean acceder = false;
        for (Environment ent = this; ent != null; ent = ent.global) {
            if(acceder){
                size += ent.TablaSimbolo.size();
                size += ent.TablaSimboloArrays.size();
            }
            if (ent.TablaSimbolo.containsKey(nombre.toUpperCase()))
                acceder = true;
            else if(ent.TablaSimboloArrays.containsKey(nombre.toUpperCase()))
                acceder = true;


        }
        return size;
    }
    public int ObtenerTamanioAsignar()
    {
        int size = 0;
        for (Environment ent = this.global; ent != null; ent = ent.global) {
            size += ent.TablaSimbolo.size();
            size += ent.TablaSimboloArrays.size();
        }
        return size;
    }
}
