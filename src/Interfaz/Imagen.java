package Interfaz;

import javax.swing.*;
import java.awt.*;

public class Imagen extends javax.swing.JPanel {
    String p ="";
    public Imagen(String path)
    {
        p = path;
        this.setSize(300, 400);
    }

    public void paint(Graphics grafico) {
        Dimension height = getSize();
        ImageIcon img = new ImageIcon(getClass().getResource("/p29KJ.svg"));
        grafico.drawImage(img.getImage(), 0, 0, height.width, height.height, null);
        setOpaque(false);
        super.paintComponent(grafico);
    }
}
