package Interfaz;

import Entorno.Environment;
import Main.Global;
import Main.NodoAst;
import Main.Visitor;
import grammar.JGrammarLexer;
import grammar.JGrammarParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;
import Error.Errores;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class Interpretar {
        static ArrayList<String> LineNum = new ArrayList<String>();
        static ArrayList<String> Type = new ArrayList<String>();
        static ArrayList<String> Content = new ArrayList<String>();
        static ArrayList <NodoAst> nodoAst = new ArrayList<>();
        static ArrayList<String> producciones = new ArrayList<String>();
    public static void Iniciar(String entrada){
        Global.consola ="";
        Global._listaSimbolos.clear();
        Global._errores.clear();
        Global._erroresLexicos.clear();
        Global._erroresSintacticos.clear();
        Global.codigoGraphviz ="";
        Global.consola ="Salida Interprete:\n";
        Global.stackC3d = null;
        Global.c3d = null;

        LineNum.clear();
        Type.clear();
        Content.clear();
        CharStream cs = fromString(entrada);
        JGrammarLexer lexico = new JGrammarLexer(cs);
        lexico.removeErrorListeners();
        lexico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                Errores.AgregarErrorLexico(msg,"", String.valueOf(line),String.valueOf(charPositionInLine));
            }
        });
        CommonTokenStream tokens = new CommonTokenStream(lexico);

        JGrammarParser parser = new JGrammarParser(tokens);
       parser.removeErrorListeners();
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                Errores.AgregarErrorSintactico(msg,"", String.valueOf(line),String.valueOf(charPositionInLine));
            }
        });

        JGrammarParser.StartContext startCtx = parser.start();
        Environment inicio = new Environment(null, "global");
        Visitor visitor = new Visitor(inicio);
        visitor.visit(startCtx);

        // pasar como parametros global: visitor , startCtx
        Global.c3d = startCtx;
        Global.stackC3d = visitor.stackEnvC3D;
        // agregando arbol cst
        List<String> rulesName = Arrays.asList(parser.getRuleNames());
        Global.cst = new TreeViewer(rulesName,startCtx);

        //generateAST(startCtx, false, 0);
        //printDOT();

    }
    /**
     * Generar codigo 3 direcciones
     * */

    private static void generateAST(RuleContext ctx, boolean verbose, int indentation) {
        boolean toBeIgnored = !verbose && ctx.getChildCount() == 1 && ctx.getChild(0) instanceof ParserRuleContext;

        if (!toBeIgnored) {
            String ruleName = JGrammarParser.ruleNames[ctx.getRuleIndex()];
            LineNum.add(Integer.toString(indentation));
            Type.add(ruleName);
            Content.add(ctx.getText());
        }
        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree element = ctx.getChild(i);
            if (element instanceof RuleContext) {
                generateAST((RuleContext) element, verbose, indentation + (toBeIgnored ? 0 : 1));
            }
        }
    }
    private static void printDOT(){
        printLabel();
        int pos = 0;
        for(int i=1; i<LineNum.size();i++){
            pos=getPos(Integer.parseInt(LineNum.get(i))-1, i);
            //System.out.println((Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i);
            producciones.add((Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i );
            Global.codigoGraphviz += (Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i+"\n";
        }
    }
    private static void printLabel(){
        for(int i =0; i<LineNum.size(); i++){
            //System.out.println(LineNum.get(i)+i+"[label=\""+Type.get(i)+"\n "+Content.get(i)+"\"]");
            Global.codigoGraphviz += LineNum.get(i)+i+"[label=\""+Type.get(i)+"\n "+Content.get(i)+"\"]\n";
            nodoAst.add(new NodoAst(LineNum.get(i)+i,Type.get(i),Content.get(i)));
        }
    }
    private static int getPos(int n, int limit){
        int pos = 0;
        for(int i=0; i<limit;i++){
            if(Integer.parseInt(LineNum.get(i))==n){
                pos = i;
            }
        }
        return pos;
    }
    public void filtro (){
        // verifivar los de tipo expresion:
        //si son operaciones aritmeticas
        for(NodoAst n: nodoAst){
            // verificar que sea de tipo expresion
        }

    }


}
