package Interfaz;

//import static Data.Data.nom_archivo;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import C3D.Visitor.VisitorC3D;
import Main.GeneradorId;
import Main.Global;
/**
 *
 * @author Pilo
 */
public class Interfaz extends javax.swing.JFrame {
    public Interfaz()
    {
        initComponents();
        this.setSize(853, 705);
        this.setMinimumSize(new Dimension(448, 490));
        this.setTitle("Fortran Analyzer");
        nuevaPestaña();
        Console.setText("Fortran Analizer Interprete:");
        ConsoleC3D.setText("Fortran Analizer C3D:");
    }

    private void initComponents()
    {
        // seccion de botones
        contenedorbt = new javax.swing.JToolBar();
        Runbt = new javax.swing.JButton();
        Runc3dbt = new javax.swing.JButton();
        TbSimbolobt =new javax.swing.JButton();
        Erroresbt = new javax.swing.JButton();
        Astbt = new javax.swing.JButton();
        Treecstbt = new javax.swing.JButton();
        Limpiarbt = new javax.swing.JButton();
        // seccion de  ventanas
        jSplitPane1 = new javax.swing.JSplitPane();
        pestañas = new javax.swing.JTabbedPane();
        ScrollConsole = new javax.swing.JScrollPane();
        Console = new javax.swing.JTextPane();

        jSplitPane2 = new javax.swing.JSplitPane() ;
        ConsoleC3D =new javax.swing.JTextPane();
        ScrollConsoleC3D =new javax.swing.JScrollPane();

        // seccion de menu
        MenuHead = new javax.swing.JMenuBar();
        MenuArchivo = new javax.swing.JMenu();
        itemArbrir = new javax.swing.JMenuItem();
        itemGuardar = new javax.swing.JMenuItem();
        itemGuardarComo = new javax.swing.JMenuItem();

        // inicio
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        contenedorbt.setRollover(true);
        // BOTON RUN

        Runbt.setText("Run");
        Runbt.setToolTipText("Run");
        Runbt.setFocusable(false);
        Runbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Runbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Runbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RunbtActionPerformed(evt);
            }
        });
        contenedorbt.add(Runbt);
        // GEnerar codigo 3 direcciones
        Runc3dbt.setText("Run C3D");
        Runc3dbt.setToolTipText("Run C3D");
        Runc3dbt.setFocusable(false);
        Runc3dbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Runc3dbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Runc3dbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                Runc3dbtActionPerformed(evt);

            }
        });
        contenedorbt.add(Runc3dbt);

        // BOTON DE REPORTE DE TABLA DE SIMBOLOS
        TbSimbolobt.setText("Simbolos");
        TbSimbolobt.setToolTipText("Simbolos");
        TbSimbolobt.setFocusable(false);
        TbSimbolobt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TbSimbolobt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        TbSimbolobt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TbSimbolobtActionPerformed(evt);
            }
        });
        contenedorbt.add(TbSimbolobt);

        // BOTON DE REPORTE DEL ARBOL CST
        Treecstbt.setText("Tree CST");
        Treecstbt.setToolTipText("Tree CST");
        Treecstbt.setFocusable(false);
        Treecstbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Treecstbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Treecstbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TreecstbtActionPerformed(evt);
            }
        });
        contenedorbt.add(Treecstbt);


        // BOTON ERRORES
        Erroresbt.setText("Errores");
        Erroresbt.setToolTipText("Errores");
        Erroresbt.setFocusable(false);
        Erroresbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Erroresbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Erroresbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ErroresActionPerformed(evt);
            }
        });
        contenedorbt.add(Erroresbt);
        // BOTON REPORTE AST
        Astbt.setText("AST");
        Astbt.setToolTipText("AST");
        Astbt.setFocusable(false);
        Astbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Astbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Astbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AstbtActionPerformed(evt);
            }
        });
        contenedorbt.add(Astbt);

        // BOTON LIMPIAR PANTALLA
        Limpiarbt.setText("CLS");
        Limpiarbt.setToolTipText("CLS");
        Limpiarbt.setFocusable(false);
        Limpiarbt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Limpiarbt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        Limpiarbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarbtActionPerformed(evt);
            }
        });
        contenedorbt.add(Limpiarbt);

        getContentPane().add(contenedorbt, java.awt.BorderLayout.PAGE_START);
        // DIVISION DE PANTALLAS CONSOLA y C3D
        jSplitPane2.setDividerLocation(100);
        jSplitPane2.setDividerSize(5);
        jSplitPane2.setOrientation(JSplitPane.VERTICAL_SPLIT);

        ConsoleC3D.setFont(new java.awt.Font("Monospaced", 0, 16)); // NOI18N
        ConsoleC3D.setBackground(Color.BLACK);
        ConsoleC3D.setForeground(Color.GREEN);
        ScrollConsoleC3D.setViewportView(ConsoleC3D);
        jSplitPane2.setRightComponent(ScrollConsoleC3D);

        Console.setFont(new java.awt.Font("Monospaced", 0, 16)); // NOI18N
        Console.setBackground(Color.BLACK);
        Console.setForeground(Color.GREEN);
        ScrollConsole.setViewportView(Console);
        jSplitPane2.setLeftComponent(ScrollConsole);

        // DIVISION DE PANTALLAS
        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setDividerSize(5);
        jSplitPane1.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane1.setLastDividerLocation((this.getSize().height*70)/100);
        jSplitPane1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                ResizeJS1(evt);
            }
        });
        jSplitPane1.setLeftComponent(pestañas);
        jSplitPane1.setRightComponent(jSplitPane2);
        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        //pantalla Original
        /*// DIVISION DE PANTALLAS
        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setDividerSize(5);
        jSplitPane1.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane1.setLastDividerLocation((this.getSize().height*70)/100);
        jSplitPane1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                ResizeJS1(evt);
            }
        });
        jSplitPane1.setLeftComponent(pestañas);

        Console.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        Console.setBackground(Color.BLACK);
        Console.setForeground(Color.GREEN);
        ScrollConsole.setViewportView(Console);
        jSplitPane1.setRightComponent(ScrollConsole);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);*/

        //SECCION DE ARCHIVO
        MenuArchivo.setText("Archivo");
        itemArbrir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        itemArbrir.setText("Abrir");
        itemArbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemArbrirActionPerformed(evt);
            }
        });
        MenuArchivo.add(itemArbrir);
        itemGuardar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        itemGuardar.setText("Guardar");
        itemGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemGuardarActionPerformed(evt);
            }
        });
        MenuArchivo.add(itemGuardar);

        itemGuardarComo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        itemGuardarComo.setText("Guardar Como");
        itemGuardarComo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemGuardarComoActionPerformed(evt);
            }
        });
        MenuArchivo.add(itemGuardarComo);
        MenuHead.add(MenuArchivo);
        setJMenuBar(MenuHead);

        pack();
    }

    //ABRIR
    private void itemArbrirActionPerformed(java.awt.event.ActionEvent evt) {
        //AbrirArchivo();
    }
    //GUARDAR
    private void itemGuardarActionPerformed(java.awt.event.ActionEvent evt)
    {
        //GuardarArchivo();
    }
    //GUARDARCOMO
    private void itemGuardarComoActionPerformed(java.awt.event.ActionEvent evt)
    {
        //GuardarComoArchivo();
    }
    private void ResizeJS1(java.awt.event.ComponentEvent evt)
    {
        jSplitPane1.setDividerLocation((this.getSize().height * 70) / 100);
    }
    //VER ERRORES
    private void ErroresActionPerformed(java.awt.event.ActionEvent evt)
    {

        new ReporteErrores().setVisible(true);


    }
    // INTERPRETAR
    private void RunbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        RunCode();

    }
    // GERERAR CODIGO 3 DIRECCIONES  Runc3dbtActionPerformed
    private void Runc3dbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        RunC3D();

    }
    // MOSTRAR TABLA DE SIMBOLOS
    private void  TbSimbolobtActionPerformed(java.awt.event.ActionEvent evt)
    {
        new ReporteSimbolos().setVisible(true);

    }
    // MOSTRAR CST
    private void  TreecstbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        if(Global.cst != null){
            new Cst().setVisible(true);
        }
        else {
            JOptionPane.showMessageDialog(this, "No se ha ejecutado Código !");
        }
    }
    // REPORTE DE ERRORES
    private void ErroresbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        // TODO add your handling code here:
        new ReporteErrores().setVisible(true);
    }
    // GENERAR AST
    private void AstbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        String aleatorio = GeneradorId.getPassword();
        new ReporteAst(aleatorio);
       // System.out.println( );

    }
    private void LimpiarbtActionPerformed(java.awt.event.ActionEvent evt)
    {
        Console.setText("Fortran Analizer Interprete:");
        ConsoleC3D.setText("Fortran Analizer C3D:");

    }


    /**
     * @param args the command line arguments
     */

    private javax.swing.JButton Runbt;
    private javax.swing.JButton Runc3dbt;
    private javax.swing.JButton TbSimbolobt;
    private javax.swing.JButton Erroresbt;
    private javax.swing.JButton Treecstbt;
    private javax.swing.JButton Astbt;
    private javax.swing.JButton Limpiarbt;

    private javax.swing.JMenu MenuArchivo;
    private javax.swing.JMenuBar MenuHead;
    private javax.swing.JMenuItem itemArbrir;
    private javax.swing.JMenuItem itemGuardar;
    private javax.swing.JMenuItem itemGuardarComo;

    private javax.swing.JScrollPane ScrollConsole;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar contenedorbt;
    private javax.swing.JTabbedPane pestañas;
    private static javax.swing.JTextPane Console;
    private javax.swing.JSplitPane jSplitPane2;
    private static javax.swing.JTextPane ConsoleC3D;
    private javax.swing.JScrollPane ScrollConsoleC3D;

//============================================== FUNCIONES UTILIZADAS =================================================

    // ============================================ CREACION DE PESTAÑAS ====================================================
    public void nuevaPestaña()
    {
        pestañas.add(new Tabs(), "Analyzer" + (pestañas.getTabCount() + 1));
        pestañas.setSelectedIndex(pestañas.getTabCount() - 1);
    }

    // ============================================ ABRIR ARCHIVO ====================================================
    public void Abrir()
    {
        JFileChooser a = new JFileChooser();
        a.setFileFilter(new FileNameExtensionFilter("Archivo","fca"));
        if (a.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File archivo = a.getSelectedFile();
            //String texto = asdfasd.LeerArchivo(archivo);
            System.out.println(archivo.getAbsolutePath());
            //nuevaPestaña(texto, archivo.getName(), archivo.getAbsolutePath());
        }
    }

    // ========================================= Run ARCHIVO ====================================================
    private void RunCode()
    {
        //String texto_consola ="";
        Tabs tab = ((Tabs) pestañas.getSelectedComponent());
        if (tab.isEmptyText()) {
            JOptionPane.showMessageDialog(this, "Sin código para Interpretar !");
        }
        else{
            //Consola.setText("hola");
            ConsoleC3D.setText("Fortran Analizer C3D:");
            Global.consola ="Salida Interprete:\n";
            Interpretar.Iniciar(tab.getText());
            System.out.println(Global.consola);
            Console.setText(Global.consola);
            System.out.println("EFE");
        }


    }

    // ========================================= GENERAR CODIGO TRES DIRECCIONS ====================================================
    private void RunC3D() {
        // verificar que no se tengan errores
        if(Global._errores.isEmpty() && Global._erroresLexicos.isEmpty() && Global._erroresSintacticos.isEmpty())
        {
            // verificar que el ctx no este nullo
            if(Global.c3d != null)
            {
                String consolaC3D = GenC3D();
                ConsoleC3D.setText("//CFortran Analizer C3D: \n" + consolaC3D);
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Error al generar C3D, Contexto nulo !");
            }


        }
        else
        {
            JOptionPane.showMessageDialog(this, "Error al generar C3D, hay errores en el codigo de entrada !");
        }
    }

    public String GenC3D()
    {
        // SEGUNDA PASADO CODIGO 3 DIRECCIONES
        VisitorC3D cod3d = new VisitorC3D(Global.stackC3d);
        cod3d.visit(Global.c3d);
        //imprimir cabecera
        String reporte = "#include <stdio.h>\n" +
                "double STACK[30101999];\n" +
                "double HEAP[30101999];\n" +
                "double P;\n" +
                "double H;\n";
        // imprimir temporales
        if(!cod3d.gen.Lista_temporales().equals("")){
            reporte += "double  "+ cod3d.gen.Lista_temporales();
        }
        for (String ln : cod3d.gen.codigo)
            reporte += ln;
        // escribiendo salida
        FileWriter fichero = null;
        PrintWriter escritor;
        try
        {
            fichero = new FileWriter("C:\\Users\\Pilo Tuy\\Desktop\\Compiladores 2\\reportes ast\\c3d.c");
            escritor = new PrintWriter(fichero);
            escritor.print(reporte);
            //escritor.print("digraph { a -> b }");
        }
        catch (Exception e)
        {
            System.err.println("Error al escribir el archivo ");
        }
        finally
        {
            try
            {
                if (null != fichero)
                    fichero.close();
            }
            catch (Exception e2)
            {
                System.err.println("Error al cerrar el archivo ");
            }
        }
        return  reporte;
    }



}
