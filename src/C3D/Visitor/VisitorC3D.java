package C3D.Visitor;

import C3D.Visitor.EntornoC3D.GlobalC3D;
import C3D.Visitor.EntornoC3D.SimboloC3D;
import C3D.Visitor.Generador.Generador;
import Entorno.*;
import Funciones.Funciones;
import Main.Global;
import Main.Puntero;
import Subrutinas.Subrutina;
import grammar.JGrammarBaseVisitor;
import grammar.JGrammarParser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.ArrayList;
import java.util.Stack;

public class VisitorC3D extends JGrammarBaseVisitor<Object> {
    //int posStack;
    ArrayList <Environment> entStackArray = new ArrayList<>();
    public Generador gen = new Generador();
    public Stack<String> Ifbreak = new Stack<>();
    public Stack<String> ControlExit = new Stack<>();
    public Stack<String> ControlCycle = new Stack<>();
    //Stack<Environment> entStackC3D = new Stack<Environment>();
    Stack<Environment> entStack = new Stack<Environment>();
    //Environment actualEnv ;
    int posicionActual;
    int incremento;


    public VisitorC3D(Stack<Environment> entStack )
    {
        //this.posStack =0;
        this.entStackArray.addAll(entStack);
        this.posicionActual =0;
        this.incremento =0;
        //System.out.println("tamaño del arreglo: " + entStackArray.size());
        /*
        * while (!entStack.isEmpty())
            this.entStack.push(entStack.pop());*/
        //actualEnv = this.entStack.pop();
        //this.entStackC3D.push(this.entStack.pop());


        //this.padre = padre;

    }

    public Object visitStart(JGrammarParser.StartContext ctx)
    {
        //gen.metodo_Print(gen);

        if(ctx.bloqueFunciones().inicioSubRutina() != null)
            visitBloqueFunciones(ctx.bloqueFunciones());
        if(ctx.bloqueFunciones2().inicioFuncion() != null)
            visitBloqueFunciones2(ctx.bloqueFunciones2());

        if(ctx.init.getText().equals(ctx.end.getText())){
            gen.Agregar_codigo("int main() {\nP=0;\t\t//INICIANDO PUNTERO STACK\nH=0;\t\t//INICIANDO PUNTERO HEAP\n");
            visitLinstrucciones(ctx.linstrucciones());
            gen.Agregar_codigo( "\n return 0;\n}");
        }

       return  null;
    }

    /*public Object visitBloqueFunciones(JGrammarParser.BloqueFuncionesContext ctx)
    {

        for (JGrammarParser.InicioSubRutinaContext ictx : ctx.inicioSubRutina()){
            Object s= visitInicioSubRutina(ictx);
            if(s instanceof  Exit || s instanceof  Cycle){
                return  s;
            }
        }

        return true;
    }

    public Object visitBloqueFunciones2(JGrammarParser.BloqueFunciones2Context ctx)
    {

        for (JGrammarParser.InicioFuncionContext ictx : ctx.inicioFuncion()){
            Object s= visitInicioFuncion(ictx);
            if(s instanceof  Exit || s instanceof  Cycle){
                return  s;
            }
        }

        return true;
    }*/
    public Object visitLinstrucciones(JGrammarParser.LinstruccionesContext ctx)
    {
        for (JGrammarParser.InstruccionesContext ictx : ctx.instrucciones())
        {

            Object s= visitInstrucciones(ictx);
            if(posicionActual ==0 && incremento !=0){
                /*for(int i = 1 ; i< incremento ;i++)
                {
                    entStackArray.remove( 1);

                }*/
                incremento =0;
            }

        }
        return true;
    }

    public Object visitInstrucciones(JGrammarParser.InstruccionesContext ctx)
    {

        if (ctx.declaracion() != null)
            visitDeclaracion(ctx.declaracion());
        else if (ctx.print() != null)
            visitPrint(ctx.print());
        else if (ctx.sentenciaIf() != null){
            Object ctrl = visitSentenciaIf(ctx.sentenciaIf());
            if(ctrl instanceof  Exit || ctrl instanceof  Cycle){
                return ctrl;
            }
        }
        else if (ctx.asignacionvar() != null)
            visitAsignacionvar(ctx.asignacionvar());
        else if (ctx.arreglosestaticos() != null)
            visitArreglosestaticos(ctx.arreglosestaticos());
        else if (ctx.asignarArrayUni() != null)
            visitAsignarArrayUni(ctx.asignarArrayUni());
        else if (ctx.asignarArrayBid () != null)
            visitAsignarArrayBid (ctx.asignarArrayBid ());
        else if (ctx.arreglosDinamicos() != null)
            visitArreglosDinamicos(ctx.arreglosDinamicos());
        else if (ctx.funAllocate () != null)
            visitFunAllocate (ctx.funAllocate ());
        else if (ctx.funDeAllocate () != null)
            visitFunDeAllocate  (ctx.funDeAllocate  ());
        else if (ctx.sentenciaIf () != null)
            visitSentenciaIf  (ctx.sentenciaIf());
        else if (ctx.cicloDo () != null)
            visitCicloDo  (ctx.cicloDo());
        else if (ctx.cicloDoWhile () != null)
            visitCicloDoWhile  (ctx.cicloDoWhile());
        else if (ctx.controlCiclos () != null)
            return  visitControlCiclos (ctx.controlCiclos());
        else if (ctx.callSubrutina () != null)
            return  visitCallSubrutina (ctx.callSubrutina());

        return true;
    }
    /**
     * *************************** EXPRESIONES **************************
     * */
    // operaciones aritmeticas
    public SimboloC3D visitOpAritmetica(JGrammarParser.OpAritmeticaContext ctx)
    {
        //Environment ent = GlobalC3D.entornos.get(posStack);
        Type[][] opAritmetica = {
                // null    // int       // rel     //complex // char    // logical
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // null
                {Type.NULL,Type.INTEGER,Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // int
                {Type.NULL,Type.REAL,   Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // real
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // char
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
        };
        try
        {
            SimboloC3D izq;
            SimboloC3D der;
            der = (SimboloC3D) visit(ctx.right);
            String operacion = ctx.op.getText();
            if(der != null ){
                switch (operacion)
                {
                    //POTENCIA
                    case "**" :
                        izq = (SimboloC3D) visit(ctx.left);
                        Type dom = opAritmetica[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            String e_entrada = gen.crear_etiquetas();
                            String e_salida = gen.crear_etiquetas();
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String oper = tmp1 +" = " + izq.getValor() +";\n";
                            oper += tmp2 +" = 1;\n " ;
                            oper += e_entrada +":\n";
                            oper +="if( "+ tmp2 +" == "+ der.getValor()+" ) goto "+e_salida +";\n";
                            oper += tmp1 +" = "+tmp1+" * "+ izq.getValor() +";\n";
                            oper += tmp2 +" = "+tmp2+" + 1 ;\n";
                            oper += "goto "+e_entrada + ";\n";
                            oper += e_salida + ":\n";
                            gen.Agregar_codigo(oper);
                            return new SimboloC3D(tmp1,true,Type.INTEGER,"","",Dato.VARIABLE);
                        }
                        else if(dom == Type.REAL)
                        {
                            String e_entrada = gen.crear_etiquetas();
                            String e_salida = gen.crear_etiquetas();
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String oper = tmp1 +" = " + izq.getValor() +";\n";
                            oper += tmp2 +" = 1;\n " ;
                            oper += e_entrada +":\n";
                            oper +="if( "+ tmp2 +" == "+ der.getValor()+" ) goto "+e_salida +";\n";
                            oper += tmp1 +" = "+tmp1+" * "+ izq.getValor() +";\n";
                            oper += tmp2 +" = "+tmp2+" + 1 ;\n";
                            oper += "goto "+e_entrada + ";\n";
                            oper += e_salida + ":\n";
                            gen.Agregar_codigo(oper);
                            return new SimboloC3D(tmp1,true,Type.REAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            System.out.println("Error en multiplicacion");
                            return  null;
                        }
                    case "*" :
                        izq = (SimboloC3D) visit(ctx.left);
                        dom = opAritmetica[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " * " +der.getValor() +";\t \t //Multiplicacion\"\n  ");
                            return new SimboloC3D(tmp,true,Type.INTEGER,"","",Dato.VARIABLE);
                        }
                        else if(dom == Type.REAL){
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " * " +der.getValor() +";\t \t //Multiplicacion\"\n  ");
                            return new SimboloC3D(tmp,true,Type.REAL,"","",Dato.VARIABLE);      }
                        else
                        {
                            System.out.println("Error en multiplicacion");
                           return  null;
                        }
                    case "/" :
                        izq = (SimboloC3D) visit(ctx.left);
                        dom = opAritmetica[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " / " +der.getValor() +";\t \t //Division\"\n  ");
                            return new SimboloC3D(tmp,true,Type.INTEGER,"","",Dato.VARIABLE);
                        }
                        else if(dom == Type.REAL)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " / " +der.getValor() +";\t \t //Division\"\n  ");
                            return new SimboloC3D(tmp,true,Type.REAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            System.out.println("Error en division");
                            return  null;
                        }
                    case "+" :
                        izq = (SimboloC3D) visit(ctx.left);
                        dom = opAritmetica[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " + " +der.getValor() +";\t \t //Suma\"\n  ");
                            return new SimboloC3D(tmp,true,Type.INTEGER,"","",Dato.VARIABLE);
                        }
                        else if(dom == Type.REAL){
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " + " +der.getValor() +";\t \t //Suma\"\n  ");
                            return new SimboloC3D(tmp,true,Type.REAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            System.out.println("Error en suma");
                            return  null;
                        }
                    case "-" :
                        if(ctx.left == null)
                        {
                            if(der.getTipo() == Type.INTEGER)
                            {
                                String tmp = gen.crear_temporal();
                                gen.Agregar_codigo(tmp +" =   " +der.getValor() +" * -1 ;\t \t // Unario\"\n  ");
                                return new SimboloC3D(tmp,true,Type.INTEGER,"","",Dato.VARIABLE);
                            }
                            else if(der.getTipo() == Type.REAL){
                                String tmp = gen.crear_temporal();
                                gen.Agregar_codigo(tmp +" =   " +der.getValor() +" * -1 ;\t \t //Unario\"\n  ");
                                return new SimboloC3D(tmp,true,Type.REAL,"","",Dato.VARIABLE);
                            }
                            else
                            {
                                System.out.println("Error en  operacio unario");
                                return  null;
                            }
                        }
                        else
                        {
                            izq = (SimboloC3D) visit(ctx.left);
                            dom = opAritmetica[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                            if(dom == Type.INTEGER)
                            {
                                String tmp = gen.crear_temporal();
                                gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " - " +der.getValor() +";\t \t //Resta\"\n  ");
                                return new SimboloC3D(tmp,true,Type.INTEGER,"","",Dato.VARIABLE);
                            }
                            else if(dom == Type.REAL){
                                String tmp = gen.crear_temporal();
                                gen.Agregar_codigo(tmp +" = "+ izq.getValor()+ " - " +der.getValor() +";\t \t //Resta\"\n  ");
                                return new SimboloC3D(tmp,true,Type.REAL,"","",Dato.VARIABLE);
                            }
                            else
                            {
                                System.out.println("Error en resta");
                                return  null;
                            }
                        }

                    default:
                        return  null;
                }
            }else {
                return  null;
            }
        }
        catch (Exception e)
        {
            System.out.println("error en operacion aritmetica \n "+ e);
            return  null;
        }
    }
    public  SimboloC3D visitOpRelacional(JGrammarParser.OpRelacionalContext ctx)
    {

        Type[][] opRelaciona = {
                // null    // int       // rel     //complex // char    // logical
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // null
                {Type.NULL,Type.INTEGER,Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // int
                {Type.NULL,Type.REAL,   Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // real
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.CHAR,Type.NULL}, // char
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.LOGICAL}, // Logical
        };
        try
        {
            SimboloC3D izq;
            SimboloC3D der;
            der = (SimboloC3D) visit(ctx.right);
            izq = (SimboloC3D) visit(ctx.left);
            String operacion = ctx.op.getText();
            if(der != null && izq != null){
                Type dom = opRelaciona[izq.getTipo().ordinal()][der.getTipo().ordinal()];
                String etiqueta_verdadera = gen.crear_etiquetas();
                String etiqueta_falsa = gen.crear_etiquetas();
                switch (operacion)
                {
                    case ".eq." :
                    case "==" :
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL == */\n ";
                            codigo +=   "if (" +  izq.getValor() +" == " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" : \n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL == */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else if(dom == Type.CHAR)
                        {
                            if(izq.getNativo() == Dato.PRIMITIVO && der.getNativo() == Dato.PRIMITIVO)
                            {
                                String r = ( Double.parseDouble(izq.getValor()) ==   Double.parseDouble(der.getValor()))? "1":"0";
                                /*String codigo = "/*  INICION OPERACION RELACIONAL == */ //\n";
                                //codigo += "if (" +  izq.getValor() +" == " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n" +
                                  //      " goto "+ etiqueta_falsa +";\n";
                                //codigo += "/*  FIN OPERACION RELACIONAL == */ \n";
                                //gen.Agregar_codigo( codigo);*/
                                return new SimboloC3D(r,false,Type.LOGICAL,"","",Dato.PRIMITIVO);
                            }
                            else
                            {
                                //String nf = gen.crear_etiquetas();
                                //String nv = gen.crear_etiquetas();
                                String tmp_result = gen.crear_temporal();
                                String salida = gen.crear_etiquetas();
                                String codigo = "/*  INICION OPERACION RELACIONAL == */\n ";
                                codigo +=   "if (" +  izq.getValor() +" == " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                        "goto "+ etiqueta_falsa +";\n";
                                codigo +=   etiqueta_verdadera +": \n " +
                                        tmp_result +" = 1 ;  \n" +
                                        "goto "+ salida +";  \n" +
                                        etiqueta_falsa +":   \n"+
                                        tmp_result +" =  0 ; \n"+
                                        salida +" :\n" ;
                                codigo += "/*  FIN OPERACION RELACIONAL == */ \n";
                                gen.Agregar_codigo( codigo);
                                return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                            }

                        }
                        else if(dom == Type.LOGICAL)
                        {
                            // verificar si es un temp los dos valores
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL == */\n ";
                            codigo +=   "if (" +  izq.getValor() +" == " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL == */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);

                        }
                        else
                        {
                           return  null;
                        }
                    case  "/=":
                    case  ".ne.":
                        if(dom == Type.REAL || dom == Type.INTEGER || dom == Type.CHAR)
                        {
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL != */\n ";
                            codigo +=   "if (" +  izq.getValor() +" != " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL != */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);

                        }
                        else if(dom == Type.LOGICAL)
                        {
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL != */\n ";
                            codigo +=   "if (" +  izq.getValor() +" != " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL != */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            return  null;
                        }
                    case  "<":
                    case  ".lt.":
                        if(dom == Type.REAL || dom == Type.INTEGER || dom == Type.CHAR)
                        {
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL < */\n ";
                            codigo +=   "if (" +  izq.getValor() +" < " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL < */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            return  null;
                        }
                    case  ">":
                    case  ".gt.":
                        if(dom == Type.REAL || dom == Type.INTEGER || dom == Type.CHAR)
                        {
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL > */\n ";
                            codigo +=   "if (" +  izq.getValor() +" > " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL > */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            return  null;
                        }
                    case  "<=":
                    case  ".le.":
                        if(dom == Type.REAL || dom == Type.INTEGER || dom == Type.CHAR)
                        {
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL  <=  */\n ";
                            codigo +=   "if (" +  izq.getValor() +"  <=  " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL  <= */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            return  null;
                        }
                    case  ">=":
                    case  ".ge.":
                        if(dom == Type.REAL || dom == Type.INTEGER || dom == Type.CHAR)
                        {
                            //String nf = gen.crear_etiquetas();
                            //String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "/*  INICION OPERACION RELACIONAL  >=  */\n ";
                            codigo +=   "if (" +  izq.getValor() +"  >=  " + der.getValor() + ") goto " + etiqueta_verdadera+ ";\n " +
                                    "goto "+ etiqueta_falsa +";\n";
                            codigo +=   etiqueta_verdadera +": \n " +
                                    tmp_result +" = 1 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    etiqueta_falsa +":   \n"+
                                    tmp_result +" =  0 ; \n"+
                                    salida +" :\n" ;
                            codigo += "/*  FIN OPERACION RELACIONAL  >= */ \n";
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            return  null;
                        }
                    default:
                        System.out.println("Error en operacion relacional c3d");
                        return  null;
                }

            }else {
                System.out.println("Error en operacion relacional c3d");
                return  null;
            }

        }
        catch (Exception e)
        {
            System.out.println("Error en operacion relacional c3d \n " + e);
            return  null;
        }
    }
    public SimboloC3D visitOpLogica(JGrammarParser.OpLogicaContext ctx)
    {
        //Environment ent = GlobalC3D.entornos.get(posStack);
        try
        {
            SimboloC3D izq;
            SimboloC3D der;
            der = (SimboloC3D) visit(ctx.right);
            //izq = (SimboloC3D) visit(ctx.left);
            String operacion = ctx.op.getText();
            switch (operacion)
            {
                case ".not." :
                    if(der.getTipo() == Type.LOGICAL)
                    {
                        if(der.isEsTemp() && der.getEtiquetaVerdera().equals("")){
                            String nf = gen.crear_etiquetas();
                            String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "if (" +  der.getValor() +" == 1) goto " + nv+ ";\n " +
                                    "goto "+ nf +";\n";
                            codigo +=  nv +": \n " +
                                    tmp_result +" = 0 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    nf +":   \n"+
                                    tmp_result +" =  1 ; \n"+
                                    salida +" :\n" ;
                            gen.Agregar_codigo( codigo);
                            String v = gen.crear_etiquetas();
                            String f = gen.crear_etiquetas();
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,v,f,Dato.VARIABLE);
                        }else {
                            String nf = gen.crear_etiquetas();
                            String nv = gen.crear_etiquetas();
                            String tmp_result = gen.crear_temporal();
                            String salida = gen.crear_etiquetas();
                            String codigo = "if (" +  der.getValor() +" == 1) goto " + nv+ ";\n " +
                                    "goto "+ nf +";\n";
                            codigo +=  nv +": \n " +
                                    tmp_result +" = 0 ;  \n" +
                                    "goto "+ salida +";  \n" +
                                    nf +":   \n"+
                                    tmp_result +" =  1 ; \n"+
                                    salida +" :\n" ;
                            gen.Agregar_codigo( codigo);
                            return new SimboloC3D(tmp_result,true,Type.LOGICAL,der.getEtiquetaVerdera(),der.getEtiquetaFalse(),Dato.VARIABLE);
                        }
                    }
                    else
                    {
                        //Errores.AgregarError("Operacion Logica no valida: Error al operar expresiones",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        System.out.println("error en operacion relacional ");
                        return  null;
                    }
                case  ".and.":
                    izq = (SimboloC3D) visit(ctx.left);
                    if(izq.getTipo() == Type.LOGICAL && der.getTipo() == Type.LOGICAL )
                    {
                        String nf = gen.crear_etiquetas();
                        String nv = gen.crear_etiquetas();
                        String nf2 = gen.crear_etiquetas();
                        String nv2 = gen.crear_etiquetas();
                        String tmp_result = gen.crear_temporal();
                        String salida = gen.crear_etiquetas();
                        String codigo = "if (" + izq.getValor() +" == 1 ) goto " + nv+ ";\n " +
                                "goto "+ nf +";\n";
                        codigo += nv +": \n " +
                                "if (" + der.getValor() +" == 1 ) goto " + nv2+ ";\n " +
                                "goto "+ nf2 +";\n";
                        codigo += nv2 +": \n " +
                                tmp_result +" =  1 ; \n"+
                                "goto "+ salida +";  \n";
                        codigo +=  nf +": "+ nf2 +": \n " +
                                tmp_result +" =0 ;  \n" +
                                salida +" :\n" ;
                        gen.Agregar_codigo( codigo);
                        String v = gen.crear_etiquetas();
                        String f = gen.crear_etiquetas();
                        return new SimboloC3D(tmp_result,true,Type.LOGICAL,v,f,Dato.VARIABLE);
                    }
                    else
                    {
                        System.out.println("error en operacion and");
                        return  null;
                    }
                case  ".or.":
                    izq = (SimboloC3D) visit(ctx.left);
                    if(izq.getTipo() == Type.LOGICAL && der.getTipo() == Type.LOGICAL )
                    {
                        String nf = gen.crear_etiquetas();
                        String nv = gen.crear_etiquetas();
                        String nf2 = gen.crear_etiquetas();
                        String nv2 = gen.crear_etiquetas();
                        String tmp_result = gen.crear_temporal();
                        String salida = gen.crear_etiquetas();
                        String codigo = "if (" + izq.getValor() +" == 1 ) goto " + nv+ ";\n " +
                                "goto "+ nf +";\n";
                        // primera expresion falsa
                        codigo += nf +": \n " +
                                "if (" + der.getValor() +" == 1 ) goto " + nv2+ ";\n " +
                                "goto "+ nf2 +";\n";
                        // segunda  expresion falsa
                        codigo += nf2 +": \n " +
                                tmp_result +" =  0 ; \n"+
                                "goto "+ salida +";  \n";
                        // primera  y segunda  expresion verdadera
                        codigo +=  nv +": "+ nv2 +": \n " +
                                tmp_result +" =1 ;  \n" +
                                salida +" :\n" ;
                        gen.Agregar_codigo( codigo);
                        String v = gen.crear_etiquetas();
                        String f = gen.crear_etiquetas();
                        return new SimboloC3D(tmp_result,true,Type.LOGICAL,v,f,Dato.VARIABLE);
                    }
                    else
                    {
                        System.out.println("error en operacion or");
                        return  null;
                    }

                default:
                    return  null;
            }
        }
        catch (Exception e)
        {
            System.out.println("error en operacion logica " + e);
            return  null;
        }
    }
    public SimboloC3D visitOpSize(JGrammarParser.OpSizeContext ctx)
    {

        Environment ent = entStackArray.get(posicionActual);
        //Environment ent =actualEnv;
        //Environment ent =entStackC3D.peek();
        try{
            ArrayS id = ent.BuscarArrays(ctx.i.getText());
            if (id == null) {
                System.out.println("Error operacion size(), array no encontrado: " +ctx.i.getText() );
                return null;
            } else{
                int size = (id.getDim()==2)?(id.getSize1()* id.getSize2()): id.getSize1();
                String opsize = gen.crear_temporal();
                opsize = String.valueOf(size);
                return  new SimboloC3D(opsize,true,Type.INTEGER,"","",Dato.VARIABLE);
            }
        } catch (Exception e){
            System.out.println("Error operacion size(), array no encontrado: " +ctx.i.getText() +" "+e );
            return  null;
        }

    }

    /**
     * *************************** INSTRUCCIONES **************************
     * */
    public Object visitSentenciaIf (JGrammarParser.SentenciaIfContext ctx)
    {
        try
        {
            Environment ent = entStackArray.get(posicionActual);
            int posAterior = posicionActual;
            //Environment entActual = entStack.get(entStack.size() -1);
            //Environment ent =entStackC3D.peek();
            //Environment ent = actualEnv;
            // obtener condicion inicial
            Object conInicial = visit(ctx.ep);
            if(conInicial instanceof  SimboloC3D)
            {
                // inicio de sentencia if
                // verificar que sea de tipo boolean
                if(((SimboloC3D) conInicial).getTipo() == Type.LOGICAL )
                {

                    String ifcontinuar = gen.crear_etiquetas();
                    Ifbreak.push(ifcontinuar);
                    SimboloC3D cond = ((SimboloC3D) conInicial);
                    String ifverdadero = gen.crear_etiquetas();
                    String iffalso  = gen.crear_etiquetas();
                    //construir if
                    String senIf = "/*  --------------- SENTENCIA IF -------------------*/ \n";
                    senIf +=    "if("+cond.getValor() +" == 1 ) goto "+ ifverdadero+ ";\n"+
                            "goto "+ iffalso+ ";\n"+
                            ifverdadero+ ":\n";
                    gen.Agregar_codigo(senIf);

                    // si la sentencia if es valida

                    //actualEnv = actualEnv.siguiente;
                    //entStackC3D.push(entStack.pop());
                    posicionActual  ++;
                    incremento ++;
                    Object control = visitLinstrucciones(ctx.bp);
                    //actualEnv = actualEnv.global;
                    //entStackC3D.pop();
                    // verificar si la posicion anterior es 0
                    posicionActual = posAterior;
                    gen.Agregar_codigo( "goto " +ifcontinuar +"; \n");
                    // ---------- aqui va la sentencia else if ------------
                    if(ctx.bElse != null)
                    {
                        gen.Agregar_codigo( iffalso +" :\n");
                        for(JGrammarParser.ListaElseContext el: ctx.bElse.listaElse() ){
                            Object result = visitListaElse(el);
                        }
                        //  if end
                        String ifend = "/*  --------------- END IF -------------------*/ \n";
                        //ifend +=   iffalso +" :\n";
                        gen.Agregar_codigo(ifend);
                        if(ctx.bfinal != null){
                            posicionActual  ++;
                            incremento ++;
                            //entStackC3D.push(entStack.pop());
                            //actualEnv = actualEnv.siguiente;
                            control = visit(ctx.bfinal);
                            //actualEnv = actualEnv.global;

                            posicionActual = posAterior;
                            //entStackC3D.pop();
                        }
                        gen.Agregar_codigo( ifcontinuar +" : \n");
                        Ifbreak.pop();
                    }
                    else
                    {
                        //  if end
                        String ifend = "/*  --------------- END IF -------------------*/ \n";
                        ifend +=   iffalso +" :\n";
                        gen.Agregar_codigo(ifend);
                        if(ctx.bfinal != null){
                            posicionActual  ++;
                            incremento ++;

                            //entStackC3D.push(entStack.pop());
                            //actualEnv = actualEnv.siguiente;
                            control = visit(ctx.bfinal);
                            //actualEnv = actualEnv.global;
                            //entStackC3D.pop();


                            posicionActual = posAterior;

                        }
                        gen.Agregar_codigo( ifcontinuar +" : \n");
                        Ifbreak.pop();
                    }
                }else {
                    System.out.println("error en condicionn sentencia if");
                }
            }else{
                System.out.println("error en condicionn sentencia if");
            }
        }catch ( Exception e) {
            System.out.println("error en sentencia if  " + e);

        }
        return null;

    }
    public Object visitListaElse(JGrammarParser.ListaElseContext ctx)
    {

        try
        {
            Environment ent = entStackArray.get(posicionActual);
            int posAterior = posicionActual;
            //Environment entActual = entStackC3D.peek();
            //Environment entActual = actualEnv;
            Object conInicial = visit(ctx.ep);
            if(conInicial instanceof  SimboloC3D)
            {
                if(((SimboloC3D) conInicial).getTipo() == Type.LOGICAL )
                {
                    // verificar que seaverdadero
                    SimboloC3D cond = ((SimboloC3D) conInicial);
                    String ifverdadero = gen.crear_etiquetas();
                    String iffalso  = gen.crear_etiquetas();
                    //construir if
                    String senIf = "/*  --------------- SENTENCIA ELSE IF -------------------*/ \n";
                    senIf +="if("+cond.getValor() +" == 1 ) goto "+ ifverdadero+ ";\n"+
                            "goto "+ iffalso+ ";\n"+
                            ifverdadero+ ":\n";
                    gen.Agregar_codigo(senIf);
                    // si la sentencia if es valida

                    //entStackC3D.push(entStack.pop());
                    //actualEnv = actualEnv.siguiente;
                    posicionActual ++;
                    incremento ++;
                    Object control = visitLinstrucciones(ctx.bp);
                    //actualEnv = actualEnv.global;
                    //entStackC3D.pop();

                    posicionActual = posAterior;
                    gen.Agregar_codigo( "goto " +Ifbreak.peek() +"; \n");
                    String ifend = "/*  --------------- END IF -------------------*/ \n";
                    ifend +=   iffalso +" :\n";
                    gen.Agregar_codigo(ifend);
                    return  null;
                }
                else
                {
                    System.out.println("error en condicion if ");
                    return  null;
                }
            }
            else
            {
                System.out.println("error en condicion if ");
                return  null;
            }

        } catch (Exception e){
            System.out.println("Error al ejecutar if else " + e);
            return  null;
        }
    }
    public SimboloC3D visitDeclaracion(JGrammarParser.DeclaracionContext ctx)
    {
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        //Environment ent = actualEnv;
        //Environment ent = entStackC3D.peek();
        for (int i=0;i<ctx.l.children.size();i++ )
        {
            if(ctx.l.children.get(i) instanceof JGrammarParser.VariablesContext)
            {
                JGrammarParser.VariablesContext v = (JGrammarParser.VariablesContext) ctx.l.children.get(i);
                // verificar si existe la variable
                if(ent.TablaSimbolo.containsKey(v.i.getText().toUpperCase()))
                {
                    gen.Agregar_codigo("/*Declarar variable : "+v.i.getText()+" */"+"\n");
                    // obtener simbolo
                    Symbol SDeclarado = ent.Buscar(v.i.getText().toUpperCase());
                    // verificar si es nulo
                    if(v.e == null)
                    {
                        // verificar tipo por medio de la variable guardada
                        if(SDeclarado.getType() == Type.INTEGER)
                        {
                            String tmp = gen.crear_temporal();

                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n " );
                            gen.Agregar_codigo("STACK[(int)P ] = 0;");

                        }
                        else if(SDeclarado.getType() == Type.REAL)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n " );
                            gen.Agregar_codigo("STACK[(int)P] = 0.00000000;\n");
                        }
                        else if(SDeclarado.getType() == Type.COMPLEX)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n "  );
                            gen.Agregar_codigo("STACK[(int) P ] = 0.00000000;"+"\n");
                        }
                        else if(SDeclarado.getType() == Type.CHAR)
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n " );
                            gen.Agregar_codigo("STACK[(int) P ] = 32;"+"\n");
                        }
                        else if(SDeclarado.getType() == Type.LOGICAL)
                        {

                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n " );
                            gen.Agregar_codigo("STACK[(int) P ] = 0;"+"\n");
                        }


                    }else
                    {
                        // aplica solo para booleanos:
                        if(SDeclarado.getType() == Type.LOGICAL && ctx.t.getText().equals("logical"))
                        {
                            // asignando un boleando no temporal
                            SimboloC3D smvalor = (SimboloC3D) visit(v.e);
                            if(!smvalor.isEsTemp())
                            {

                                // verificar si tiene el valor de  0 o 1
                                if(smvalor.getNativo() == Dato.PRIMITIVO){
                                    String salida= gen.crear_etiquetas();
                                    String tmp = gen.crear_temporal();
                                    String dcl  = smvalor.getValor().equals("1")?"goto "+ smvalor.getEtiquetaVerdera() +";\n":"goto "+ smvalor.getEtiquetaFalse() +";\n";
                                    dcl +=  smvalor.getEtiquetaVerdera() +":\n"+
                                            tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                            " P = "+tmp+"; \n "+
                                            "STACK[(int) P ] = 1;\n"+
                                            "goto "+ salida +";\n"+
                                            smvalor.getEtiquetaFalse() +":\n"+
                                            tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                            " P = "+tmp+"; \n "+
                                    "STACK[(int) P ] = 0;\n"+
                                            salida +":"+"\n";
                                    gen.Agregar_codigo(dcl);

                                }else {

                                    String tmp = gen.crear_temporal();
                                    String e_salida = gen.crear_etiquetas();
                                    String verdadero = gen.crear_etiquetas();
                                    String falso = gen.crear_etiquetas();
                                    // la etiqueta de verdadero

                                    String dcl ="if ( "+smvalor.getValor()  +" == 1) goto " +verdadero +";\n" +
                                            "goto "+ falso +"; \n";
                                    dcl += verdadero +" :\n";
                                    dcl += tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                            " P = "+tmp+"; \n ";
                                    dcl += "STACK[(int)P] = 1;\n";
                                    dcl += "goto "+ e_salida +";\n";
                                    // etiqueta falsa:
                                    dcl += falso+" :\n";
                                    dcl += tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                            " P = "+tmp+"; \n ";
                                    dcl += "STACK[(int)P] = 0;\n";
                                    dcl += e_salida +":\n";
                                    gen.Agregar_codigo(dcl);

                                }

                            }
                            else
                            {
                                String tmp = gen.crear_temporal();
                                gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                        " P = "+tmp+"; \n "  );
                                gen.Agregar_codigo("STACK[(int) P ] =  "+smvalor.getValor()+";"+"\n");
                            }

                        }
                        else
                        {
                            //obtener valor
                            SimboloC3D smvalor = (SimboloC3D) visit(v.e);;
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo(tmp+" ="+ent.ObtenerTamanioAsignar()+"+"+SDeclarado.getPos()+";\t \t// POSICION: "+v.i.getText() +"\n"+
                                    " P = "+tmp+"; \n "  );
                            gen.Agregar_codigo("STACK[(int) P] = "+smvalor.getValor()+";"+"\n");
                        }


                    }
                    gen.Agregar_codigo("/*Finalizar proceso de declaracion Variable  "+v.i.getText()+"*/"+"\n");
                }

            }
        }

        return  null;
    }
    public Object visitAsignacionvar(JGrammarParser.AsignacionvarContext ctx)
    {
        //Environment ent = entStackC3D.peek();
        //Environment ent = actualEnv;
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        try
        {

            Object val = visit(ctx.l);
            if(val instanceof  SimboloC3D)
            {

                // obtener simbolo
                Symbol SDeclarado = ent.Buscar(ctx.i.getText().toUpperCase());
                if(SDeclarado != null)
                {
                    gen.Agregar_codigo("/* -------------  Asiganr variable ---------------  "+ctx.i.getText() +"*/\n");
                    if((((SimboloC3D) val).getTipo() ==Type.LOGICAL &&  SDeclarado.getType() == Type.LOGICAL))
                    {

                        // verificar si es un temporal:
                        SimboloC3D smvalor = ((SimboloC3D) val);
                        if(!smvalor.isEsTemp())
                        {
                            if(smvalor.getNativo() == Dato.PRIMITIVO)
                            {
                                String salida= gen.crear_etiquetas();
                                String tmp = gen.crear_temporal();
                                String dcl  = smvalor.getValor().equals("1")?"goto "+ smvalor.getEtiquetaVerdera() +";\n":"goto "+ smvalor.getEtiquetaFalse() +";\n";
                                dcl +=  smvalor.getEtiquetaVerdera() +":\n"+
                                        tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                        " P = "+tmp+"; \n "+
                                        "STACK[(int) P ] = 1;\n"+
                                        "goto "+ salida +";\n"+
                                        smvalor.getEtiquetaFalse() +":\n"+
                                        tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                        " P = "+tmp+"; \n "+
                                        "STACK[(int)P] = 0;\n"+
                                        salida +":\n";
                                gen.Agregar_codigo(dcl);
                            }
                            else
                            {
                                String tmp = gen.crear_temporal();
                                String e_salida = gen.crear_etiquetas();
                                String verdadero = gen.crear_etiquetas();
                                String falso = gen.crear_etiquetas();
                                // la etiqueta de verdadero

                                String dcl ="if ( "+smvalor.getValor()  +" == 1) goto " +verdadero +";\n" +
                                        "goto "+ falso +"; \n";
                                dcl += verdadero +" :\n";
                                dcl += tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                        " P = "+tmp+"; \n ";
                                dcl += "STACK[(int) P] = 1;\n";
                                dcl += "goto "+ e_salida +";\n";
                                // etiqueta falsa:
                                dcl += falso+" :\n";
                                dcl +=  tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                        " P = "+tmp+"; \n ";
                                dcl += "STACK[(int)P] = 0;\n";
                                dcl += e_salida +":\n";
                                gen.Agregar_codigo(dcl);

                            }


                        }
                        else
                        {
                            String tmp = gen.crear_temporal();
                            gen.Agregar_codigo( tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                    " P = "+tmp+"; \n ");
                            gen.Agregar_codigo("STACK[(int)P] =  "+smvalor.getValor()+";"+"\n");

                        }


                    }
                    else if(((SimboloC3D) val).getTipo() == SDeclarado.getType())
                    {

                        String tmp = gen.crear_temporal();
                        gen.Agregar_codigo( tmp+" ="+ent.ObtenerTamanio(ctx.i.getText())+"+"+SDeclarado.getPos()+"; \t \t// POSICION: "+ctx.i.getText() +"\n "+
                                " P = "+tmp+"; \n " );
                        gen.Agregar_codigo("STACK[(int)P] = "+((SimboloC3D) val).getValor()+";"+"\n");
                    }
                }
                else
                {
                    System.out.println( "Error al asignar variable: no encontrado");
                }

            }
            else if(val instanceof  ArrayS)
            {
                /*
                * obtener el simbolo
                * obtener la dimension
                * obtener el indice 1
                * obtener el indice 2
                * obtener el tipo de dato
                * */

                // obtener simbolo
                Object arrayDeclarado = ent.BuscarArrays(ctx.i.getText().toUpperCase());
                ArrayS arrValor = (ArrayS) val;
                // verificar que sea de tipo array
                if(arrayDeclarado instanceof  ArrayS)
                {
                    ArrayS arrDeclarado = (ArrayS) arrayDeclarado;
                    // verificar que no sen dinamicos
                    if(!arrDeclarado.isAllocatable() &&  !arrValor.isAllocatable())
                    {
                        // dimension 1
                        if(arrDeclarado.getDim() ==1 && arrValor.getDim() ==1)
                        {
                            // verificar que se del mismo tipo y tamaño
                            if(arrDeclarado.getTypeData() == arrValor.getTypeData() && arrDeclarado.getSize1() == arrValor.getSize1())
                            {
                                // ciclo while para copiar lo datos del arreglo
                                String inicio = gen.crear_etiquetas();
                                String continuar = gen.crear_etiquetas();
                                String finalizar = gen.crear_etiquetas();
                                String indice = gen.crear_temporal();
                                // obteniendo temporales del  arreglo a copiar
                                String tmp1 = gen.crear_temporal();
                                String tmp2 = gen.crear_temporal();
                                String valor = gen.crear_temporal();
                                String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(arrValor.getId())+"+ "+arrValor.getPos()+" ; \n"+
                                                " P = "+tmp1+"; \n "+
                                                tmp2 +" = STACK[(int)P] ;\n"+
                                                valor +" = " +"HEAP[(int)" + tmp2 + "]; \n";
                                gen.Agregar_codigo(ValorArreglo);
                                // obteniendo temporales del arreglo a asingar
                                String t1 = gen.crear_temporal();
                                String t2 = gen.crear_temporal();
                                String t3 = gen.crear_temporal();
                                String agrergar = t1 + " = "+ent.ObtenerTamanio(arrDeclarado.getId())+"+"+arrDeclarado.getPos()+"; \n"+
                                        "P = "+t1 +";\n"+
                                        t2 +"  = STACK[(int)P];\n"+
                                        indice +" = 0 ;\n";
                                gen.Agregar_codigo(agrergar);
                                String conDoW = "//----------- ASIGNANDO ARREGLO -> ARREGLO ---------- \n";
                                conDoW += inicio +":\n" +
                                        "if ( "+indice +" < "+arrValor.getSize1() +" ) goto "+continuar +";\n" +
                                        "goto  "+ finalizar +";\n"+
                                        continuar + ":\n";
                                gen.Agregar_codigo(conDoW);
                                // seccion de codigo a ejecutar
                                // obtener el valor del arreglo
                                conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                        t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                        "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                        indice +" = "+ indice +" + 1;  \n"+
                                        tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                                gen.Agregar_codigo(conDoW);
                                conDoW ="goto "+inicio +";\n"+
                                        finalizar +":\n";
                                gen.Agregar_codigo(conDoW);
                                gen.Agregar_codigo("//----------- FIN ASIGNACION ARREGLO -> ARREGLO ---------- \n");


                            }
                            else
                            {
                                System.out.println("Arreglos  no son del mismo tipo");
                            }
                        }// dimension 2
                        else if(arrDeclarado.getDim() ==2 && arrValor.getDim() ==2)
                        {
                            // verificar que se del mismo tipo y tamaño
                            if(arrDeclarado.getTypeData() == arrValor.getTypeData() && arrDeclarado.getSize1() == arrValor.getSize1())
                            {
                                // ciclo while para copiar lo datos del arreglo
                                String inicio = gen.crear_etiquetas();
                                String continuar = gen.crear_etiquetas();
                                String finalizar = gen.crear_etiquetas();
                                String indice = gen.crear_temporal();
                                String size = gen.crear_temporal();
                                // obteniendo temporales del  arreglo a copiar
                                String tmp1 = gen.crear_temporal();
                                String tmp2 = gen.crear_temporal();
                                String valor = gen.crear_temporal();
                                String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(arrValor.getId())+"+ "+arrValor.getPos()+" ; \n"+
                                        " P = "+tmp1+"; \n "+
                                        tmp2 +" = STACK[(int)P] ;\n"+
                                        valor +" = " +"HEAP[(int)" + tmp2 + "]; \n";
                                gen.Agregar_codigo(ValorArreglo);
                                // obteniendo temporales del arreglo a asingar
                                String t1 = gen.crear_temporal();
                                String t2 = gen.crear_temporal();
                                String t3 = gen.crear_temporal();
                                String agrergar = t1 + " = "+ent.ObtenerTamanio(arrDeclarado.getId())+"+"+arrDeclarado.getPos()+"; \n"+
                                        "P = "+t1 +";\n"+
                                        t2 +"  = STACK[(int)P];\n"+
                                        indice +" = 0 ;\n"+
                                        size +"= "+arrValor.getSize1()+" * "+arrValor.getSize2() +"; \n " ;
                                gen.Agregar_codigo(agrergar);
                                String conDoW = "//----------- ASIGNANDO ARREGLO -> ARREGLO ---------- \n";
                                conDoW += inicio +":\n" +
                                        "if ( "+indice +" < "+ size +" ) goto "+continuar +";\n" +
                                        "goto  "+ finalizar +";\n"+
                                        continuar + ":\n";
                                gen.Agregar_codigo(conDoW);
                                // seccion de codigo a ejecutar
                                // obtener el valor del arreglo
                                conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                        t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                        "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                        indice +" = "+ indice +" + 1;  \n"+
                                        tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                                gen.Agregar_codigo(conDoW);
                                conDoW ="goto "+inicio +";\n"+
                                        finalizar +":\n";
                                gen.Agregar_codigo(conDoW);
                                gen.Agregar_codigo("//----------- FIN ASIGNACION ARREGLO -> ARREGLO ---------- \n");
                            }
                            else
                            {
                                System.out.println("Arreglos  no son del mismo tipo");
                            }

                        }

                    }
                    else
                    {
                        System.out.println("error al asignar arreglos, no es posible ne c3d");
                    }
                }
                else
                {
                    System.out.println(  " errro al asingar, el simbolo no es de tipo arreglo");
                }
            }
            return null;
        }
        catch (Exception e)
        {
            return  null;
        }

    }
    public Object visitPrint(JGrammarParser.PrintContext ctx)
    {
        //Environment ent = entStackC3D.peek();
        //Environment ent = actualEnv;
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        String consola ="";
        boolean conError = false;
        try{
            for(ParseTree i : ctx.l.children)
            {
                Object aux = visit(i);
                //  imprimir los objetos de tipo simbolo
                if(aux instanceof  SimboloC3D)
                {
                    consola  += "/* IMPRIMIR VARIABLE TEMPORAL */\n";
                    // verificar tipos
                    if(((SimboloC3D) aux).getTipo() ==Type.INTEGER )
                    {
                        consola += "\tprintf(\"%d\",(int)" + ((SimboloC3D) aux).getValor() + ");\n";
                    }else if(((SimboloC3D) aux).getTipo() ==Type.REAL )
                    {
                        consola += "\tprintf(\"%f\"," + ((SimboloC3D) aux).getValor() + ");\n";
                    }
                    else if(((SimboloC3D) aux).getTipo() ==Type.CHAR )
                    {
                        consola += "\tprintf(\"%c\",(int)" + ((SimboloC3D) aux).getValor() + ");\n";
                    }
                    else if(((SimboloC3D) aux).getTipo() ==Type.LOGICAL )
                    {

                        SimboloC3D tmp = ((SimboloC3D) aux);
                        consola += "\tprintf(\"%d\",(int)" + tmp.getValor() + ");\n";
                        // si es un temporal:
                        //if(tmp.isEsTemp()  && tmp.getEtiquetaVerdera().equals(""))
                        /*if(tmp.isEsTemp()  && tmp.getEtiquetaVerdera().equals(""))
                        {
                        String verdadero = gen.crear_etiquetas();
                        String falso  = gen.crear_etiquetas();
                        String continuar = gen.crear_etiquetas();
                        String str = "if ("+tmp.getValor()+" == 1 ) goto "+ verdadero +" ;\n" +
                                "goto " +falso +" ;\n";
                        str +=  verdadero +":\n";
                        str +=  "\tprintf(\"%c\", (int)116);\n" +
                                "\tprintf(\"%c\", (int)114);\n" +
                                "\tprintf(\"%c\", (int)117);\n" +
                                "\tprintf(\"%c\", (int)101);\n" +
                                "\tgoto "+ continuar +";\n";
                        str += falso +":\n" +
                                "\tprintf(\"%c\", (int)102);\n" +
                                "\tprintf(\"%c\", (int)97);\n" +
                                "\tprintf(\"%c\", (int)108);\n" +
                                "\tprintf(\"%c\", (int)115);\n" +
                                "\tprintf(\"%c\", (int)101);\n" ;
                        str +=continuar +":\n";
                        consola +=str;

                        }
                        else if(tmp.isEsTemp() )
                        {

                            String continuar = gen.crear_etiquetas();
                            String str = "if ("+tmp.getValor()+" == 1 ) goto "+ tmp.getEtiquetaVerdera() +" ;\n" +
                                    "goto " +tmp.getEtiquetaFalse() +" ;\n";
                            str +=  tmp.getEtiquetaVerdera() +":\n";
                            str +=  "\tprintf(\"%c\", (int)116);\n" +
                                    "\tprintf(\"%c\", (int)114);\n" +
                                    "\tprintf(\"%c\", (int)117);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" +
                                    "\tgoto "+ continuar +";\n";
                            str += tmp.getEtiquetaFalse() +":\n" +
                                    "\tprintf(\"%c\", (int)102);\n" +
                                    "\tprintf(\"%c\", (int)97);\n" +
                                    "\tprintf(\"%c\", (int)108);\n" +
                                    "\tprintf(\"%c\", (int)115);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" ;
                            str +=continuar +":\n";
                            consola +=str;
                        }
                        else
                        {
                            String e_salida = gen.crear_etiquetas();
                            String aux2= tmp.getValor().equals("1")? "goto "+tmp.getEtiquetaVerdera()+ ";\n" : "goto "+tmp.getEtiquetaFalse()+ ";\n";
                            aux2 += tmp.getEtiquetaVerdera()+":\n"+
                                    "\tprintf(\"%c\", (int)116);\n" +
                                    "\tprintf(\"%c\", (int)114);\n" +
                                    "\tprintf(\"%c\", (int)117);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" +
                                    "goto "+ e_salida +";\n";
                            aux2 += tmp.getEtiquetaFalse()+":\n" +
                                    "\tprintf(\"%c\", (int)102);\n" +
                                    "\tprintf(\"%c\", (int)97);\n" +
                                    "\tprintf(\"%c\", (int)108);\n" +
                                    "\tprintf(\"%c\", (int)115);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" ;
                            aux2 +=e_salida +":\n";
                            consola +=aux2;
                        }*/

                    }else if(((SimboloC3D) aux).getTipo() ==Type.COMPLEX )
                    {

                        SimboloC3D tmp = ((SimboloC3D) aux);
                        consola += "\tprintf(\"%d\",(int) 0);\n";
                        // si es un temporal:
                        //if(tmp.isEsTemp()  && tmp.getEtiquetaVerdera().equals(""))
                        /*
                        if(tmp.isEsTemp()  && tmp.getEtiquetaVerdera().equals(""))
                        {
                        String verdadero = gen.crear_etiquetas();
                        String falso  = gen.crear_etiquetas();
                        String continuar = gen.crear_etiquetas();
                        String str = "if ("+tmp.getValor()+" == 1 ) goto "+ verdadero +" ;\n" +
                                "goto " +falso +" ;\n";
                        str +=  verdadero +":\n";
                        str +=  "\tprintf(\"%c\", (int)116);\n" +
                                "\tprintf(\"%c\", (int)114);\n" +
                                "\tprintf(\"%c\", (int)117);\n" +
                                "\tprintf(\"%c\", (int)101);\n" +
                                "\tgoto "+ continuar +";\n";
                        str += falso +":\n" +
                                "\tprintf(\"%c\", (int)102);\n" +
                                "\tprintf(\"%c\", (int)97);\n" +
                                "\tprintf(\"%c\", (int)108);\n" +
                                "\tprintf(\"%c\", (int)115);\n" +
                                "\tprintf(\"%c\", (int)101);\n" ;
                        str +=continuar +":\n";
                        consola +=str;

                        }
                        else if(tmp.isEsTemp() )
                        {

                            String continuar = gen.crear_etiquetas();
                            String str = "if ("+tmp.getValor()+" == 1 ) goto "+ tmp.getEtiquetaVerdera() +" ;\n" +
                                    "goto " +tmp.getEtiquetaFalse() +" ;\n";
                            str +=  tmp.getEtiquetaVerdera() +":\n";
                            str +=  "\tprintf(\"%c\", (int)116);\n" +
                                    "\tprintf(\"%c\", (int)114);\n" +
                                    "\tprintf(\"%c\", (int)117);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" +
                                    "\tgoto "+ continuar +";\n";
                            str += tmp.getEtiquetaFalse() +":\n" +
                                    "\tprintf(\"%c\", (int)102);\n" +
                                    "\tprintf(\"%c\", (int)97);\n" +
                                    "\tprintf(\"%c\", (int)108);\n" +
                                    "\tprintf(\"%c\", (int)115);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" ;
                            str +=continuar +":\n";
                            consola +=str;
                        }
                        else
                        {
                            String e_salida = gen.crear_etiquetas();
                            String aux2= tmp.getValor().equals("1")? "goto "+tmp.getEtiquetaVerdera()+ ";\n" : "goto "+tmp.getEtiquetaFalse()+ ";\n";
                            aux2 += tmp.getEtiquetaVerdera()+":\n"+
                                    "\tprintf(\"%c\", (int)116);\n" +
                                    "\tprintf(\"%c\", (int)114);\n" +
                                    "\tprintf(\"%c\", (int)117);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" +
                                    "goto "+ e_salida +";\n";
                            aux2 += tmp.getEtiquetaFalse()+":\n" +
                                    "\tprintf(\"%c\", (int)102);\n" +
                                    "\tprintf(\"%c\", (int)97);\n" +
                                    "\tprintf(\"%c\", (int)108);\n" +
                                    "\tprintf(\"%c\", (int)115);\n" +
                                    "\tprintf(\"%c\", (int)101);\n" ;
                            aux2 +=e_salida +":\n";
                            consola +=aux2;
                        }*/

                    }
                }
                else if(aux instanceof  String)
                {

                    char[] aCaracteres = ((String) aux).toCharArray();

                    for(char c: aCaracteres){
                        int asciiValue = c;
                        consola +=  "printf(\"%c\",(int)" + asciiValue + ");\n";
                    }

                }
                else if(aux instanceof  ArrayS )
                {
                   // verificar la dimension
                    ArrayS arrImprimir = (ArrayS) aux;
                    if(arrImprimir.getDim() == 1){
                        // obtener el tamaño
                        // obtener tipo
                        int size = arrImprimir.getSize1();
                        Type tipo = arrImprimir.getTypeData();
                        if(tipo == Type.INTEGER)
                        {
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo =
                                    "// ------------------ IMPRIMIR ARREGLO ------------------ "+arrImprimir.getId() +"\n"+
                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P] ;\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "\tprintf(\"%d\",(int)" + print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;


                        }
                        else  if(tipo == Type.REAL)
                        {
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo =
                                    "// -------------------- IMPRIMIR ARREGLO ------------------ "+arrImprimir.getId() +"\n"+
                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P];\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "\tprintf(\"%f\"," + print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;

                        }
                        else  if(tipo == Type.CHAR)
                        {
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo =
                                    "//------------------ IMPRIMIR ARREGLO ------------------"+arrImprimir.getId() +"\n"+
                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P] ;\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "\tprintf(\"%c\",(int)" + print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;
                        }

                    }
                    // arreglos bidimensionales
                    else
                    {
                        // obtener el tamaño
                        // obtener tipo
                        int size1 = arrImprimir.getSize1();
                        int size2 = arrImprimir.getSize2();
                        int size = size1 *size2;
                        Type tipo = arrImprimir.getTypeData();
                        if(tipo == Type.INTEGER)
                        {
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String tmp3 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String salto1 = gen.crear_etiquetas();
                            String salto2 = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo   tmp3  +" = "+col+"; \n"+
                            String arreglo =
                                    "// ----------------- IMPRIMIR ARREGLO ----------------- "+arrImprimir.getId() +"\n"+
                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P] ;\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp2 +"; \n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "if ("+tmp2 +" == "+tmp3+") goto "+ salto1 +"; \n"+
                                            "goto "+ salto2 +"; \n"+
                                            salto1 +" : \n"+
                                            "printf(\"%c\",10);"+"\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp3+"; \n"+
                                            salto2 +" : \n"+
                                            "\tprintf(\"%d\",(int) "+ print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;

                        }
                        else if(tipo == Type.REAL)
                        {

                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String tmp3 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String salto1 = gen.crear_etiquetas();
                            String salto2 = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo   tmp3  +" = "+col+"; \n"+
                            String arreglo =
                                    "// ----------------- IMPRIMIR ARREGLO ----------------- "+arrImprimir.getId() +"\n"+

                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P] ;\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp2 +"; \n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "if ("+tmp2 +" == "+tmp3+") goto "+ salto1 +"; \n"+
                                            "goto "+ salto2 +"; \n"+
                                            salto1 +" : \n"+
                                            "printf(\"%c\",10);"+"\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp3+"; \n"+
                                            salto2 +" : \n"+
                                            "\tprintf(\"%f\"," + print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;
                        }
                        else if(tipo == Type.CHAR)
                        {
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String tmp3 = gen.crear_temporal();
                            String print = gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String salto1 = gen.crear_etiquetas();
                            String salto2 = gen.crear_etiquetas();
                            String sizetmp = gen.crear_temporal();
                            // agregando arreglo   tmp3  +" = "+col+"; \n"+
                            String arreglo =
                                    "// ----------------- IMPRIMIR ARREGLO ----------------- "+arrImprimir.getId() +"\n"+
                                            tmp1 +" = "+ent.ObtenerTamanio(arrImprimir.getId())+"+ "+arrImprimir.getPos()+" ; \n"+
                                            " P = "+tmp1+"; \n "+
                                            tmp2 +" = STACK[(int)P] ;\n"+
                                            sizetmp +" = "+tmp2+" + " +size +" ;\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp2 +"; \n"+
                                            inicio +": \n"+
                                            "if("+ tmp2 +" <"+ sizetmp+" ) goto "+ continuar +"; \n"+
                                            "goto " + finalizar+ " ; \n"+
                                            continuar +": \n"+
                                            print +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                            "if ("+tmp2 +" == "+tmp3+") goto "+ salto1 +"; \n"+
                                            "goto "+ salto2 +"; \n"+
                                            salto1 +" : \n"+
                                            "printf(\"%c\",10);"+"\n"+
                                            tmp3  +" = "+size2+"+ "+ tmp3+"; \n"+
                                            salto2 +" : \n"+
                                            "\tprintf(\"%c\",(int)" + print + ");\n"+
                                            "\tprintf(\"%c\", 32 );\n"+
                                            tmp2 +" = "+ tmp2 +" + 1"+"; \n"+
                                            "goto "+ inicio +"; \n"+
                                            finalizar +": \n";
                            consola += arreglo ;

                        }


                    }



                }

            }
            if(!conError){
                consola  += "printf(\"%c\",10);"+"\n";
                gen.Agregar_codigo( consola);
            }

        }catch (Exception e){
            System.out.println("error print\n"+ e);
        }
        return true;
    }
    public Object visitCicloDo(JGrammarParser.CicloDoContext ctx)
    {
        //Environment entActual = actualEnv;
        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        // array de condiciones
        ArrayList<String> cond = new ArrayList<>();
        // obtener la etiqueta
        String idDoI = "";
        String idDoF = "";
        if(ctx.idDOi != null)
        {
            idDoI = ctx.idDOi.getText();
        }
        if(ctx.idDOf != null)
        {
            idDoF = ctx.idDOi.getText();
        }
        try
        {
            // do sin etiqueta
            if(idDoI.equals("") && idDoF.equals(""))
            {
                //obtener el simbolo del indice
                //SimboloC3D id = entActual.Buscar(ctx.var.getText());
                Symbol id = entActual.Buscar(ctx.var.getText());
                if(id !=  null && id.getType() == Type.INTEGER)
                {
                    // obtener inicio, fin y salto
                    for(ParseTree c :ctx.lc.children)
                    {
                        if(!(c instanceof TerminalNodeImpl))
                        {
                            SimboloC3D auxCond = (SimboloC3D) visit(c);
                            if(auxCond.getTipo()== Type.INTEGER)
                            {
                                cond.add(auxCond.getValor());
                            }
                            else
                            {
                                System.out.println( "Error en condiciones ciclo DO");
                                return  null;
                            }
                        }
                    }
                    // verificar el tamaño de ciclo
                    if(cond.size() == 2 || cond.size() == 3)
                    {
                        String indice = gen.crear_temporal();
                        String cont_final = gen.crear_temporal();
                        String salto = gen.crear_temporal();
                        String  _panico =gen.crear_temporal();
                        String inicio = gen.crear_etiquetas();
                        String continuar = gen.crear_etiquetas();
                        String finalizar = gen.crear_etiquetas();
                        String tmp1 = gen.crear_temporal();
                        String conDo = "/*----------- CICLO DO ---------- */\n"+
                                tmp1 +"= "+entActual.ObtenerTamanio(id.getId())+"+" +id.getPos() +"; \n"+
                                "P = "+tmp1 +";\n";
                        conDo += "STACK[(int)"+tmp1+"] = "+cond.get(0)+"; \n"+
                                cont_final +" =  " +cond.get(1) +";\n"+
                                salto +" =  " +((cond.size() == 3)? cond.get(2): "1") +";\n";

                        conDo += inicio +": \n"+
                                indice +" =  " +"STACK[(int)"+tmp1+"];\n" +
                                "if("+indice +" <= "+ cont_final+" ) goto "+ continuar +";\n"+
                                "goto "+ finalizar +"; \n"+
                                continuar +" :\n";
                        gen.Agregar_codigo(conDo);
                                // ejecucion de mas codigo

                                //entStackC3D.push(entStack.pop());
                        posicionActual++;
                        incremento ++;
                                //actualEnv = actualEnv.siguiente;
                        Object salida = visitLinstrucciones(ctx.bp);
                                //entStackC3D.pop();

                        posicionActual = posAterior;
                        // verificar si hay un cycle
                        if(!ControlCycle.isEmpty())
                            gen.Agregar_codigo(ControlCycle.pop() +":\n");
                        // seccion de incremento
                        conDo = indice +" = "+ indice +" + "+ salto+" ;\n" +
                                "STACK[(int)"+tmp1+"] = "+indice+";\n"+
                                "goto "+ inicio +" ;\n" +
                                finalizar +": \n";
                        gen.Agregar_codigo(conDo);
                        //verificar si hay un exit
                        if(!ControlExit.isEmpty())
                            gen.Agregar_codigo(ControlExit.pop() +":\n");
                        gen.Agregar_codigo("// ----------- FIN CICLO DO ---------- \n");

                        //actualEnv = entActual;

                    }
                    else
                    {
                        System.out.println("error en ciclo Do, cantidad de condicionales no validos");
                        return  null;
                    }
                }
                else
                {
                    System.out.println("Error en  Ciclo Do, condicional no valido");
                    return  null;
                }
            }
            else
            {
                if(idDoI.toLowerCase().equals(idDoF.toLowerCase()))
                {
                    //obtener el simbolo del indice
                    //SimboloC3D id = entActual.Buscar(ctx.var.getText());
                    Symbol id = entActual.Buscar(ctx.var.getText());
                    if(id !=  null && id.getType() == Type.INTEGER)
                    {
                        // obtener inicio, fin y salto
                        for(ParseTree c :ctx.lc.children)
                        {
                            if(!(c instanceof TerminalNodeImpl))
                            {
                                SimboloC3D auxCond = (SimboloC3D) visit(c);
                                if(auxCond.getTipo()== Type.INTEGER)
                                {
                                    cond.add(auxCond.getValor());
                                }
                                else
                                {
                                    System.out.println( "Error en condiciones ciclo DO");
                                    return  null;
                                }
                            }
                        }
                        // verificar el tamaño de ciclo
                        if(cond.size() == 2 || cond.size() == 3)
                        {
                            String indice = gen.crear_temporal();
                            String cont_final = gen.crear_temporal();
                            String salto = gen.crear_temporal();
                            String  _panico =gen.crear_temporal();
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String tmp1 = gen.crear_temporal();
                            String conDo = "/*----------- CICLO DO ---------- */\n"+
                                    tmp1 +"= "+entActual.ObtenerTamanio(id.getId())+"+" +id.getPos() +"; \n"+
                                    "P = "+tmp1 +";\n";
                            conDo += "STACK[(int)"+tmp1+"] = "+cond.get(0)+"; \n"+
                                    cont_final +" =  " +cond.get(1) +";\n"+
                                    salto +" =  " +((cond.size() == 3)? cond.get(2): "1") +";\n";

                            conDo += inicio +": \n"+
                                    indice +" =  " +"STACK[(int)"+tmp1+"];\n" +
                                    "if("+indice +" <= "+ cont_final+" ) goto "+ continuar +";\n"+
                                    "goto "+ finalizar +"; \n"+
                                    continuar +" :\n";
                            gen.Agregar_codigo(conDo);
                            // ejecucion de mas codigo

                            //entStackC3D.push(entStack.pop());
                            posicionActual++;
                            incremento ++;
                            //actualEnv = actualEnv.siguiente;
                            Object salida = visitLinstrucciones(ctx.bp);
                            //actualEnv = actualEnv.global;

                            posicionActual = posAterior;
                            //entStackC3D.pop();

                            // verificar si hay un cycle
                            if(!ControlCycle.isEmpty())
                                gen.Agregar_codigo(ControlCycle.pop() +":\n");
                            // seccion de incremento
                            conDo = indice +" = "+ indice +" + "+ salto+" ;\n" +
                                    "STACK[(int)"+tmp1+"] = "+indice+";\n"+
                                    "goto "+ inicio +" ;\n" +
                                    finalizar +": \n";
                            gen.Agregar_codigo(conDo);
                            //verificar si hay un exit
                            if(!ControlExit.isEmpty())
                                gen.Agregar_codigo(ControlExit.pop() +":\n");
                            gen.Agregar_codigo("// ----------- FIN CICLO DO ---------- \n");

                        }
                        else
                        {
                            System.out.println("error en ciclo Do, cantidad de condicionales no validos");
                            return  null;
                        }
                    }
                    else
                    {
                        System.out.println("Error en  Ciclo Do, condicional no valido");
                        return  null;
                    }

                }
                else
                {
                    System.out.println("Error en ciclo Do, etiquetas no coinciden ");
                    return  null;
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Error al ejecutar ciclo Do  " + e);
            return  null;
        }
        return null;
    }
    public Object visitCicloDoWhile(JGrammarParser.CicloDoWhileContext ctx)
    {
        //Environment entActual = actualEnv;
        //Environment entActual = entStackC3D.peek();
        // obtener la etiqueta
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        String idDoWI = "";
        String idDoWF = "";
        if(ctx.idDOi != null){
            idDoWI = ctx.idDOi.getText();
        }
        if(ctx.idDOf != null){
            idDoWF = ctx.idDOi.getText();
        }
        try
        {
            // do sin etiqueta
            if(idDoWI.equals("") && idDoWF.equals("") ){

                //entStackC3D.push(entStack.pop());
                //actualEnv = actualEnv.siguiente;
                posicionActual ++;
                incremento ++;
                String inicio = gen.crear_etiquetas();
                String continuar = gen.crear_etiquetas();
                String finalizar = gen.crear_etiquetas();
                String condicion = gen.crear_temporal();
                String conDoW = "//----------- CICLO DO WHILE ---------- \n";
                conDoW += inicio +":\n" ;
                gen.Agregar_codigo(conDoW);
                // condicion
                SimboloC3D  i  = (SimboloC3D) visit(ctx.cond);
                //condicion = i.getValor();
                conDoW  =condicion +" = "+i.getValor() +";\n"+
                        "if ( "+condicion +" == 1 ) goto "+continuar +";\n" +
                        "goto  "+ finalizar +";\n"+
                        continuar + ":\n";
                gen.Agregar_codigo(conDoW);
                // seccion de codigo a ejecutar
                Object salida = visitLinstrucciones(ctx.bp);
                // verificar si hay un cycle
                if(!ControlCycle.isEmpty())
                    gen.Agregar_codigo(ControlCycle.pop() +":\n");
                conDoW ="goto "+inicio +";\n"+
                        finalizar +":\n";
                gen.Agregar_codigo(conDoW);
                gen.Agregar_codigo("//----------- FIN CICLO DO WHILE ---------- \n");
                //verificar si hay un exit
                if(!ControlExit.isEmpty())
                    gen.Agregar_codigo(ControlExit.pop() +":\n");


                posicionActual = posAterior;
                //entStackC3D.pop();
                //actualEnv = actualEnv.global;
                // ejecucion de mas codigo
            }
        }
        catch (Exception e)
        {
            System.out.println("error al ejecutar ciclo do while " + e);
            return  null;
        }
        return true;
    }
    // declarar arreglos estaticos
    public Object visitArreglosestaticos(JGrammarParser.ArreglosestaticosContext ctx)
    {
        //Environment entActual = actualEnv;
        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        try
        {
            // asignar arreglos
            if(ctx.l !=null)
            {
                for(ParseTree lis: ctx.l.children)
                {
                    if(lis instanceof  JGrammarParser.ListaArraSimpleContext)
                    {
                        JGrammarParser.ListaArraSimpleContext lisAct = (JGrammarParser.ListaArraSimpleContext) lis;
                        // obtener id de arreglo
                        String idArray = lisAct.i.getText();
                        // verificar si el arreglo existe
                        if(entActual.TablaSimboloArrays.containsKey(idArray.toUpperCase()))
                        {
                           ArrayS arrAgregado = entActual.BuscarArrays(idArray);
                           // verififar  que no sea allocatable
                            if(!arrAgregado.isAllocatable())
                            {
                                /*
                                * Obtener dimension
                                * obtener indice 1
                                * obtener indice 2
                                * obtener tipo de dato
                                * */
                                int dimension = arrAgregado.getDim();
                                // arreglo unidimensional
                                if(dimension == 1)
                                {
                                    int indice1 = arrAgregado.getSize1();
                                    Type dato = arrAgregado.getTypeData();
                                    // verificar tipo de arreglo
                                    if(dato == Type.INTEGER)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int) P ] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+indice1 +";\n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1; \n ";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< indice1 ;i++){
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +" ;\n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 0 ;  \n");
                                        }
                                    }
                                    else if(dato == Type.REAL)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int)P] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+indice1 +"; \n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1;\n";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< indice1 ;i++){
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +"; \n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 0.00000000 ;  \n");
                                        }
                                    }
                                    else if(dato == Type.CHAR)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int)P] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+indice1 +";\n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1;";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< indice1 ;i++)
                                        {
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +"; \n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 32 ;  \n");
                                        }
                                    }
                                }
                                // arreglo bidimensional
                                else if(dimension == 2)
                                {
                                    int indice1 = arrAgregado.getSize1();
                                    int indice2 = arrAgregado.getSize2();
                                    Type dato = arrAgregado.getTypeData();
                                    // verificar tipo de arreglo
                                    if(dato == Type.INTEGER)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int)P] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+(indice1*indice2) +";\n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1; \n ";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< (indice1*indice2) ;i++){
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +" ;\n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 0 ;  \n");
                                        }
                                    }
                                    else if(dato == Type.REAL)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int)P] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+(indice1*indice2) +"; \n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1;\n";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< (indice1*indice2) ;i++){
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +"; \n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 0.00000000 ;  \n");
                                        }
                                    }
                                    else if(dato == Type.CHAR)
                                    {
                                        // agregar arreglo
                                        //obtener valor
                                        String tmp1 = gen.crear_temporal();
                                        String tmp2 = gen.crear_temporal();
                                        String tmp3 = gen.crear_temporal();
                                        // agregando arreglo
                                        String arreglo =
                                                "// -------------------- DECLARANDO ARREGLO "+arrAgregado.getId() +"\n"+
                                                        tmp1 +"= "+entActual.ObtenerTamanioAsignar()+"+" +arrAgregado.getPos() +"; \n"+
                                                        "P = "+tmp1 +";\n"+
                                                        "STACK[(int)P] =  H ;\n"+
                                                        tmp2 +" =  H ;\n"+
                                                        "H = H +"+(indice1*indice2) +";\n"+
                                                        "HEAP[(int)H ] = -2;\n" +
                                                        "H = H + 1;";
                                        gen.Agregar_codigo( arreglo);

                                        for(int i =0 ;i< (indice1*indice2) ;i++)
                                        {
                                            gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +"; \n");
                                            gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = 32 ;  \n");
                                        }
                                    }
                                }
                                // agregar dimension 2

                            }
                            else
                            {
                                System.out.println( " Solo se permiten arreglos estaticos");
                            }
                        }
                        else
                        {
                            System.out.println("Error el arreglo no existe");
                        }
                    }

                }
            }
            else
            {
                System.out.println(" no hay arreglos que agregar");
            }
        }
        catch (Exception e)
        {
            System.out.println("error al agregar arreglo " +e);
            return  null;
        }
        return null;
    }
    // asignar arreglos unidimensionales
    public Object visitAsignarArrayUni(JGrammarParser.AsignarArrayUniContext ctx)
    {
        //Environment entActual = actualEnv;
        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        String nomArray = ctx.i.getText();
        ArrayList<SimboloC3D> val = new ArrayList<>(); // lista de valores del arreglo
        try {
            // parametros  : nombre y lista de valrores
            if(ctx.pos == null)
            {
                ArrayS  arrayAsignar = entActual.BuscarArrays(nomArray);
                if(arrayAsignar != null)
                {
                    //verificar que no sea dinamico
                    if(!arrayAsignar.isAllocatable())
                    {
                        for(ParseTree v: ctx.l.children){
                            Object newval = visit(v);
                            if(newval instanceof SimboloC3D)
                            {
                                val.add((SimboloC3D)newval);
                            }
                        }

                        /*
                         * Obtener dimension
                         * obtener indice 1
                         * obtener indice 2
                         * obtener tipo de dato
                         * */
                        int dimension = arrayAsignar.getDim();
                        int indice = arrayAsignar.getSize1();
                        Type tipo = arrayAsignar.getTypeData();
                        if(dimension == 1)
                        {
                            // verificar el tamaño del arreglo y el tamaño de la lista
                            if(indice == val.size())
                            {
                                // verificar los  tipos obtenidos
                                boolean aceptar = true;
                                for (SimboloC3D s : val)
                                {
                                    if(tipo != s.getTipo())
                                    {
                                        aceptar = false;
                                        break;
                                    }
                                }
                                if(aceptar)
                                {

                                    String tmp1 = gen.crear_temporal();
                                    String tmp2 = gen.crear_temporal();
                                    String tmp3 = gen.crear_temporal();
                                    // agregando arreglo
                                    String arreglo =
                                            "// -------------------- ASIGNANDO ARREGLO "+nomArray +"\n"+
                                                    tmp1 + " = "+entActual.ObtenerTamanio(nomArray)+"+"+arrayAsignar.getPos()+"; \n"+
                                                    "P = "+tmp1 +";\n"+
                                                    tmp2 +"  = STACK[(int)P];\n";
                                    gen.Agregar_codigo( arreglo);
                                    for(int i =0 ;i< indice ;i++)
                                    {
                                        gen.Agregar_codigo( tmp3 + " = " + tmp2 + " + "+ i +" ;\n");
                                        gen.Agregar_codigo( "HEAP[(int)" + tmp3 + "] = "+val.get(i).getValor()+" ;  \n");
                                    }

                                }
                                else
                                {
                                    System.out.println("error al agregar elementos, tipos diferentes");
                                }

                            }
                            else
                            {
                                System.out.println(" tamaño y lista de objetos no coinciden en asignacion");
                            }
                        }
                        else
                        {
                            System.out.println("Error al asignar, se permite arreglo de una dimenison");
                        }



                    }
                    else
                    {
                        System.out.println("Arreglo dinamico no permitido");
                    }
                }
                else
                {
                    System.out.println("no se encontro el arreglo");
                }
                return null;
            }
            else
            {
                //    parametros : nombre y valor
                // obtener posicion del vector
                Object npos = visit(ctx.pos);
                if(npos instanceof  SimboloC3D && ((SimboloC3D) npos).getTipo() == Type.INTEGER)
                {
                    ArrayS  arrayAsignar = entActual.BuscarArrays(nomArray);
                    if(arrayAsignar != null)
                    {
                        //verificar que no sea dinamico
                        if(!arrayAsignar.isAllocatable())
                        {
                            /*
                             * Obtener dimension
                             * obtener indice 1
                             * obtener indice 2
                             * obtener tipo de dato
                             * */
                            int dimension = arrayAsignar.getDim();
                            int indice = arrayAsignar.getSize1();
                            Type tipo = arrayAsignar.getTypeData();
                            if(dimension == 1)
                            {
                                // obtener el dato a declarar
                                SimboloC3D updateValue = (SimboloC3D) visit(ctx.val);
                                // verificar el tipo de datos y
                                if(tipo == updateValue.getTipo())
                                {
                                    String nf = gen.crear_etiquetas();
                                    String nv = gen.crear_etiquetas();
                                    String nf2 = gen.crear_etiquetas();
                                    String nv2 = gen.crear_etiquetas();

                                    String tmp1 = gen.crear_temporal();
                                    String tmp2 = gen.crear_temporal();
                                    String ind = gen.crear_temporal();
                                    String indSuperior = gen.crear_temporal();
                                    // agregando arreglo
                                    String codigo =  "// ---------------- ASIGNANDO ARREGLO ------------ "+nomArray +"\n"+
                                            tmp1 + " = "+entActual.ObtenerTamanio(nomArray)+"+"+arrayAsignar.getPos()+"; \n"+
                                            "P = "+tmp1 +";\n"+
                                            tmp2 +"  = STACK[(int) P ];\n"+                    // indice  inferior
                                            ind +" = "+((SimboloC3D) npos).getValor() +" -1; \n"+
                                            ind +" =  " + tmp2 + " + "+ ind + "; \n"+               // indice puntero
                                            indSuperior +" = "+tmp2 +" + "+indice +"; \n"+          // indice superior
                                            "if (" + ind +" < "+indSuperior +" ) goto " + nv+ ";\n " +
                                            "goto "+ nf +";\n";
                                    codigo += nv +": \n " +
                                            "if (" + ind +" >= "+tmp2+" ) goto " + nv2+ ";\n " +
                                            "goto "+ nf2 +";\n";
                                    codigo += nv2 +": \n "+
                                            "HEAP[(int)" + ind + "] = "+updateValue.getValor()+" ;  \n"+
                                            nf +": "+ nf2 +": \n " ;

                                    gen.Agregar_codigo(codigo);
                                }
                                else
                                {
                                    System.out.println(" Tipos diferentes al asignar elemento a arreglo");
                                }
                            }
                            else
                            {
                                System.out.println("Error al asignar, se permite arreglo de una dimenison");
                            }
                        }
                        else
                        {
                            System.out.println("Arreglo dinamico no permitido");
                        }
                    }
                    else
                    {
                        System.out.println("no se encontro el arreglo");
                    }
                    return null;
                }
                else
                {
                    System.out.println(" errro en indice");
                    return  null;
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("ocurrio un error al asignar el arreglo " + e);
            return  null;
        }
    }
    // asginar arreglos bidimensionales
    public Object visitAsignarArrayBid(JGrammarParser.AsignarArrayBidContext ctx)
    {
        //Environment entActual = actualEnv;
        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        String nomArray = ctx.i.getText();
        ArrayList <SimboloC3D> posiciones = new ArrayList<>();

        try
        {
            // parametros  : nombre, posicion y valor
            // obtener la posicion de la matriz
            for(ParseTree i: ctx.d.children)
            {
                if(!(i instanceof TerminalNodeImpl))
                {
                    SimboloC3D dimension = (SimboloC3D) visit(i);
                    if(dimension.getTipo() == Type.INTEGER)
                    {
                        posiciones.add(dimension);
                    }
                    else
                    {
                        System.out.println("error en dimension de arreglo bidimensional ");
                        return  null;
                    }
                }
            }
            // verificar que sea de dos dimensiones
            if(posiciones.size() != 2)
            {
                System.out.println("Dimension del arreglo bidimensional no valido");
                return  null;
            }
            else
            {

                ArrayS  arrayAsignar = entActual.BuscarArrays(nomArray);
                if(arrayAsignar != null)
                {
                    //verificar que no sea dinamico
                    if(!arrayAsignar.isAllocatable())
                    {
                        /*
                         * Obtener dimension
                         * obtener indice 1
                         * obtener indice 2
                         * obtener tipo de dato
                         * */
                        int columna = arrayAsignar.getSize1();
                        Type tipo = arrayAsignar.getTypeData();
                        // obtener el dato a declarar
                        SimboloC3D updateValue = (SimboloC3D) visit(ctx.val);
                        // verificar el tipo de datos y
                        if(tipo == updateValue.getTipo())
                        {
                            String t1 = gen.crear_temporal();
                            String t2 = gen.crear_temporal();
                            String pos1 = gen.crear_temporal();
                            String pos2 = gen.crear_temporal();

                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String tmp3 = gen.crear_temporal();

                            String arreglo = "// ---------------- ASIGNANDO ARREGLO ------------ "+nomArray +"\n"+
                                    pos1 +" =  "+posiciones.get(0).getValor()+" - 1"+" ; \n"+
                                    pos2 +" =  "+posiciones.get(1).getValor()+" - 1"+" ; \n"+
                                    t1 + " = "+pos1 +" * " +columna +" ; \n"+
                                    t2 + " = "+t1 +" + " +pos2 +" ; \n"+
                                    tmp1 + " = "+entActual.ObtenerTamanio(nomArray)+"+"+arrayAsignar.getPos()+"; \n"+
                                    "P = "+tmp1 +";\n"+
                                    tmp2 +"  = STACK[(int) P ];\n"+
                                    tmp3 + " = " + tmp2 + " + "+ t2 +" ;\n"+
                                    "HEAP[(int)" + tmp3 + "] = "+updateValue.getValor()+" ;  \n";
                            gen.Agregar_codigo( arreglo);
                        }
                        else
                        {
                            System.out.println(" Tipos diferentes al asignar elemento a arreglo bidimensional");
                        }
                    }
                    else
                    {
                        System.out.println("Arreglo bidimensional dinamico no permitido ");
                    }
                }
                else
                {
                    System.out.println("no se encontro el arreglo bidimensional ");
                }
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println( "Error al asignar el arreglo bidimensional: " +ctx.i.getText() +" "+ e);
            return  null;
        }

    }
    /**
     * *************************** EXPRESIONES PRIMITIVOS **************************
     * */
    public SimboloC3D visitInteger (JGrammarParser.IntegerContext ctx)
    {

        return new SimboloC3D(ctx.getText(),false, Type.INTEGER,"","",Dato.PRIMITIVO);
    }
    public SimboloC3D visitFloat(JGrammarParser.FloatContext ctx)
    {
        return new SimboloC3D(ctx.getText(),false, Type.REAL,"","",Dato.PRIMITIVO);
    }
    public SimboloC3D visitComplex(JGrammarParser.ComplexContext ctx)
    {
        return new SimboloC3D(ctx.getText(),false, Type.COMPLEX,"","",Dato.PRIMITIVO);
    }
    public SimboloC3D visitChar(JGrammarParser.CharContext ctx)
    {
        int asciiValue = ctx.getText().charAt(1);
        return new SimboloC3D(String.valueOf(asciiValue),false, Type.CHAR,"","",Dato.PRIMITIVO);
    }
    public SimboloC3D visitLogico(JGrammarParser.LogicoContext ctx)
    {

        if(ctx.getText().equals(".true.")){
            /*String temp = gen.crear_temporal();
            String verdad = gen.crear_etiquetas();
            String falso = gen.crear_etiquetas();
            String boolean_true = temp + " = 1;\n";
            boolean_true += "if (" + temp + " == 1) goto " + verdad + ";\ngoto " + falso + ";";
            gen.Agregar_codigo(boolean_true);
            return new SimboloC3D( boolean_true,false,Type.LOGICAL, verdad,  falso);
            //String temp = gen.crear_temporal();
            String temp = gen.crear_temporal();
            String verdad = gen.crear_etiquetas();
            String falso = gen.crear_etiquetas();
            String boolean_true =temp +" = 1; \n if (" + temp + " == 1) goto " + verdad + ";\ngoto " + falso + ";";
            //gen.Agregar_codigo(boolean_true);
            return new SimboloC3D( boolean_true,false,Type.LOGICAL, verdad,  falso);*/
            String verdad = gen.crear_etiquetas();
            String falso = gen.crear_etiquetas();
            return new SimboloC3D("1",false, Type.LOGICAL,verdad,falso,Dato.PRIMITIVO);

        }else {
            /*String temp =  gen.crear_temporal();
            String verdad = gen.crear_etiquetas();
            String falso = gen.crear_etiquetas();
            String boolean_false = temp + " = 0;\n";
            boolean_false += "if (" + temp + " == 1) goto " + verdad + "; \ngoto " + falso + ";";
            //gen.Agregar_codigo(boolean_false);
            return new SimboloC3D(boolean_false,false, Type.LOGICAL,verdad,falso);*/
            String verdad = gen.crear_etiquetas();
            String falso = gen.crear_etiquetas();
            return new SimboloC3D("0",false, Type.LOGICAL,verdad,falso,Dato.PRIMITIVO);
        }
    }
    public SimboloC3D visitParenExpr(JGrammarParser.ParenExprContext ctx)
    {

        // obtener posicion
        try{
            SimboloC3D expr = (SimboloC3D) visit(ctx.expresion());
            if(expr != null){
                return new SimboloC3D(expr.getValor(),false, expr.getTipo(),"","",Dato.VARIABLE);
            }else {
               return  null;
            }
        }catch (Exception e){
            System.out.println("error al ejectar operacion con parentesis \n"+e);
            return null;
        }
    }
    public String visitCad(JGrammarParser.CadContext ctx)
    {
        String str = ctx.getText();
        return str.substring(1, str.length() - 1);
    }
    public Object visitVarExp(JGrammarParser.VarExpContext ctx)
    {
        //Environment ent = entStackC3D.peek();
        //Environment ent = actualEnv;
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        Symbol id = ent.Buscar(ctx.ID().getText());
        if(id != null)
        {

            String tmp1 = gen.crear_temporal();
            String tmp2 = gen.crear_temporal();
            gen.Agregar_codigo(tmp1 +"= "+ent.ObtenerTamanio(ctx.ID().getText())+" +" +id.getPos() +"; \n");
            gen.Agregar_codigo(tmp2 + "= STACK[(int)"+tmp1+"];\n" );
            return new SimboloC3D(tmp2,true,id.getType(),"","",Dato.VARIABLE);
        }
        // buscar si es un objeto de tipo arreglos
        ArrayS arreglo = ent.BuscarArrays(ctx.ID().getText());
        if(arreglo != null)
        {
            return  arreglo;
        }
        return null;

    }
    public Object visitControlCiclos(JGrammarParser.ControlCiclosContext ctx)
    {
        //Environment ent = entStack.get(entStack.size() -1);
        try
        {
            String valor = ctx.tk.getText();
            if(valor.toLowerCase().equals("exit"))
            {
                String exit = gen.crear_etiquetas();
                gen.Agregar_codigo("goto "+ exit +"; \n");
                ControlExit.push(exit);
                //return new Exit(ctx.tk.getLine(),ctx.tk.getCharPositionInLine(),"exit",true);
            }
            else
            {
                String cycle = gen.crear_etiquetas();
                gen.Agregar_codigo("goto "+ cycle +"; \n");
                ControlCycle.push(cycle);
            }
            return null;
        }
        catch (Exception e)
        {
            System.out.println("error en declarara control de ciclo " + e);

            return  null;
        }

    }
    // obtener valor de un arreglo unidimensional en una posicion especifica
    public SimboloC3D visitArrUniExp(JGrammarParser.ArrUniExpContext ctx)
    {
        //Environment entActual = actualEnv;

        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        // id del arreglo ctx.ID().getText()
        // obtener posicion
        try
        {
            //    parametros : nombre y valor
            // obtener posicion del vector
            Object npos = visit(ctx.pos);
            if(npos instanceof  SimboloC3D && ((SimboloC3D) npos).getTipo() == Type.INTEGER)
            {
                ArrayS  arrayAsignar = entActual.BuscarArrays(ctx.ID().getText());
                if(arrayAsignar != null)
                {
                    //verificar que no sea dinamico
                    if(!arrayAsignar.isAllocatable())
                    {
                        /*
                         * Obtener dimension
                         * obtener indice 1
                         * obtener indice 2
                         * obtener tipo de dato
                         * */
                        int dimension = arrayAsignar.getDim();
                        int indice = arrayAsignar.getSize1();
                        Type tipo = arrayAsignar.getTypeData();
                        if(dimension == 1)
                        {

                            // verificar el tipo de datos  tipo
                            String nf = gen.crear_etiquetas();
                            String nv = gen.crear_etiquetas();
                            String nf2 = gen.crear_etiquetas();
                            String nv2 = gen.crear_etiquetas();

                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String ind = gen.crear_temporal();
                            String indSuperior = gen.crear_temporal();
                            String tmpReturn = gen.crear_temporal();
                            // agregando arreglo
                            String codigo =  "// ---------------- ACCEDIENTO ARREGLO ------------ "+ctx.ID().getText() +"\n"+
                                    tmp1 + " = "+ entActual.ObtenerTamanio(ctx.ID().getText())+"+"+arrayAsignar.getPos()+"; \n"+
                                    "P = "+tmp1 +";\n" +
                                    tmp2 +"  = STACK[(int)P];\n"+                    // indice  inferior
                                    ind +" = "+((SimboloC3D) npos).getValor() +" -1; \n"+
                                    ind +" =  " + tmp2 + " + "+ ind + "; \n"+               // indice puntero
                                    indSuperior +" = "+tmp2 +" + "+indice +"; \n"+          // indice superior
                                    "if (" + ind +" < "+indSuperior +" ) goto " + nv+ ";\n " +
                                    "goto "+ nf +";\n";
                            codigo += nv +": \n " +
                                    "if (" + ind +" >= "+tmp2+" ) goto " + nv2+ ";\n " +
                                    "goto "+ nf2 +";\n";
                            codigo += nv2 +": \n "+
                                    tmpReturn +" = HEAP[(int)" + ind + "]  ;  \n"+
                                    nf +": "+ nf2 +": \n " ;
                            gen.Agregar_codigo(codigo);
                            return  new SimboloC3D(tmpReturn,true,tipo,"","",Dato.VARIABLE);
                        }
                        else
                        {
                            System.out.println("Error al acceder al arreglo unidimensional: se permite arreglo de una dimenison");
                        }
                    }
                    else
                    {
                        System.out.println("acceder al arreglo unidimensional: Arreglo dinamico no permitido");
                    }
                }
                else
                {
                    System.out.println("acceder al arreglo unidimensional: no se encontro el arreglo");
                }
                return null;
            }
            else
            {
                System.out.println(" acceder al arreglo unidimensional : errro en indice");
                return  null;
            }
        }
        catch(Exception e)
        {
            System.out.println("error al acceder al arreglo unidimensional " + e + ctx.pos.getText());
            return  null;
        }


    }
    // obtener valor de un arreglo bidimensional en una posicon especifica
    public SimboloC3D visitArrBiExp(JGrammarParser.ArrBiExpContext ctx)
    {
        //Environment entActual = actualEnv;
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        //Environment entActual = entStackC3D.peek();
        String nomArray = ctx.ID().getText();
        ArrayList <SimboloC3D> posiciones = new ArrayList<>();

        try
        {
            // parametros  : nombre, posicion y valor
            // obtener la posicion de la matriz
            for(ParseTree i: ctx.pos.children)
            {
                if(!(i instanceof TerminalNodeImpl))
                {
                    SimboloC3D dimension = (SimboloC3D) visit(i);
                    if(dimension.getTipo() == Type.INTEGER)
                    {
                        posiciones.add(dimension);
                    }
                    else
                    {
                        System.out.println("acceso a arreglo bidimensional: error en dimension de arreglo ");
                        return  null;
                    }
                }
            }
            // verificar que sea de dos dimensiones
            if(posiciones.size() != 2)
            {
                System.out.println("acceso a arreglo bidimensional: Dimension del arreglo no valido");
                return  null;
            }
            else
            {

                ArrayS  arrayAsignar = entActual.BuscarArrays(nomArray);
                if(arrayAsignar != null)
                {
                    //verificar que no sea dinamico
                    if(!arrayAsignar.isAllocatable())
                    {
                        /*
                         * Obtener dimension
                         * obtener indice 1
                         * obtener indice 2
                         * obtener tipo de dato
                         * */
                        int columna = arrayAsignar.getSize1();
                        Type tipo = arrayAsignar.getType();
                        // obtener el dato a declarar
                        //SimboloC3D updateValue = (SimboloC3D) visit(ctx.val);
                        // verificar el tipo de datos y
                        String t1 = gen.crear_temporal();
                        String t2 = gen.crear_temporal();
                        String pos1 = gen.crear_temporal();
                        String pos2 = gen.crear_temporal();

                        String tmp1 = gen.crear_temporal();
                        String tmp2 = gen.crear_temporal();
                        String tmp3 = gen.crear_temporal();
                        String tmpRetorno = gen.crear_temporal();

                        String arreglo = "// ---------------- ASIGNANDO ARREGLO ------------ "+nomArray +"\n"+
                                pos1 +" =  "+posiciones.get(0).getValor()+" - 1"+" ; \n"+
                                pos2 +" =  "+posiciones.get(1).getValor()+" - 1"+" ; \n"+
                                t1 + " = "+pos1 +" * " +columna +" ; \n"+
                                t2 + " = "+t1 +" + " +pos2 +" ; \n"+
                                tmp1 + " = "+entActual.ObtenerTamanio(nomArray)+" +"+arrayAsignar.getPos()+"; \n"+
                                "P = "+tmp1 +";\n"+
                                tmp2 +"  = STACK[(int)"+tmp1+"];\n"+
                                tmp3 + " = " + tmp2 + " + "+ t2 +" ;\n"+
                                tmpRetorno +" = HEAP[(int)" + tmp3 + "] ;  \n";
                        gen.Agregar_codigo( arreglo);
                        return  new SimboloC3D(tmpRetorno,true, tipo,"","",Dato.VARIABLE);
                    }
                    else
                    {
                        System.out.println("acceso a arreglo bidimensional: Arreglo dinamico no permitido");
                    }
                }
                else
                {
                    System.out.println("acceso a arreglo bidimensional: no se encontro el arreglo");
                }
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println( "Error al acceder  a arreglo bidimensional:  " +ctx.ID().getText() +" "+ e);
            return  null;
        }
    }

    /**
     * ====================================== SECCION DE SUBRUTINAS y FUNCIONES ===================================
     **/
    public Object visitInicioSubRutina(JGrammarParser.InicioSubRutinaContext ctx)
    {
        //Environment entActual = entStackC3D.peek();
        //Environment entActual = actualEnv;
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        try
        {
            // verificar el nombre de inicio y el nombre final de la subrutina
            if(ctx.idsb1.getText().toLowerCase().equals(ctx.idsb2.getText().toLowerCase()))
            {
                // verificar si  existe en el entorno actual
                Symbol subrutina = entActual.Buscar((ctx.idsb1.getText() + Type.SUBRUTINA.name()).toUpperCase());
                if(subrutina != null)
                {
                    // bloque
                    // agregar entorno
                    String c3d = "void "+ ctx.idsb1.getText().toLowerCase() +" () \n {\n";
                    gen.Agregar_codigo(c3d);
                    Subrutina bloque = (Subrutina) subrutina.getValue();
                    //actualEnv = bloque.getEntorno();
                    // buscar
                    int indice = 0;
                    for(int i =0; i< entStackArray.size();i++)
                    {
                        if(bloque.getEntorno().nombre.equals(entStackArray.get(i).nombre))
                        {
                            posicionActual = i;
                            indice = i;
                            break;
                        }
                    }
                    //entStackC3D.push(bloque.getEntorno());
                    /*
                    for(Symbol s: bloque.parametros)
                    {
                        // buscar la direccion en memoria del parametro en el entorno global
                        //  si son variables
                        Object parametro = entActual.Buscar(s.getId());
                        Object declarado = entornoSub.Buscar(s.getId());
                        // verificar si es un simbolo o un arreglo
                        if(declarado == null){
                            declarado = entornoSub.BuscarArrays(s.getId());
                        }
                        // verificar si es una  variable
                       if(parametro != null)
                       {
                           if(parametro instanceof  Symbol)
                           {
                               //obtener valor
                               String tmp = gen.crear_temporal();
                               String tmp1 = gen.crear_temporal();
                               String tmp2 = gen.crear_temporal();
                               String posParametro = tmp1 +"= "+entActual.ObtenerTamanio(s.getId())+" +" + ((Symbol) parametro).getPos() +"; \n"+
                                       tmp2 + "= STACK[(int)"+tmp1+"];\n";
                               gen.Agregar_codigo( posParametro);
                               String declarar = tmp+" ="+entornoSub.ObtenerTamanioAsignar()+"+"+ ((Symbol) declarado).getPos()+";\t \t// POSICION: "+s.getId() +"\n"+
                                       " P = "+tmp+"; \n "+
                                       "STACK[(int) P] = "+tmp2+";"+"\n";
                               gen.Agregar_codigo(declarar);
                           }
                           else if(parametro instanceof  ArrayS)
                           {
                               ArrayS  arrParametro = (ArrayS) parametro;
                               ArrayS arrdeclarado = (ArrayS) declarado;
                               // verificar las dimensiones:
                               if(arrParametro.getDim() ==1)
                               {


                                   // ciclo while para copiar lo datos del arreglo
                                   String inicio = gen.crear_etiquetas();
                                   String continuar = gen.crear_etiquetas();
                                   String finalizar = gen.crear_etiquetas();
                                   String indice = gen.crear_temporal();
                                   // obteniendo temporales del  arreglo a copiar
                                   String tmp1 = gen.crear_temporal();
                                   String tmp2 = gen.crear_temporal();
                                   String valor = gen.crear_temporal();
                                   String ValorArreglo = tmp1 +" = "+entActual.ObtenerTamanio(arrParametro.getId())+"+ "+arrParametro.getPos()+" ; \n"+
                                           " P = "+tmp1+"; \n "+
                                           tmp2 +" = STACK[(int)P] ;\n";
                                   gen.Agregar_codigo(ValorArreglo);
                                   // temporales de arreglo a declarar
                                   String t1 = gen.crear_temporal();
                                   String t2 = gen.crear_temporal();
                                   String t3 = gen.crear_temporal();
                                   // agregando arreglo
                                   String arreglo1 =
                                           "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+arrdeclarado.getId() +"\n"+
                                                   t1 +"= "+entornoSub.ObtenerTamanioAsignar()+"+" +arrdeclarado.getPos() +"; \n"+
                                                   "P = "+t1 +";\n"+
                                                   "STACK[(int) P ] =  H ;\n"+
                                                   t2 +" =  H ;\n"+
                                                   "H = H +"+arrdeclarado.getSize1() +";\n"+
                                                   "HEAP[(int)H ] = -2;\n" +
                                                   "H = H + 1; \n "+
                                                   indice +" = 0 ;\n";

                                   gen.Agregar_codigo( arreglo1);
                                   String conDoW =  inicio +":\n" +
                                                    "if ( "+indice +" < "+ arrdeclarado.getSize1() +" ) goto "+continuar +";\n" +
                                                    "goto  "+ finalizar +";\n"+
                                                    continuar + ":\n";
                                   gen.Agregar_codigo(conDoW);
                                   // seccion de codigo a ejecutar
                                   // obtener el valor del arreglo
                                   conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                           t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                           "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                           indice +" = "+ indice +" + 1;  \n"+
                                           tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                                   gen.Agregar_codigo(conDoW);
                                   conDoW ="goto "+inicio +";\n"+
                                           finalizar +":\n";
                                   gen.Agregar_codigo(conDoW);

                               }
                               else
                               {
                                   // ciclo while para copiar lo datos del arreglo
                                   String inicio = gen.crear_etiquetas();
                                   String continuar = gen.crear_etiquetas();
                                   String finalizar = gen.crear_etiquetas();
                                   String indice = gen.crear_temporal();
                                   // obteniendo temporales del  arreglo a copiar
                                   String tmp1 = gen.crear_temporal();
                                   String tmp2 = gen.crear_temporal();
                                   String valor = gen.crear_temporal();
                                   String ValorArreglo = tmp1 +" = "+entActual.ObtenerTamanio(arrParametro.getId())+"+ "+arrParametro.getPos()+" ; \n"+
                                           " P = "+tmp1+"; \n "+
                                           tmp2 +" = STACK[(int)P] ;\n";
                                   gen.Agregar_codigo(ValorArreglo);
                                   // temporales de arreglo a declarar
                                   String t1 = gen.crear_temporal();
                                   String t2 = gen.crear_temporal();
                                   String t3 = gen.crear_temporal();
                                   // agregando arreglo
                                   String arreglo1 =
                                           "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+arrdeclarado.getId() +"\n"+
                                                   t1 +"= "+entornoSub.ObtenerTamanioAsignar()+"+" +arrdeclarado.getPos() +"; \n"+
                                                   "P = "+t1 +";\n"+
                                                   "STACK[(int) P ] =  H ;\n"+
                                                   t2 +" =  H ;\n"+
                                                   "H = H +"+arrdeclarado.getSize1()*arrdeclarado.getSize2() +";\n"+
                                                   "HEAP[(int)H ] = -2;\n" +
                                                   "H = H + 1; \n "+
                                                   indice +" = 0 ;\n";

                                   gen.Agregar_codigo( arreglo1);
                                   String conDoW =  inicio +":\n" +
                                           "if ( "+indice +" < "+ arrdeclarado.getSize1()*arrdeclarado.getSize2() +" ) goto "+continuar +";\n" +
                                           "goto  "+ finalizar +";\n"+
                                           continuar + ":\n";
                                   gen.Agregar_codigo(conDoW);
                                   // seccion de codigo a ejecutar
                                   // obtener el valor del arreglo
                                   conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                           t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                           "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                           indice +" = "+ indice +" + 1;  \n"+
                                           tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                                   gen.Agregar_codigo(conDoW);
                                   conDoW ="goto "+inicio +";\n"+
                                           finalizar +":\n";
                                   gen.Agregar_codigo(conDoW);

                               }


                           }
                       }
                       else
                       {
                           if(s.getValue() !=null){
                               String tmp = gen.crear_temporal();
                               String declarar = tmp+" ="+entornoSub.ObtenerTamanioAsignar()+"+"+ ((Symbol) declarado).getPos()+";\t \t// POSICION: "+s.getId() +"\n"+
                                       " P = "+tmp+"; \n "+
                                       "STACK[(int) P] = "+s.getValue()+";"+"\n";
                               gen.Agregar_codigo(declarar);

                           }

                       }


                    }*/

                    // declarar los parametros con el entorno global
                    visitLinstrucciones((JGrammarParser.LinstruccionesContext) bloque.lista_instrucciones);
                    c3d = "return;\n}\n";
                    gen.Agregar_codigo(c3d);
                    //entStackC3D.pop();
                    /*if(incremento != 0)
                    {
                        for(int i = indice; i< (indice +incremento);i++)
                        {
                            entStackArray.remove(indice);
                        }
                        incremento =0;
                    }*/
                    posicionActual = posAterior;

                }
                else{
                    System.out.println("Error al ejecutar la subrutina, no existe: "+ctx.idsb1.getText());
                }

            }
            else
            {
                System.out.println("Error al ejecutar la subrutina,  no se reconoce nombre: "+ctx.idsb1.getText());
            }
            return null;
        }
        catch (Exception e )
        {
            System.out.println("Error al ejecutar la subrutina: "+ctx.idsb1.getText() +" " +e);
            return  null;
        }

    }

    public Object visitCallSubrutina(JGrammarParser.CallSubrutinaContext ctx)
    {
        //Environment ent  = entStackC3D.peek();
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        //Environment ent = actualEnv;
        try
        {
            // buscar la subrutina
            Symbol SubRutina = ent.Buscar(ctx.id.getText() +Type.SUBRUTINA.name());
            if(SubRutina != null)
            {
                // obtener la subrutina
                Subrutina bloque = (Subrutina)SubRutina.getValue();
                Environment entornoSub = bloque.getEntorno();
                // reasignando los parametros
                for(int i = 0; i<ctx.listaExpr().expresion().size();i++)
                {
                    Object objeto = visit(ctx.listaExpr().expresion().get(i));
                    Symbol paramEnviado =bloque.parametros.get(i); // parametro enviad
                    if(objeto instanceof  SimboloC3D){
                        SimboloC3D value = (SimboloC3D) objeto;
                        if(value.getValor() != null){

                            //obtener valor
                            Object declarado = entornoSub.Buscar(paramEnviado.getId());
                            String tmp = gen.crear_temporal();
                            String declarar = tmp+" ="+entornoSub.ObtenerTamanioAsignar()+"+"+ ((Symbol) declarado).getPos()+";\t \t// POSICION: "+paramEnviado.getId() +"\n"+
                                    " P = "+tmp+"; \n "+
                                    "STACK[(int) P] = "+value.getValor()+";"+"\n";
                            gen.Agregar_codigo(declarar);
                        }
                    }else if(objeto instanceof  ArrayS)
                    {
                        ArrayS parametro = (ArrayS) objeto;
                        ArrayS declarar = entornoSub.BuscarArrays(paramEnviado.getId());
                        // unidimensional
                        if(declarar.getDim() ==1)
                        {
                            // ciclo while para copiar lo datos del arreglo
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String indice = gen.crear_temporal();
                            // obteniendo temporales del  arreglo a copiar
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String valor = gen.crear_temporal();
                            String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(parametro.getId())+"+ "+parametro.getPos()+" ; \n"+
                                    " P = "+tmp1+"; \n "+
                                    tmp2 +" = STACK[(int)P] ;\n";
                            gen.Agregar_codigo(ValorArreglo);
                            // temporales de arreglo a declarar
                            String t1 = gen.crear_temporal();
                            String t2 = gen.crear_temporal();
                            String t3 = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo1 =
                                    "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+declarar.getId() +"\n"+
                                            t1 +"= "+entornoSub.ObtenerTamanioAsignar()+"+" +declarar.getPos() +"; \n"+
                                            "P = "+t1 +";\n"+
                                            "STACK[(int) P ] =  H ;\n"+
                                            t2 +" =  H ;\n"+
                                            "H = H +"+declarar.getSize1() +";\n"+
                                            "HEAP[(int)H ] = -2;\n" +
                                            "H = H + 1; \n "+
                                            indice +" = 0 ;\n";

                            gen.Agregar_codigo( arreglo1);
                            String conDoW =  inicio +":\n" +
                                    "if ( "+indice +" < "+ declarar.getSize1() +" ) goto "+continuar +";\n" +
                                    "goto  "+ finalizar +";\n"+
                                    continuar + ":\n";
                            gen.Agregar_codigo(conDoW);
                            // seccion de codigo a ejecutar
                            // obtener el valor del arreglo
                            conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                    t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                    "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                    indice +" = "+ indice +" + 1;  \n"+
                                    tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                            gen.Agregar_codigo(conDoW);
                            conDoW ="goto "+inicio +";\n"+
                                    finalizar +":\n";
                            gen.Agregar_codigo(conDoW);

                        }
                        else
                        {
                            // ciclo while para copiar lo datos del arreglo
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String indice = gen.crear_temporal();
                            // obteniendo temporales del  arreglo a copiar
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String valor = gen.crear_temporal();
                            String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(parametro.getId())+"+ "+parametro.getPos()+" ; \n"+
                                    " P = "+tmp1+"; \n "+
                                    tmp2 +" = STACK[(int)P] ;\n";
                            gen.Agregar_codigo(ValorArreglo);
                            // temporales de arreglo a declarar
                            String t1 = gen.crear_temporal();
                            String t2 = gen.crear_temporal();
                            String t3 = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo1 =
                                    "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+declarar.getId() +"\n"+
                                            t1 +"= "+entornoSub.ObtenerTamanioAsignar()+"+" +declarar.getPos() +"; \n"+
                                            "P = "+t1 +";\n"+
                                            "STACK[(int) P ] =  H ;\n"+
                                            t2 +" =  H ;\n"+
                                            "H = H +"+declarar.getSize1()*declarar.getSize2() +";\n"+
                                            "HEAP[(int)H ] = -2;\n" +
                                            "H = H + 1; \n "+
                                            indice +" = 0 ;\n";

                            gen.Agregar_codigo( arreglo1);
                            String conDoW =  inicio +":\n" +
                                    "if ( "+indice +" < "+ declarar.getSize1()*declarar.getSize2() +" ) goto "+continuar +";\n" +
                                    "goto  "+ finalizar +";\n"+
                                    continuar + ":\n";
                            gen.Agregar_codigo(conDoW);
                            // seccion de codigo a ejecutar
                            // obtener el valor del arreglo
                            conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                    t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                    "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                    indice +" = "+ indice +" + 1;  \n"+
                                    tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                            gen.Agregar_codigo(conDoW);
                            conDoW ="goto "+inicio +";\n"+
                                    finalizar +":\n";
                            gen.Agregar_codigo(conDoW);

                        }



                    }
                }

               String call = ctx.id.getText().toLowerCase() +"(); \n";
               gen.Agregar_codigo(call);
                return null;
            }
            else
            {
                System.out.println("Error, sub rutina no encontrado: "+ctx.id.getText());
                return  null;
            }
        }
        catch (Exception e )
        {
            System.out.println("Error al ejecutar la subrutina: "+ctx.id.getText() +" "+e);
            return  null;
        }

    }

    public Object visitInicioFuncion(JGrammarParser.InicioFuncionContext ctx)
    {
        //Environment entActual = entStackC3D.peek();
        Environment entActual = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        //Environment entActual = actualEnv;
        try
        {
            // verificar el nombre de inicio y el nombre final de la funcion
            if(ctx.idf1.getText().toLowerCase().equals(ctx.idf2.getText().toLowerCase()))
            {

                Symbol funcion = entActual.Buscar((ctx.idf1.getText() + Type.FUNCION.name()).toUpperCase());
                if(funcion != null)
                {
                    // bloque
                    // agregar entorno
                    String c3d = "void "+ ctx.idf1.getText().toLowerCase() +" () \n {\n";
                    gen.Agregar_codigo(c3d);
                    Funciones bloque = (Funciones) funcion.getValue();
                    int indice =0;
                    for(int i =0;i< entStackArray.size();i++)
                    {
                        if(bloque.getEntorno().nombre.equals(entStackArray.get(i).nombre)){
                            posicionActual = i;
                            indice = i;
                            break;
                        }
                    }
                    //entStackC3D.push(bloque.getEntorno());
                    //actualEnv = bloque.getEntorno();
                    // declarar los parametros con el entorno global
                    visitLinstrucciones((JGrammarParser.LinstruccionesContext) bloque.lista_instrucciones);
                    c3d = "return;\n}\n";
                    gen.Agregar_codigo(c3d);
                    //entStackC3D.pop();
                    //actualEnv = entActual;
                    /*if(incremento != 0 && indice != 0)
                    {
                        for(int i = indice; i< (indice +incremento);i++)
                        {
                            entStackArray.remove(indice);
                        }
                        incremento =0;
                    }*/
                    posicionActual = posAterior;


                }
                else{
                    System.out.println("Error al ejecutar la  funcion, no existe: "+ctx.idf1.getText());
                }

            }
            else{
                System.out.println("Error al ejecutar la funcion,  no se reconoce nombre: "+ctx.idf1.getText());
            }
            return null;
        }
        catch (Exception e )
        {
            System.out.println("Error al ejecutar la funcion: "+ctx.idf1.getText() +" "+e);
            return  null;
        }

    }

    public Object visitCallFunction(JGrammarParser.CallFunctionContext ctx)
    {
        //Environment ent  = entStackC3D.peek();
        Environment ent = entStackArray.get(posicionActual);
        int posAterior = posicionActual;
        //Environment ent =actualEnv;
        try
        {
            // buscar la subrutina
            // buscar la subrutina
            Symbol funcion = ent.Buscar(ctx.ID().getText() +Type.FUNCION.name());
            if(funcion != null)
            {
                // obtener la subrutina
                Funciones bloque = (Funciones)funcion.getValue();
                Environment entornoFun = bloque.getEntorno();
                // obtener el nombre de la variable de retorno
                String idRetornar = "";
                for(Symbol e : bloque.parametros)
                {
                    if(e.getDato() == Dato.RETORNO){
                        idRetornar = e.getId();
                        //bloque.parametros.remove(e);
                        break;
                    }
                }
                // reasignando los parametros
                for(int i = 0; i<ctx.listaExpr().expresion().size();i++)
                {
                    Object objeto = visit(ctx.listaExpr().expresion().get(i));
                    Symbol paramEnviado =bloque.parametros.get(i); // parametro enviad
                    if(objeto instanceof  SimboloC3D){
                        SimboloC3D value = (SimboloC3D) objeto;
                        if(value.getValor() != null){

                            //obtener valor
                            Object declarado = entornoFun.Buscar(paramEnviado.getId());
                            String tmp = gen.crear_temporal();
                            String declarar = tmp+" ="+entornoFun.ObtenerTamanioAsignar()+"+"+ ((Symbol) declarado).getPos()+";\t \t// POSICION: "+paramEnviado.getId() +"\n"+
                                    " P = "+tmp+"; \n "+
                                    "STACK[(int) P] = "+value.getValor()+";"+"\n";
                            gen.Agregar_codigo(declarar);
                        }
                    }else if(objeto instanceof  ArrayS)
                    {
                        ArrayS parametro = (ArrayS) objeto;
                        ArrayS declarar = entornoFun.BuscarArrays(paramEnviado.getId());
                        // unidimensional
                        if(declarar.getDim() ==1)
                        {
                            // ciclo while para copiar lo datos del arreglo
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String indice = gen.crear_temporal();
                            // obteniendo temporales del  arreglo a copiar
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String valor = gen.crear_temporal();
                            String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(parametro.getId())+"+ "+parametro.getPos()+" ; \n"+
                                    " P = "+tmp1+"; \n "+
                                    tmp2 +" = STACK[(int)P] ;\n";
                            gen.Agregar_codigo(ValorArreglo);
                            // temporales de arreglo a declarar
                            String t1 = gen.crear_temporal();
                            String t2 = gen.crear_temporal();
                            String t3 = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo1 =
                                    "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+declarar.getId() +"\n"+
                                            t1 +"= "+entornoFun.ObtenerTamanioAsignar()+"+" +declarar.getPos() +"; \n"+
                                            "P = "+t1 +";\n"+
                                            "STACK[(int) P ] =  H ;\n"+
                                            t2 +" =  H ;\n"+
                                            "H = H +"+declarar.getSize1() +";\n"+
                                            "HEAP[(int)H ] = -2;\n" +
                                            "H = H + 1; \n "+
                                            indice +" = 0 ;\n";

                            gen.Agregar_codigo( arreglo1);
                            String conDoW =  inicio +":\n" +
                                    "if ( "+indice +" < "+ declarar.getSize1() +" ) goto "+continuar +";\n" +
                                    "goto  "+ finalizar +";\n"+
                                    continuar + ":\n";
                            gen.Agregar_codigo(conDoW);
                            // seccion de codigo a ejecutar
                            // obtener el valor del arreglo
                            conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                    t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                    "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                    indice +" = "+ indice +" + 1;  \n"+
                                    tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                            gen.Agregar_codigo(conDoW);
                            conDoW ="goto "+inicio +";\n"+
                                    finalizar +":\n";
                            gen.Agregar_codigo(conDoW);

                        }
                        else
                        {
                            // ciclo while para copiar lo datos del arreglo
                            String inicio = gen.crear_etiquetas();
                            String continuar = gen.crear_etiquetas();
                            String finalizar = gen.crear_etiquetas();
                            String indice = gen.crear_temporal();
                            // obteniendo temporales del  arreglo a copiar
                            String tmp1 = gen.crear_temporal();
                            String tmp2 = gen.crear_temporal();
                            String valor = gen.crear_temporal();
                            String ValorArreglo = tmp1 +" = "+ent.ObtenerTamanio(parametro.getId())+"+ "+parametro.getPos()+" ; \n"+
                                    " P = "+tmp1+"; \n "+
                                    tmp2 +" = STACK[(int)P] ;\n";
                            gen.Agregar_codigo(ValorArreglo);
                            // temporales de arreglo a declarar
                            String t1 = gen.crear_temporal();
                            String t2 = gen.crear_temporal();
                            String t3 = gen.crear_temporal();
                            // agregando arreglo
                            String arreglo1 =
                                    "// ----------- DECLARANDO ARREGLO  EN SUBRUTINA "+declarar.getId() +"\n"+
                                            t1 +"= "+entornoFun.ObtenerTamanioAsignar()+"+" +declarar.getPos() +"; \n"+
                                            "P = "+t1 +";\n"+
                                            "STACK[(int) P ] =  H ;\n"+
                                            t2 +" =  H ;\n"+
                                            "H = H +"+declarar.getSize1()*declarar.getSize2() +";\n"+
                                            "HEAP[(int)H ] = -2;\n" +
                                            "H = H + 1; \n "+
                                            indice +" = 0 ;\n";

                            gen.Agregar_codigo( arreglo1);
                            String conDoW =  inicio +":\n" +
                                    "if ( "+indice +" < "+ declarar.getSize1()*declarar.getSize2() +" ) goto "+continuar +";\n" +
                                    "goto  "+ finalizar +";\n"+
                                    continuar + ":\n";
                            gen.Agregar_codigo(conDoW);
                            // seccion de codigo a ejecutar
                            // obtener el valor del arreglo
                            conDoW =  valor +" = " +"HEAP[(int)" + tmp2 + "]; \n"+
                                    t3 + " = " + t2 + " + "+ indice +" ;\n"+
                                    "HEAP[(int)" + t3 + "] = "+valor+" ;  \n"+
                                    indice +" = "+ indice +" + 1;  \n"+
                                    tmp2 +" = "+ tmp2 +" + 1"+"; \n";
                            gen.Agregar_codigo(conDoW);
                            conDoW ="goto "+inicio +";\n"+
                                    finalizar +":\n";
                            gen.Agregar_codigo(conDoW);

                        }



                    }
                }
                String call = ctx.ID().getText().toLowerCase() +"(); \n";
                gen.Agregar_codigo(call);

                // retornar
                Symbol id = entornoFun.Buscar(idRetornar);
                if(id != null)
                {

                    String tmp1 = gen.crear_temporal();
                    String tmp2 = gen.crear_temporal();
                    gen.Agregar_codigo(tmp1 +"= "+entornoFun.ObtenerTamanio(idRetornar)+" +" +id.getPos() +"; \n");
                    gen.Agregar_codigo(tmp2 + "= STACK[(int)"+tmp1+"];\n" );
                    return new SimboloC3D(tmp2,true,id.getType(),"","",Dato.VARIABLE);
                }
                // buscar si es un objeto de tipo arreglos
                ArrayS arreglo = entornoFun.BuscarArrays(idRetornar);
                if(arreglo != null)
                {
                    return  arreglo;
                }
                return  null;
            }
            else
            {
                System.out.println("Error al llamar funcion: no encontrado "+ctx.ID().getText());
                return  null;
            }

        }
        catch (Exception e )
        {
            System.out.println("Error al ejecutar la funcion: "+ctx.ID().getText() +" "+ e);
            return  null;
        }

    }
}
