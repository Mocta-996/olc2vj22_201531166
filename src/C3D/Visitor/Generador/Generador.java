package C3D.Visitor.Generador;

import java.util.ArrayList;

public class Generador {
    public  int temporal;
    public  int etiqueta;
    public  ArrayList<String> codigo ;
    public  ArrayList <String > lista_temp;
    public  ArrayList <String> lista_etiqueta;
    public  ArrayList<String> codigoFucniones ;

    public Generador( ) {
        this.temporal = 0;
        this.etiqueta = 0;
        codigo =new ArrayList<>();
        lista_temp = new ArrayList<>();
        lista_etiqueta = new ArrayList<>();
        codigoFucniones = new ArrayList<>();
    }



    //METODO PARA CREAR TEMPORALES Y AGREGARLOS A LA LISTA
    public  String crear_temporal()
    {
        //SE CREA EL TEMPORAL
        String tempo = "T" + temporal;
        //SE AUMENTA EN UNO AL CONTADOR
        temporal ++;
        //SE VERIFICA SI EL TEMPORAL YA EXISTE
        //BANDERA PARA VERIFICAR QUE EXISTA EL TEMPORAL EN LA LISTA
        boolean bandera = false;
        //SE RECORRE LA LISTA DE TEMPORALES
        if(!lista_temp.contains(tempo)){
            lista_temp.add(tempo);
        }
        return tempo;
    }

    //METODO PARA CREAR ETIQUETAS
    public   String crear_etiquetas()
    {
        String label = "L"+ etiqueta;
        etiqueta++;
        lista_etiqueta.add(label);
        return   label;
    }
    //METODO PARA AGREGAR EXPRESIONES LOGICAS A LA LISTA CODIG7
    public    void  Agregar_codigo(String e) {
        codigo.add(e);
    }
    public    void  eliminar_etiqueta(String e)
    {
        lista_etiqueta.remove(e);
    }

    public  String Lista_temporales(){
        String ll ="";
        for (int i = 0; i < lista_temp.size(); i++)
            ll += lista_temp.get(i) + (i == (lista_temp.size()-1) ? "; \n" : ",");
        return ll;
    }

    // METODO PARA IMPRIMIR CADENAS
    public void  metodo_Print(Generador gen ){
        gen.Agregar_codigo("/* INICIO DE FUNCION PRINT*/");
        String puntero = gen.crear_temporal();
        String heap = gen.crear_temporal();
        String aux = gen.crear_temporal();
        String mientras = gen.crear_etiquetas();
        String retornar = gen.crear_etiquetas();

        String prt = "void print() { \n ";
        prt += puntero + "= P; \n ";
        prt += heap + "= STACK[(int)"+puntero+"]; \n ";
        prt += mientras + ": \n ";
        prt += aux +  " = HEAP[(int)" + heap + "];\n";
        prt += "if ( " + aux + " == -1) goto " + retornar + ";\n";
        prt += "printf(\"%c\", (int)" + aux + ");\n";
        prt += heap + " = " + heap + " + 1;\n";
        prt += "goto " + mientras + ";\n";
        prt += retornar + ":\n";
        prt += "return;\n}\n";
        gen.Agregar_codigo(prt);
        gen.Agregar_codigo("/* FINDE FUNCION  PRINT*/");

    }

    public    void  Agregar_codigoFunciones(String e) {
        codigoFucniones.add(e);
    }

}
