package C3D.Visitor.EntornoC3D;
import Entorno.Type;
import Entorno.Dato;
public class SimboloAux {

    private String nombre;
    private Object value;
    private Type type;
    private Dato dato;
    private  int apuntador;


    public SimboloAux(String nombre, Object value, Type type, Dato dato, int apuntador) {
        this.nombre = nombre;
        this.value = value;
        this.type = type;
        this.dato = dato;
        this.apuntador = apuntador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Dato getDato() {
        return dato;
    }

    public void setDato(Dato dato) {
        this.dato = dato;
    }

    public int getApuntador() {
        return apuntador;
    }

    public void setApuntador(int apuntador) {
        this.apuntador = apuntador;
    }
}
