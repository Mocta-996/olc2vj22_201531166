package C3D.Visitor.EntornoC3D;

import Entorno.Dato;
import Entorno.Type;

public class SimboloC3D {
    private  String valor;
    private  boolean esTemp;
    private Type  tipo;
    private String etiquetaVerdera;
    private String etiquetaFalse;
    private Dato nativo;

    public SimboloC3D(String valor, boolean esTemp, Type tipo, String etiquetaVerdera, String etiquetaFalse, Dato nativo) {
        this.valor = valor;
        this.esTemp = esTemp;
        this.tipo = tipo;
        this.etiquetaVerdera = etiquetaVerdera;
        this.etiquetaFalse = etiquetaFalse;
        this.nativo = nativo;
    }


    public Dato getNativo() {
        return nativo;
    }

    public void setNativo(Dato nativo) {
        this.nativo = nativo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public boolean isEsTemp() {
        return esTemp;
    }

    public void setEsTemp(boolean esTemp) {
        this.esTemp = esTemp;
    }

    public Type getTipo() {
        return tipo;
    }

    public void setTipo(Type tipo) {
        this.tipo = tipo;
    }

    public String getEtiquetaVerdera() {
        return etiquetaVerdera;
    }

    public void setEtiquetaVerdera(String etiquetaVerdera) {
        this.etiquetaVerdera = etiquetaVerdera;
    }

    public String getEtiquetaFalse() {
        return etiquetaFalse;
    }

    public void setEtiquetaFalse(String etiquetaFalse) {
        this.etiquetaFalse = etiquetaFalse;
    }
}
