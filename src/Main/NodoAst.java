package Main;

public class NodoAst
{
    public String numero ;
    public String nombre;
    public String cuerpo;

    public NodoAst(String numero, String nombre, String cuerpo) {
        this.numero = numero;
        this.nombre = nombre;
        this.cuerpo = cuerpo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}
