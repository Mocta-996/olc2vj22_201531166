package Main;

import Entorno.ArrayS;
import Entorno.Environment;
import Entorno.Symbol;

import java.util.HashMap;

public class Puntero {

    public String padre;
    public String nombre;
    public Puntero  siguiente;       // c3d

    public Puntero(String padre, String nombre) {
        this.padre = padre;
        this.nombre = nombre;
    }
}
