grammar JGrammar;
options { caseInsensitive = true; }
@header {
import java.util.ArrayList;
import java.util.List;
}
// Tokens
PUNTO:          '.';
PTCOMA:         ';';
PARIZQ:         '(';
PARDER:         ')';
LLAVEIZQ:       '{';
LLAVEDER:       '}';
COMA:           ',';
CORIZQ:         '[';
CORDER:         ']';
KLEENE:         '?';
IGUAL:          '=';
DOSPUNTOS:      ':';
EXCLA:          '!';

//  PALABRAS RESERVADAS
PROGRAM:        'program';
END:            'end';
IMPLICIT:       'implicit';
NONE:           'none';
PRINT:          'print';
TRUE:           '.true.';
FALSE:          '.false.';
DIMENSION:      'dimension';
SIZE:           'size';
ALLOCATABLE:    'allocatable';
ALLOCATE:       'allocate';
DEALLOCATE:     'deallocate';
IF :            'if';
THEN :          'then';
ELSE :          'else';
DO:             'do';
WHILE:          'while';
EXIT:           'exit';
CYCLE:          'cycle';

// -- tipos de datos
RINTEGER:       'integer';
RREAL:          'real';
RLOGICAL:       'logical';
RCOMPLEX:       'complex';
RCHARACTER:     'character';

// OPREACIONES LOGICAS
AND:            '.and.';
OR:             '.or.';
NOT:            '.not.';
// OPERACIONES RELACIONALES

IGUALACION : '.eq.' | '==';
DIFERENCIACION:  '/='|'.ne.';
MENORQUE:       '<'|'.lt.';
MAYORQUE:       '>'|'.gt.';
MENORIGUAL:     '<='|'.le.';
MAYORIGUAL:     '>='|'.ge.';

// OPERACIONES ARITMETICAS
POTENCIA:       '**';
MULTIPLICACION: '*';
DIVISION:       '/';
SUMA:           '+';
RESTA:          '-';
// PALABRAS RESERVADAS SUBRUTINAS
SUBRUTINE: 'subroutine';
INTENT :    'intent';
IN :        'in';
CALL:           'call';

// PALABRAS RESERVADAS FUNCIONES
FUNCTION:       'function';
RESULT:       'result';

// primitivos
INTEGER:        [0-9]+;
FLOAT:          ('0'..'9')+ ('.' ('0'..'9')+)?;
COMPLEX:         [0-9]+;
CHARACTER:        ["|'][a-zA-Z]["|'];
ID:             [a-zA-Z0-9_]+ ;
CADENA:         ["|']~["|']*["|'];

// ADICIONALES
WHITESPACE:             [ \\\r\n\t]+ -> skip;   //ESPACIOS EN BLANCO Y SALTOS DE LINEA
COMENTARIO_LINEA :      '!' ~[\r\n]* -> skip;  // comentario linea


fragment
ESC_SEQ
    :   '\\' ('\\'|'@'|'['|']'|'.'|'#'|'+'|'-'|'!'|':'|' ')
    ;



start           : bloqueFunciones bloqueFunciones2  PROGRAM init=ID IMPLICIT NONE linstrucciones  END PROGRAM end=ID EOF
                |   comentario bloqueFunciones bloqueFunciones2 PROGRAM init=ID  linstrucciones  END PROGRAM end=ID  comentario EOF
                ;
/*start           : listaMetodos  PROGRAM init=ID IMPLICIT NONE linstrucciones  END PROGRAM end=ID EOF
                |  listaMetodos  PROGRAM init=ID  linstrucciones  END PROGRAM end=ID  comentario EOF
                ;*/
// ============================= FUNCIONES =====================================
/*function vector_norm(n,vec) result(norm)
  implicit none
  integer, intent(in) :: n
  real, intent(in) :: vec(n)
  real :: norm

  norm = sqrt(sum(vec**2))

end function vector_norm*/
/*listaMetodos : bloqueFunciones
              | bloqueFunciones2
              | comentario
              ;*/
// -> listaExpr : hacer referencia a la produccion de asigancion de arreglos unidimensionales
inicioFuncion     : FUNCTION idf1 = ID PARIZQ  listaExpr PARDER RESULT PARIZQ retorno = expresion PARDER
                    IMPLICIT NONE declararParametros  linstrucciones END FUNCTION idf2 = ID ;



// -> listaExpr : hacer referencia a la produccion de asigancion de arreglos unidimensionales
inicioSubRutina     : SUBRUTINE  idsb1 = ID PARIZQ  listaExpr PARDER IMPLICIT NONE
                         declararParametros  linstrucciones END SUBRUTINE idsb2 = ID ;
declararParametros  : (parametros )*;

parametros : tipo COMA INTENT PARIZQ IN PARDER '::' var=ID
           |tipo COMA INTENT PARIZQ IN PARDER  '::'  var=ID PARIZQ listaExpr  PARDER
           ;

// llamadad de subrutinas
callSubrutina :CALL  id= ID PARIZQ listaExpr PARDER
        ;

bloqueFunciones : (inicioSubRutina)*
                ;

bloqueFunciones2 : (inicioFuncion)*
                ;
comentario      :   (COMENTARIO_LINEA )*
                ;
linstrucciones  :  (instrucciones)*
                ;


instrucciones   :   declaracion                 //#decl       // declaracion de variables
                |   COMENTARIO_LINEA            //#com        // comentarios
                |   print                       //#prnt       // funcion de imprimir
                |   asignacionvar               //#asig       // asignacion de variables
                |   arreglosestaticos           //#arest      // declaracion de arreglos estaticos
                |   asignarArrayUni             //#asarru     // asignacion de arraglos unidimensionales
                |   asignarArrayBid             //#asarrbid   // asignacion de arreglos bidimensionales
                |   arreglosDinamicos           //#arrdinamicos   // declaracion de arreglos dinamicos
                |   funAllocate                 //#funAllocateDi  // funcion para asignar dimenensiones de un arreglo
                |   funDeAllocate               //#funDeall       // funcion para desasignar dimensiones de un arreglo
                |   sentenciaIf                 //#senIf          // sentencia if
                |   cicloDo
                |   cicloDoWhile
                |   controlCiclos
                |   callSubrutina               //  llamada de subrutinas
                ;

// ================= DECLARACION DE VARIABLES ======================
declaracion     :   t= tipo '::' l=listaDeclarada
                ;
listaDeclarada  :   variables (COMA variables)*
                ;
variables       :  i= ID IGUAL  e=expresion
                |  i=ID
                ;

// ================= ASIGNACION DE VARIABLES ======================
asignacionvar   :  i=ID IGUAL l=expresion
                ;

// ================= IMPRIMIR ======================
print:PRINT MULTIPLICACION COMA  l=listaprint
    ;
listaprint:expresion (COMA expresion)*
    ;

//================ ARREGLOS ESTATICOS =================
/*
integer, dimension(10) :: array1, array(20,2)
*/
arreglosestaticos   :   t=tipo COMA DIMENSION PARIZQ d=dim PARDER  '::' l =idarrays
                    |   t=tipo '::' l =idarrays
                    ;
idarrays            :   listaArraSimple (COMA listaArraSimple)*
                    ;
listaArraSimple     :   i= ID PARIZQ d=dim PARDER
                    |   i=ID
                    ;
dim                 :   expresion (COMA expresion)*
                    ;

//================ ARREGLOS DINAMICOS =================
/*
 integer, allocatable :: array1(:), array2(:), array3(:,:)
 integer, allocatable :: array1(:,:)
*/
arreglosDinamicos   :   t=tipo COMA ALLOCATABLE '::' l =idArrayDin
                    ;
idArrayDin          :   listaArraBid (COMA listaArraBid)*
                    ;
listaArraBid        :   i= ID PARIZQ d=dimBid PARDER
                    ;
dimBid              :   DOSPUNTOS (COMA DOSPUNTOS)*
                    ;


// ================= ASIGNACION DE ARREGLOS UNIDIMENSIONALES ======================
/*
array = (/1.5, 3.2,4.5,0.9,7.2 /)
array[2] = 20.5
*/
asignarArrayUni     :   i =ID  IGUAL PARIZQ '/' l=listaExpr '/' PARDER
                    |   i= ID CORIZQ pos = expresion CORDER IGUAL val= expresion
                    ;
listaExpr           :   expresion* (COMA expresion)*
                    ;

/*listaExpr           :   expresion (COMA expresion)*
                    ;*/
// ================= ASIGNACION DE ARREGLOS BIDIMENSIONALES ======================
/*
array [1,2] = 10
*/
asignarArrayBid :i = ID CORIZQ d=dim CORDER IGUAL val = expresion
                ;

// ================= ASIGNACION DE TAMAÑO DE ARREGLOS DINAMICOS ======================
/*
allocate(array1(10)
allocate(array2(10,10))
allocate(array1(10),array2(10,10))
*/
funAllocate  :ALLOCATE PARIZQ l =idarrays PARDER
    ;
// ================= DESASIGNACION DE TAMAÑO DE ARREGLOS DINAMICOS ======================
/*
deallocate(array1)
deallocate(array1,array2)
*/
funDeAllocate  :DEALLOCATE PARIZQ l =listaDeallocate PARDER
    ;

listaDeallocate : ID (COMA ID)*
                ;

// ================= SENTENCIA IF  ======================
/*
    if (angle < 90.0) then
     print *, 'Angle is acute'
   else if (angle < 180.0) then
     print *, 'Angle is obtuse'
   else
     print *, 'Angle is reflex'
   end if
*/

sentenciaIf: IF  PARIZQ ep=expresion PARDER THEN  bp = linstrucciones  END IF
        | IF  PARIZQ ep = expresion PARDER THEN   bp = linstrucciones   ELSE   bfinal = linstrucciones  END IF
        | IF  PARIZQ ep = expresion PARDER THEN   bp = linstrucciones   bElse=bloqueElse  END IF
        | IF  PARIZQ ep = expresion PARDER THEN   bp = linstrucciones   bElse=bloqueElse  ELSE  bfinal=linstrucciones  END IF
        ;

bloqueElse : listaElse(listaElse)*
            ;
listaElse : ELSE IF PARIZQ ep=expresion PARDER THEN bp = linstrucciones
        ;

// ================= CICLO DO  ======================
/*
   do i = 1, 10, 2
     print *, i  ! Imprime lo números impares
   end do
outer_loop: do i = 1, 10
  inner_loop: do j = 1, 10
  end do inner_loop
end do outer_loop
*/

cicloDo : DO  var=ID IGUAL lc=listaCondiciones   bp = linstrucciones END DO
        |idDOi= ID DOSPUNTOS DO  var=ID IGUAL lc=listaCondiciones   bp = linstrucciones END DO idDOf = ID
    ;
listaCondiciones: expresion (COMA expresion)*
    ;
// ================= CICLO DO WHILE  ======================
/*
   do while (i < 11)
     print *, i
     i = i + 1
   end do
*/

cicloDoWhile : DO WHILE PARIZQ cond=expresion PARDER   bp = linstrucciones END DO
            |idDOi= ID DOSPUNTOS DO WHILE PARIZQ cond=expresion PARDER   bp = linstrucciones END DO idDOf = ID
    ;


// ================= DECLARACION DE TIPOS ======================
tipo    :RINTEGER
        | RREAL
        | RLOGICAL
        | RCOMPLEX
        | RCHARACTER
        ;

// bloque
// ============================== CONTROL DE CICLOS ======================
controlCiclos   : tk=EXIT         //#instrExit
                | tk=CYCLE
                | tk=CYCLE idcycle = ID
                | tk=EXIT  idexit = ID
                ;

// ================= IMPRIMIR ======================
expresion   : op = RESTA right = expresion                                  #opAritmetica
            | op = NOT  right = expresion                                   #opLogica
            | PARIZQ expresion PARDER                                       #parenExpr
            | left=expresion op=POTENCIA right=expresion                    #opAritmetica
            | left=expresion op=(MULTIPLICACION|DIVISION) right=expresion   #opAritmetica
            | left=expresion op=(SUMA|RESTA) right=expresion                #opAritmetica
            | left = expresion  op = ( MAYORIGUAL|MENORIGUAL|IGUALACION|DIFERENCIACION|MAYORQUE|MENORQUE)  right = expresion   #opRelacional
            | left = expresion  op = (AND |OR)  right = expresion           #opLogica
            | INTEGER                                                       #integer
            | FLOAT                                                         #float
            | COMPLEX                                                       #complex
            | CHARACTER                                                     #char
            | TRUE                                                          #logico
            | FALSE                                                         #logico
            | ID                                                            #varExp                 // acceder al valor de una variable
            | CADENA                                                        #cad                    // impresion de una cadena
            | SIZE PARIZQ i=ID PARDER                                       #opSize                 // funciono nativa size()
            | ID CORIZQ pos=expresion CORDER                                #arrUniExp              // accder a la posicion de un arreglos unidimensional
            | ID CORIZQ pos=dim CORDER                                      #arrBiExp              // accder a la posicion de un arreglos unidimensional
            | ID PARIZQ listaExpr PARDER                                    #callFunction           // llamada de una funcion
            ;




