package Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import Entorno.Environment;
import Entorno.RepSimbolos;
import Error.Errores;
import grammar.JGrammarParser;
import org.antlr.v4.gui.TreeViewer;

public class Global {
    public static String consola = "";
    public static List<Errores> _errores = new ArrayList<Errores>();
    public static List<Errores> _erroresLexicos = new ArrayList<Errores>();
    public static List<Errores> _erroresSintacticos = new ArrayList<Errores>();
    public static List<RepSimbolos> _listaSimbolos = new ArrayList<>();
    public static TreeViewer cst ;
    public static String codigoGraphviz = "";
    public  static  JGrammarParser.StartContext c3d;
    public  static Stack <Environment> stackC3d;
    public  static  Puntero punteroGlobal;
}
