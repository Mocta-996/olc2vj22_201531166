package Main;

import C3D.Visitor.EntornoC3D.GlobalC3D;
import C3D.Visitor.Generador.Generador;
import C3D.Visitor.VisitorC3D;
import Entorno.Environment;
import grammar.*;
import Error.Errores;
import Interfaz.*;


import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;

import static org.antlr.v4.runtime.CharStreams.fromString;
import Main.Visitor;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Main {
    static ArrayList<String> LineNum = new ArrayList<String>();
    static ArrayList<String> Type = new ArrayList<String>();
    static ArrayList<String> Content = new ArrayList<String>();
    static ArrayList <NodoAst> nodoAst = new ArrayList<>();
    static ArrayList<String> producciones = new ArrayList<String>();

    public static void main(String[] args)
    {

       /*String input ="";
        CharStream cs = fromString(input);

        JGrammarLexer lexico = new JGrammarLexer(cs);
        // agregar errores
        lexico.removeErrorListeners();
        lexico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                Errores.AgregarErrorLexico(msg,offendingSymbol.toString(), String.valueOf(line),String.valueOf(charPositionInLine));
            }
        });
        CommonTokenStream tokens = new CommonTokenStream(lexico);

        JGrammarParser parser = new JGrammarParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                Errores.AgregarErrorSintactico(msg,offendingSymbol.toString(), String.valueOf(line),String.valueOf(charPositionInLine));
            }
        });

        JGrammarParser.StartContext startCtx = parser.start();
        Environment inicio = new Environment(null, "global");
        Visitor visitor = new Visitor(inicio);
        visitor.visit(startCtx);
        System.out.println(Global.consola);

        // pasar como parametros global: visitor , startCtx
       /* Global.c3d = startCtx;
        Global.stackC3d = visitor.stackEnvC3D;
        System.out.println(Global.consola);
        */


        /*for(Environment ss : visitor.stackEnvC3D){
            System.out.println(ss.nombre);
        }*/
        // SEGUNDA PASADO CODIGO 3 DIRECCIONES
       // VisitorC3D cod3d = new VisitorC3D(inicio);
        /*VisitorC3D cod3d = new VisitorC3D(visitor.stackEnvC3D);
        cod3d.visit(startCtx);
        //imprimir cabecera
        String reporte = "#include <stdio.h>\n" +
                "double STACK[30101999];\n" +
                "double HEAP[30101999];\n" +
                "double P;\n" +
                "double H;\n";

        // imprimir temporales
        if(!cod3d.gen.Lista_temporales().equals("")){
            reporte += "double  "+ cod3d.gen.Lista_temporales();
        }
        for (String ln : cod3d.gen.codigo)
            reporte += ln;

        // escribiendo salida
        FileWriter fichero = null;
        PrintWriter escritor;
        try
        {
            fichero = new FileWriter("C:\\Users\\Pilo Tuy\\Desktop\\Compiladores 2\\reportes ast\\c3d.c");
            escritor = new PrintWriter(fichero);
            escritor.print(reporte);
            //escritor.print("digraph { a -> b }");
        }
        catch (Exception e)
        {
            System.err.println("Error al escribir el archivo ");
        }
        finally
        {
            try
            {
                if (null != fichero)
                    fichero.close();
            }
            catch (Exception e2)
            {
                System.err.println("Error al cerrar el archivo ");
            }
        }

        //generateAST(startCtx, false, 0);
        //printDOT();


        //System.out.println(Global.astActual);

        /*for(int i=0; i<_errores.size(); i++){
            System.out.println( _errores.get(i).getDescripcion() +", " +_errores.get(i).getAmbito() );

        }*/
        Interfaz iniciar = new Interfaz();
        iniciar.setVisible(true);
    }
   /* private static void generateAST(RuleContext ctx, boolean verbose, int indentation) {
        boolean toBeIgnored = !verbose && ctx.getChildCount() == 1 && ctx.getChild(0) instanceof ParserRuleContext;

        if (!toBeIgnored) {
            String ruleName = JGrammarParser.ruleNames[ctx.getRuleIndex()];
            LineNum.add(Integer.toString(indentation));
            Type.add(ruleName);
            Content.add(ctx.getText());
        }
        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree element = ctx.getChild(i);
            if (element instanceof RuleContext) {
                generateAST((RuleContext) element, verbose, indentation + (toBeIgnored ? 0 : 1));
            }
        }
    }

    private static void printDOT(){
        printLabel();
        int pos = 0;
        for(int i=1; i<LineNum.size();i++){
            pos=getPos(Integer.parseInt(LineNum.get(i))-1, i);
            System.out.println((Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i);
            producciones.add((Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i);
            Global.codigoGraphviz += (Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"->"+LineNum.get(i)+i;
        }
    }

    private static void printLabel(){
        for(int i =0; i<LineNum.size(); i++){
            System.out.println(LineNum.get(i)+i+"[label=\""+Type.get(i)+"\n "+Content.get(i)+"\"]");
            Global.codigoGraphviz += LineNum.get(i)+i+"[label=\""+Type.get(i)+"\n "+Content.get(i)+"\"]";
            nodoAst.add(new NodoAst(LineNum.get(i)+i,Type.get(i),Content.get(i)));
        }
    }

    private static int getPos(int n, int limit){
        int pos = 0;
        for(int i=0; i<limit;i++){
            if(Integer.parseInt(LineNum.get(i))==n){
                pos = i;
            }
        }
        return pos;
    }

    public void filtro (){
        // verifivar los de tipo expresion:
        //si son operaciones aritmeticas
        for(NodoAst n: nodoAst){
            // verificar que sea de tipo expresion
        }

    }*/
}
