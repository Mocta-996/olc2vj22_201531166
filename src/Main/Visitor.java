package Main;

import C3D.Visitor.Generador.Generador;
import Entorno.*;
import Funciones.Funciones;
import Subrutinas.Subrutina;
import grammar.*;

import Error.Errores;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.ArrayList;
import java.util.Stack;

public class Visitor extends JGrammarBaseVisitor<Object> {
    Stack<Environment> stackEnv = new Stack<Environment>();
    Environment entGlobal ;
    public Stack<Environment> stackEnvC3D = new Stack<Environment>();
    ArrayList<String>  nomEntorno = new ArrayList<>();
    static  String codigo ="";

    public Visitor(Environment ent)
    {
        this.stackEnv.push(ent);
        entGlobal = ent;
        this.stackEnvC3D.push(ent);
        nomEntorno.add(ent.nombre);
        codigo = GeneradorId.getPassword();
    }
    public Object visitStart(JGrammarParser.StartContext ctx)
    {
        try
        {
            if(ctx.bloqueFunciones().inicioSubRutina() != null)
                visitBloqueFunciones(ctx.bloqueFunciones());
            if(ctx.bloqueFunciones2().inicioFuncion() != null)
                visitBloqueFunciones2(ctx.bloqueFunciones2());

            if(ctx.init.getText().equals(ctx.end.getText())){
                visitLinstrucciones(ctx.linstrucciones());
                return  true;
            }
            else
            {
                Errores.AgregarError("Errror de declaracion de Main","global",String.valueOf(ctx.end.getLine()),String.valueOf(ctx.end.getCharPositionInLine()));
                return null;
            }

        }
        catch (Exception e)
        {
            Errores.AgregarError("Errror en codigo de entrada","global",String.valueOf(ctx.init.getLine()),String.valueOf(ctx.init.getCharPositionInLine()));
            return null;

        }
    }

    public Object visitBloqueFunciones(JGrammarParser.BloqueFuncionesContext ctx)
    {

        for (JGrammarParser.InicioSubRutinaContext ictx : ctx.inicioSubRutina())
        {
            Object s= visitInicioSubRutina(ictx);
            if(s instanceof  Exit || s instanceof  Cycle)
            {
                return  s;
            }
        }

        return true;
    }
    public Object visitBloqueFunciones2(JGrammarParser.BloqueFunciones2Context ctx)
    {

        for (JGrammarParser.InicioFuncionContext ictx : ctx.inicioFuncion())
        {
            Object s= visitInicioFuncion(ictx);
            if(s instanceof  Exit || s instanceof  Cycle)
            {
                return  s;
            }
        }

        return true;
    }
    public Object visitLinstrucciones(JGrammarParser.LinstruccionesContext ctx)
    {
        for (JGrammarParser.InstruccionesContext ictx : ctx.instrucciones())
        {
            
            Object s= visitInstrucciones(ictx);
            if(s != null)
            {
            }

            if(s instanceof  Exit || s instanceof  Cycle)
            {
                return  s;
            }
            if(stackEnv.peek().equals(entGlobal))
            {
                if(!stackEnvC3D.peek().equals(entGlobal))
                {
                    //stackEnvC3D.peek().siguiente = stackEnv.peek();
                    //stackEnvC3D.push(stackEnv.peek());
                    codigo = GeneradorId.getPassword();
                }
            }
        }
        return true;
    }

    public Object visitInstrucciones(JGrammarParser.InstruccionesContext ctx)
    {

        if (ctx.declaracion() != null)
            visitDeclaracion(ctx.declaracion());
        else if (ctx.print() != null)
            visitPrint(ctx.print());
        else if (ctx.sentenciaIf() != null){
            Object ctrl = visitSentenciaIf(ctx.sentenciaIf());
            if(ctrl instanceof  Exit || ctrl instanceof  Cycle){
                return ctrl;
            }
        }
        else if (ctx.asignacionvar() != null)
            visitAsignacionvar(ctx.asignacionvar());
        else if (ctx.arreglosestaticos() != null)
            visitArreglosestaticos(ctx.arreglosestaticos());
        else if (ctx.asignarArrayUni() != null)
            visitAsignarArrayUni(ctx.asignarArrayUni());
        else if (ctx.asignarArrayBid () != null)
            visitAsignarArrayBid (ctx.asignarArrayBid ());
        else if (ctx.arreglosDinamicos() != null)
            visitArreglosDinamicos(ctx.arreglosDinamicos());
        else if (ctx.funAllocate () != null)
            visitFunAllocate (ctx.funAllocate ());
        else if (ctx.funDeAllocate () != null)
            visitFunDeAllocate  (ctx.funDeAllocate  ());
        else if (ctx.sentenciaIf () != null)
            visitSentenciaIf  (ctx.sentenciaIf());
        else if (ctx.cicloDo () != null)
            visitCicloDo  (ctx.cicloDo());
        else if (ctx.cicloDoWhile () != null)
            visitCicloDoWhile  (ctx.cicloDoWhile());
        else if (ctx.controlCiclos () != null)
            return  visitControlCiclos (ctx.controlCiclos());
        else if (ctx.callSubrutina () != null)
            return  visitCallSubrutina (ctx.callSubrutina());

        return true;
    }

    /**
     *  ALMACENAR SUBRUTINA
     * **/
    public Object visitInicioSubRutina(JGrammarParser.InicioSubRutinaContext ctx)
    {
        Environment ent  = stackEnv.peek();
        try
        {
            // verificar el nombre de inicio y el nombre final de la subrutina
            if(ctx.idsb1.getText().toLowerCase().equals(ctx.idsb2.getText().toLowerCase()))
            {
                // verificar si no existe en el entorno actual
                if(!stackEnv.peek().TablaSimbolo.containsKey((ctx.idsb1.getText() + Type.SUBRUTINA.name()).toUpperCase()))
                {
                    // lista de  parametros de la subrutina
                    ArrayList<Symbol> parametros = new ArrayList<Symbol>();
                    if(ctx.listaExpr().expresion() !=null)
                    {
                        for(JGrammarParser.ExpresionContext v: ctx.listaExpr().expresion())
                            parametros.add(new Symbol(0,0,v.getText(),null,Type.NULL,Dato.PARAMETRO,-1));
                    }

                    Subrutina subr = new Subrutina(ctx.idsb1.getText(), parametros, ctx.linstrucciones(), ctx.declararParametros(),null);

                    // se agrega un nuevo simbolo de tipo subrutina

                    stackEnv.peek().nuevoSimbolo(ctx.idsb1.getText() + Type.SUBRUTINA.name(),
                            new Symbol(ctx.idsb1.getLine(),ctx.idsb1.getCharPositionInLine(),
                                    ctx.idsb1.getText(),subr,Type.SUBRUTINA,Dato.SUBRUTINA,-1 ));

                    RepSimbolos.AgregarTSimbolo("Subrutina",ctx.idsb1.getText(),String.valueOf(ctx.idsb1.getLine()),
                            String.valueOf(ctx.idsb1.getCharPositionInLine()),"---",ent.nombre);

                    return true;
                }
                else
                {
                    Errores.AgregarError("Error al ejecutar la subrutina, ya existe: "+ctx.idsb1.getText(),ent.nombre,String.valueOf(ctx.idsb1.getLine()),String.valueOf(ctx.idsb1.getCharPositionInLine()));

                }

            }
            else{
                Errores.AgregarError("Error al ejecutar la subrutina,  no se reconoce nombre: "+ctx.idsb1.getText(),ent.nombre,String.valueOf(ctx.idsb1.getLine()),String.valueOf(ctx.idsb1.getCharPositionInLine()));
            }
            return true;
        }catch (Exception e ){
            System.out.println(e);
            Errores.AgregarError("Error al ejecutar la subrutina: "+ctx.idsb1.getText(),ent.nombre,String.valueOf(ctx.idsb1.getLine()),String.valueOf(ctx.idsb1.getCharPositionInLine()));
            return  null;
        }

    }

    /**
     * ALMACENAR  FUNCIONES
     * **/
    public Object visitInicioFuncion(JGrammarParser.InicioFuncionContext ctx)
    {
        Environment ent  = stackEnv.peek();
        try
        {
            // verificar el nombre de inicio y el nombre final de la funcion
            if(ctx.idf1.getText().toLowerCase().equals(ctx.idf2.getText().toLowerCase()))
            {
                // verificar si no existe en el entorno actual
                if(!stackEnv.peek().TablaSimbolo.containsKey((ctx.idf1.getText() + Type.FUNCION.name()).toUpperCase()))
                {
                    // lista de  parametros de la funcion
                    ArrayList<Symbol> parametros = new ArrayList<Symbol>();
                    if(ctx.listaExpr().expresion() !=null)
                    {
                        for(JGrammarParser.ExpresionContext v: ctx.listaExpr().expresion())
                            parametros.add(new Symbol(0,0,v.getText(),null,Type.NULL,Dato.PARAMETRO,-1));
                    }

                    if(ctx.retorno != null)
                    {
                        parametros.add(new Symbol(0,0,ctx.retorno.getText(),null,Type.NULL,Dato.RETORNO,-1));
                    }

                    Funciones func = new Funciones(ctx.idf1.getText(), parametros, ctx.linstrucciones(), ctx.declararParametros(),null);
                    // se agrega un nuevo simbolo de tipo funcion
                    /*int posicion = ent.ul_pos;
                    ent.ul_pos ++;*/
                    stackEnv.peek().nuevoSimbolo(ctx.idf1.getText() + Type.FUNCION.name(),
                            new Symbol(ctx.idf1.getLine(),ctx.idf1.getCharPositionInLine(),
                                    ctx.idf1.getText(),func,Type.FUNCION,Dato.FUNCION,-1));

                    RepSimbolos.AgregarTSimbolo("Funcion",ctx.idf1.getText(),String.valueOf(ctx.idf1.getLine()),
                            String.valueOf(ctx.idf1.getCharPositionInLine()),"---",ent.nombre);
                    return true;

                }
                else
                {
                    Errores.AgregarError("Error al ejecutar la funcion, ya existe una funcion declarada: "+ctx.idf1.getText(),ent.nombre,String.valueOf(ctx.idf1.getLine()),String.valueOf(ctx.idf1.getCharPositionInLine()));

                }
            }
            else
            {
                Errores.AgregarError("Error al ejecutar la funcion,  no se reconoce nombre: "+ctx.idf1.getText(),ent.nombre,String.valueOf(ctx.idf1.getLine()),String.valueOf(ctx.idf1.getCharPositionInLine()));
            }
            return true;
        }
        catch (Exception e )
        {
            System.out.println(e);
            Errores.AgregarError("Error al ejecutar la funcion: "+ctx.idf1.getText(),ent.nombre,String.valueOf(ctx.idf1.getLine()),String.valueOf(ctx.idf1.getCharPositionInLine()));
            return  null;
        }

    }

    /**
     *  LLAMADA DE SUBRUTINA
     * **/
    public Object visitCallSubrutina(JGrammarParser.CallSubrutinaContext ctx)
    {
        Environment ent  = stackEnv.peek();
        try
        {
            // buscar la subrutina
            Symbol SubRutina = ent.Buscar(ctx.id.getText() +Type.SUBRUTINA.name());
            if(SubRutina != null)
            {
                Environment envSub = new Environment(ent,"SB_"+ctx.id.getText()+codigo);
                Subrutina sbejecutar = (Subrutina) SubRutina.getValue();
                // verificar los parametros
                // parametros -> contiene los ids de los parametros en la declaracion
                // USAR ENTORNO FUNCION
                // agregar el puntero
                //ent.siguiente = envSub;
                stackEnv.push(envSub);
                // agregar el entono del a subrutina como paramentro
                sbejecutar.setEntorno(envSub);
                if(!nomEntorno.contains("SB_"+ctx.id.getText()+codigo) )
                {
                    nomEntorno.add("SB_"+ctx.id.getText()+codigo);
                    stackEnvC3D.push(envSub);
                }

                if(sbejecutar.parametros.size() == ctx.listaExpr().expresion().size() &&
                sbejecutar.parametros.size() == sbejecutar.lista_parametros.getChildCount())
                {

                    // asignar parametros
                    for(int i = 0; i<ctx.listaExpr().expresion().size();i++)
                    {

                        Symbol paramEnviado = sbejecutar.parametros.get(i); // parametro enviad
                        // parametro a declarar
                        JGrammarParser.ParametrosContext auxnuevo = (JGrammarParser.ParametrosContext) sbejecutar.lista_parametros.getChild(i);
                        String nuevo_nombre =  auxnuevo.var.getText();
                        Type nuevo_tipo  = visitType(auxnuevo.tipo());
                        // verificar si no es un arreglo
                        if(auxnuevo.listaExpr() != null)
                        {
                            // verificar si es el mismo nombre
                            if(paramEnviado.getId().toLowerCase().equals(nuevo_nombre.toLowerCase()))
                            {
                                // verificar que el tipo sea el mismo declarado y enviado
                                Object arrayAux = visit(ctx.listaExpr().expresion().get(i));
                                if(arrayAux instanceof  ArrayS  )
                                {
                                    ArrayS paramAux = (ArrayS) arrayAux;
                                    if(nuevo_tipo == paramAux.getTypeData())
                                    {
                                        // verificar si es de dos  o una dimensiones
                                        int filas = -1;
                                        int columnas = -1;
                                        int dimension=0 ;
                                        for(JGrammarParser.ExpresionContext dim : auxnuevo.listaExpr().expresion())
                                        {
                                            dimension = dimension +1;
                                            filas = (filas == -1)? (int)((Symbol)visit(dim)).getValue():filas;
                                            columnas = (filas != -1)? (int)((Symbol)visit(dim)).getValue():-1;
                                        }
                                        if(dimension == paramAux.getDim() && dimension == 1)
                                        {
                                            // comprobar el tamaño
                                            if(filas <=paramAux.getSize1() )
                                            {
                                                ArrayList<Symbol> newArray2 = new ArrayList<>();
                                                ArrayList<Symbol> aux = (ArrayList<Symbol>) paramAux.getValue();
                                                 for(int k=0;k<filas;k++)
                                                 {
                                                     newArray2.add(aux.get(k));
                                                 }
                                                 // agregar posicion
                                                int posicion = envSub.ul_pos;
                                                envSub.ul_pos ++;
                                                ArrayS nuevo = new ArrayS(auxnuevo.ID().getSymbol().getLine(),auxnuevo.ID().getSymbol().getCharPositionInLine()
                                                ,paramEnviado.getId(),newArray2,paramAux.getType(),paramAux.getTypeData(),filas,0,1,false,false,posicion);
                                                envSub.nuevoArrayS(paramEnviado.getId(),nuevo);

                                                RepSimbolos.AgregarTSimbolo("Array",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                                        ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                                        nuevo_tipo.name(),envSub.nombre);

                                            }
                                            else
                                            {
                                                Errores.AgregarError("Error al asignar arreglo, indice fuera de rango: "+ auxnuevo.ID().getText(),
                                                        ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));

                                            }
                                        }// dimension  2
                                        else if(dimension == paramAux.getDim() && dimension == 2)
                                        {
                                            // comprobar el tamaño
                                            if(filas <=paramAux.getSize1() && columnas <= paramAux.getSize2() )
                                            {
                                                ArrayList<ArrayList<Symbol>> auxiliar = (ArrayList<ArrayList<Symbol>>) paramAux.getValue();

                                                ArrayList<ArrayList<Symbol>> newArray = new ArrayList<>();
                                                for(int k=0;k<filas;k++)
                                                {
                                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                                    for(int c = 0; c<columnas;c++)
                                                    {
                                                        newArray2.add(auxiliar.get(k).get(c));
                                                    }
                                                    newArray.add(newArray2);
                                                }
                                                // agregar posicon
                                                int posicion = envSub.ul_pos;
                                                envSub.ul_pos ++;
                                                ArrayS nuevo = new ArrayS(auxnuevo.ID().getSymbol().getLine(),auxnuevo.ID().getSymbol().getCharPositionInLine()
                                                        ,paramEnviado.getId(),newArray,paramAux.getType(),paramAux.getTypeData(),filas,columnas,2,false,false,posicion);
                                                envSub.nuevoArrayS(paramEnviado.getId(),nuevo);

                                                RepSimbolos.AgregarTSimbolo("Array",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                                        ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                                        nuevo_tipo.name(),envSub.nombre);

                                            }
                                            else
                                            {
                                                Errores.AgregarError("Error al asignar arreglo, indice fuera de rango: "+ auxnuevo.ID().getText(),
                                                        ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));

                                            }

                                        }
                                        else
                                        {
                                            Errores.AgregarError("Error,  no coinciden las dimensiones del arreglo: "+ auxnuevo.ID().getText(),
                                                    ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                        }

                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error, Tipos de datos no coinciden: "+ auxnuevo.tipo().getText(),ent.nombre,String.valueOf(auxnuevo.tipo().getStart().getLine()),String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()));
                                    }

                                }
                                else
                                {
                                    Errores.AgregarError("Error, Parametro no es  un arreglo: "+ auxnuevo.ID().getText(),
                                            ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                }


                            }
                            else
                            {
                                Errores.AgregarError("Error, nombre de parametros no coinciden: "+ auxnuevo.ID().getText(),ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                            }


                        }//  simbolo
                        else
                        {
                            // verificar si es el mismo nombre
                            if(paramEnviado.getId().toLowerCase().equals(nuevo_nombre.toLowerCase()))
                            {
                                // verificar que el tipo sea el mismo declarado y enviado
                                Symbol paramAux = (Symbol) visit(ctx.listaExpr().expresion().get(i));
                                if(nuevo_tipo == paramAux.getType())
                                {
                                    paramEnviado.setValue(paramAux.getValue());
                                    paramEnviado.setType(paramAux.getType());
                                    paramEnviado.setLine(auxnuevo.var.getLine());
                                    paramEnviado.setColumn(auxnuevo.var.getCharPositionInLine());
                                    paramEnviado.setDato(Dato.PRIMITIVO);
                                    int posicion = envSub.ul_pos;
                                    envSub.ul_pos ++;
                                    paramEnviado.setPos(posicion);
                                    envSub.nuevoSimbolo(paramEnviado.getId(),paramEnviado);
                                    RepSimbolos.AgregarTSimbolo("Primitivo",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                            ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                            nuevo_tipo.name(),envSub.nombre);
                                }
                                else
                                {
                                    Errores.AgregarError("Error, Tipos no coinciden: "+ auxnuevo.tipo().getText(),ent.nombre,String.valueOf(auxnuevo.tipo().getStart().getLine()),String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()));
                                }

                            }
                            else
                            {
                                Errores.AgregarError("Error, nombre de parametros no coinciden: "+ auxnuevo.ID().getText(),ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                            }
                        }

                    }

                    visitLinstrucciones((JGrammarParser.LinstruccionesContext) sbejecutar.lista_instrucciones);
                    stackEnv.pop();
                }

                return true;
            }
            else
            {
                Errores.AgregarError("Error, sub rutina no encontrado: "+ctx.id.getText(),ent.nombre,String.valueOf(ctx.id.getLine()),String.valueOf(ctx.id.getCharPositionInLine()));
                return  null;
            }
        }
        catch (Exception e )
        {
            System.out.println(e);
            Errores.AgregarError("Error al ejecutar la subrutina: "+ctx.id.getText(),ent.nombre,String.valueOf(ctx.id.getLine()),String.valueOf(ctx.id.getCharPositionInLine()));
            return  null;
        }

    }
    /**
     *  CONTROL DE CICLOS
     * **/
    public Object visitControlCiclos(JGrammarParser.ControlCiclosContext ctx)
    {
        Environment ent = stackEnv.peek();
        try
        {
            String valor = ctx.tk.getText();
            if(valor.toLowerCase().equals("exit"))
            {
                return new Exit(ctx.tk.getLine(),ctx.tk.getCharPositionInLine(),"exit",true);
            }
            else
            {
                if(ctx.idcycle != null)
                {
                    return new Cycle(ctx.tk.getLine(), ctx.tk.getCharPositionInLine(), ctx.idcycle.getText());
                }
                else
                {
                    return new Cycle(ctx.tk.getLine(), ctx.tk.getCharPositionInLine(), "");
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error en Ciclo Do",ent.nombre,String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
            return  null;
        }

    }

    /**
     * CICLO DO WHILE
     **/
    public Object visitCicloDoWhile(JGrammarParser.CicloDoWhileContext ctx)
    {
        Environment ent = stackEnv.peek();

        // obtener la etiqueta
        String idDoWI = "";
        String idDoWF = "";
        if(ctx.idDOi != null)
        {
            idDoWI = ctx.idDOi.getText();
        }
        if(ctx.idDOf != null)
        {
            idDoWF = ctx.idDOi.getText();
        }
        try
        {
            // do sin etiqueta
            if(idDoWI.equals("") && idDoWF.equals("") )
            {
                //obtener la expresion
                Symbol cond = (Symbol) visit(ctx.cond);
                if(cond !=  null)
                {
                    // verificar condicion
                    boolean condicion = false;
                    condicion =(boolean) cond.getValue();
                    int _break = 0;
                    Environment nuevoEnt = new Environment(ent,"CICLO_DOWHILE"+ent.nombre+codigo);
                    //ent.siguiente = nuevoEnt;
                    stackEnv.push(nuevoEnt);
                    if(!nomEntorno.contains("CICLO_DOWHILE"+ent.nombre+codigo) )
                    {
                        nomEntorno.add("CICLO_DOWHILE"+ent.nombre+codigo);
                        stackEnvC3D.push(nuevoEnt);
                    }
                    while (condicion)
                    {
                        Object salida = visitLinstrucciones(ctx.bp);
                        if(salida instanceof  Exit)
                        {
                            break;
                        }
                        if(salida instanceof Cycle)
                        {
                            cond = (Symbol) visit(ctx.cond);
                            condicion =(boolean) cond.getValue();
                            _break ++;
                            if(_break == 1000)
                            {
                                System.out.println(" Tiempo de espera agotado ");
                                Errores.AgregarError("Error en  Ciclo Do While, Limite de tiempo de  ciclo superado",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                                break;
                            }
                            continue;
                        }
                        cond = (Symbol) visit(ctx.cond);
                        condicion =(boolean) cond.getValue();
                        _break ++;
                        if(_break == 1000)
                        {
                            System.out.println(" limite de ciclo superado");
                            Errores.AgregarError("Error en  Ciclo Do While, Limite de ciclo superado",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                            break;
                        }
                    }
                    stackEnv.pop();
                }
                else
                {
                    Errores.AgregarError("Error en  Ciclo Do While, condicional no valido",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                    return  null;
                }
            }
            else
            {
                if(idDoWI.toLowerCase().equals(idDoWF.toLowerCase() ))
                {
                    //obtener la expresion
                    Symbol cond = (Symbol) visit(ctx.cond);
                    if(cond !=  null)
                    {
                        // verificar condicion
                        boolean condicion = false;
                        condicion =(boolean) cond.getValue();
                        int _break = 0;
                        Environment nuevoEnt = new Environment(ent,"CICLO_DOWHILE"+idDoWI+codigo);
                        //ent.siguiente = nuevoEnt;
                        stackEnv.push(nuevoEnt);
                        if(!nomEntorno.contains("CICLO_DOWHILE"+idDoWI+codigo) )
                        {
                            nomEntorno.add("CICLO_DOWHILE"+idDoWI+codigo);
                            stackEnvC3D.push(nuevoEnt);
                        }

                        while (condicion)
                        {
                            Object salida = visitLinstrucciones(ctx.bp);
                            if(salida instanceof  Exit)
                            {
                                break;
                            }
                            if(salida instanceof Cycle)
                            {
                                cond = (Symbol) visit(ctx.cond);
                                condicion =(boolean) cond.getValue();
                                _break ++;
                                if(_break == 1000)
                                {
                                    System.out.println(" Tiempo de espera agotado ");
                                    Errores.AgregarError("Error en  Ciclo Do While, Limite de tiempo de  ciclo superado",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                                    break;
                                }
                                continue;
                            }
                            cond = (Symbol) visit(ctx.cond);
                            condicion =(boolean) cond.getValue();
                            _break ++;
                            if(_break == 1000)
                            {
                                System.out.println(" limite de ciclo superado");
                                Errores.AgregarError("Error en  Ciclo Do While, Limite de ciclo superado",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                                break;
                            }
                        }
                        stackEnv.pop();
                    }
                    else
                    {
                        Errores.AgregarError("Error en  Ciclo Do While, condicional no valido",ent.nombre,String.valueOf(ctx.cond.getStart().getLine()),String.valueOf(ctx.cond.getStart().getCharPositionInLine()));
                        return  null;
                    }
                }
                else
                {
                    Errores.AgregarError("Error en Ciclo Do: Error en declaracion de etiqueta",ent.nombre,String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error en Ciclo Do",ent.nombre,String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     * CICLO DO
     **/
    public Object visitCicloDo(JGrammarParser.CicloDoContext ctx)
    {
        Environment ent = stackEnv.peek();
        // array de condiciones
        ArrayList<Integer> cond = new ArrayList<>();
        // obtener la etiqueta
        String idDoI = "";
        String idDoF = "";
        if(ctx.idDOi != null)
        {
            idDoI = ctx.idDOi.getText();
        }
        if(ctx.idDOf != null)
        {
            idDoF = ctx.idDOi.getText();
        }
        try
        {
            // do sin etiqueta
            if(idDoI.equals("") && idDoF.equals(""))
            {
                //obtener el simbolo del indice
                Symbol id = ent.Buscar(ctx.var.getText());
                if(id !=  null){
                    // obtener inicio, fin y salto
                    for(ParseTree c :ctx.lc.children)
                    {
                        if(!(c instanceof TerminalNodeImpl))
                        {
                            Symbol auxCond = (Symbol) visit(c);
                            if(auxCond.getType()== Type.INTEGER)
                            {
                                cond.add((Integer) auxCond.getValue());
                            }
                            else
                            {
                                Errores.AgregarError("Condicional no valido ",ent.nombre,String.valueOf(auxCond.getLine()),String.valueOf(auxCond.getColumn()));
                                return  null;
                            }
                        }
                    }
                    // verificar el tamaño de ciclo
                    if(cond.size() == 2 || cond.size() == 3)
                    {
                        int contador = cond.get(0);
                        int cont_final = cond.get(1);
                        int salto = (cond.size() == 3)? cond.get(2): 1;
                        int _break = 0;
                        // actualizar el simbolo
                        id.setValue(contador);
                        ent.AsignarVar(ctx.var.getText(),id);
                        //Symbol id = ent.Buscar(ctx.var.getText());
                        Environment nuevoEnt = new Environment(ent,"CICLO_DO"+ent.nombre+codigo);
                        //ent.siguiente = nuevoEnt;
                        stackEnv.push(nuevoEnt);
                        if(!nomEntorno.contains("CICLO_DO"+ent.nombre+codigo) )
                        {
                            nomEntorno.add("CICLO_DO"+ent.nombre+codigo);
                            stackEnvC3D.push(nuevoEnt);
                        }

                        while (contador <= cont_final)
                        {
                            Object salida = visitLinstrucciones(ctx.bp);
                            if(salida instanceof  Exit)
                            {
                                break;
                            }
                            if(salida instanceof Cycle)
                            {
                                contador = contador+salto;
                                id.setValue(contador);
                                ent.AsignarVar(ctx.var.getText(),id);
                                continue;
                            }
                            contador = contador+salto;
                            id.setValue(contador);
                            ent.AsignarVar(ctx.var.getText(),id);
                            _break ++;
                            if(_break == 1000)
                            {
                                break;
                            }
                        }
                        stackEnv.pop();
                    }
                    else
                    {
                        Errores.AgregarError("Cantidad en condiconales incorrectos ",ent.nombre,String.valueOf(ctx.lc.getStart().getLine()),String.valueOf(ctx.lc.getStart().getCharPositionInLine()));
                        return  null;
                    }


                }
                else
                {
                    Errores.AgregarError("Error en  Ciclo Do, condicional no valido",ent.nombre,String.valueOf(ctx.var.getLine()),String.valueOf(ctx.var.getCharPositionInLine()));
                    return  null;
                }
            }
            else
            {
                if(idDoI.toLowerCase().equals(idDoF.toLowerCase()))
                {
                    //obtener el simbolo del indice
                    Symbol id = ent.Buscar(ctx.var.getText());
                    if(id !=  null)
                    {
                        // obtener inicio, fin y salto
                        for(ParseTree c :ctx.lc.children)
                        {
                            if(!(c instanceof TerminalNodeImpl))
                            {
                                Symbol auxCond = (Symbol) visit(c);
                                if(auxCond.getType()== Type.INTEGER)
                                {
                                    cond.add((Integer) auxCond.getValue());
                                }
                                else
                                {
                                    Errores.AgregarError("Condicional no valido ",ent.nombre,String.valueOf(auxCond.getLine()),String.valueOf(auxCond.getColumn()));
                                    return  null;
                                }
                            }
                        }
                        // verificar el tamaño de ciclo
                        if(cond.size() == 2 || cond.size() == 3)
                        {
                            int contador = cond.get(0);
                            int cont_final = cond.get(1);
                            int salto = (cond.size() == 3)? cond.get(2): 1;
                            int _break = 0;
                            // actualizar el simbolo
                            id.setValue(contador);
                            ent.AsignarVar(ctx.var.getText(),id);
                            //Symbol id = ent.Buscar(ctx.var.getText());
                            Environment  nuevoEnt = new Environment(ent,"CICLO_DO"+ent.nombre+codigo);
                            //ent.siguiente = nuevoEnt;
                            stackEnv.push(nuevoEnt);
                            if(!nomEntorno.contains("CICLO_DO"+ent.nombre+codigo) )
                            {
                                nomEntorno.add("CICLO_DO"+ent.nombre+codigo);
                                stackEnvC3D.push(nuevoEnt);
                            }
                            while (contador <= cont_final)
                            {
                                Object salida = visitLinstrucciones(ctx.bp);
                                if(salida instanceof  Exit)
                                {
                                    break;
                                }
                                if(salida instanceof Cycle)
                                {
                                    contador = contador+salto;
                                    id.setValue(contador);
                                    ent.AsignarVar(ctx.var.getText(),id);
                                    continue;
                                }
                                contador = contador+salto;
                                id.setValue(contador);
                                ent.AsignarVar(ctx.var.getText(),id);
                                _break ++;
                                if(_break == 1000)
                                {
                                    break;
                                }
                            }
                            stackEnv.pop();
                        }
                        else
                        {
                            Errores.AgregarError("Cantidad en condiconales incorrectos ",ent.nombre,String.valueOf(ctx.lc.getStart().getLine()),String.valueOf(ctx.lc.getStart().getCharPositionInLine()));
                            return  null;
                        }


                    }
                    else
                    {
                        Errores.AgregarError("Error en  Ciclo Do, condicional no valido",ent.nombre,String.valueOf(ctx.var.getLine()),String.valueOf(ctx.var.getCharPositionInLine()));
                        return  null;
                    }
                }
                else
                {
                    Errores.AgregarError("Error en Ciclo Do: Error en declaracion de etiqueta ",ent.nombre,String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
                    return  null;
                }

            }


        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error en Ciclo Do",ent.nombre,String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     * DECLARARCION ARREGLOS ESTATICOS
     **/
    public Object visitArreglosestaticos(JGrammarParser.ArreglosestaticosContext ctx)
    {
        Environment ent = stackEnv.peek();
        // obtener tipo del arreglo
        Type tipoArreglo = visitType(ctx.t);
        try
        {// obtener dimension
            int a = -1;
            int b = -1;
            int dimPadre =0;
            if(ctx.d != null)
            {
                for(ParseTree i: ctx.d.children)
                {
                    if(!(i instanceof TerminalNodeImpl))
                    {
                        Symbol dimension = (Symbol) visit(i);
                        if(dimension.getType()== Type.INTEGER)
                        {
                            if (a==-1)
                            {
                                a = (int) dimension.getValue();
                                dimPadre = 1;
                            }
                            else
                            {
                                b = (int)dimension.getValue();
                                dimPadre = 2;
                            }

                        }
                        else
                        {
                            Errores.AgregarError("Dimension del arreglo no valido",ent.nombre,String.valueOf(dimension.getLine()),String.valueOf(dimension.getColumn()));
                            return  null;
                        }
                    }
                }
            }
            // asignar arreglos
            if(ctx.l !=null)
            {
                for(ParseTree lis: ctx.l.children)
                {

                    if(lis instanceof  JGrammarParser.ListaArraSimpleContext)
                    {
                        JGrammarParser.ListaArraSimpleContext lisAct = (JGrammarParser.ListaArraSimpleContext) lis;
                        // obtener id de arreglo
                        String idArray = lisAct.i.getText();
                        // verificar si ya existe el arreglo
                        if(!ent.TablaSimboloArrays.containsKey(idArray.toUpperCase()))
                        {
                            // verificar si tiene dimension individual
                            int dimActual = 0;
                            int c = -1;
                            int d = -1;
                            if(lisAct.d != null)
                            {
                                for(ParseTree i: lisAct.d.children)
                                {
                                    if(!(i instanceof TerminalNodeImpl))
                                    {
                                        Symbol dimension = (Symbol) visit(i);
                                        if(dimension.getType()== Type.INTEGER)
                                        {
                                            if (c==-1)
                                            {
                                                c = (int) dimension.getValue();
                                                dimActual = 1;
                                            }
                                            else
                                            {
                                                d = (int)dimension.getValue();
                                                dimActual = 2;
                                            }
                                        }
                                        else
                                        {
                                            Errores.AgregarError("Dimension del arreglo no valido: +" +idArray,ent.nombre,String.valueOf(dimension.getLine()),String.valueOf(dimension.getColumn()));
                                            return  null;
                                        }
                                    }
                                }
                            }
                            // agregar arreglo
                            if(dimActual !=0)
                            {
                                if(dimActual ==2)
                                {
                                    if(tipoArreglo == Type.INTEGER)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< d;i++)
                                        {
                                            newArray2.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,c,d,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.REAL)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< d;i++)
                                        {
                                            newArray2.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,c,d,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.CHAR)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< d;i++)
                                        {
                                            newArray2.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,c,d,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error al declarar el arreglo " +idArray+" tipo no soportado ",ent.nombre,String.valueOf(lisAct.getStart().getLine()),String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                    }

                                }
                                else
                                {
                                    if (tipoArreglo == Type.INTEGER)
                                    {
                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                        }

                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, c, d, 1,false,false,posicion);
                                        ent.nuevoArrayS(idArray, nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if (tipoArreglo == Type.REAL)
                                    {

                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, c, d, 1,false,false,posicion);
                                        ent.nuevoArrayS(idArray, nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if (tipoArreglo == Type.CHAR)
                                    {
                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< c;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                        }

                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, c, d, 1,false,false,posicion);
                                        ent.nuevoArrayS(idArray, nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error al declarar el arreglo " + idArray + " tipo no soportado ", ent.nombre, String.valueOf(lisAct.getStart().getLine()), String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                    }
                                }
                            }
                            else
                            {
                                if(dimPadre ==2)
                                {
                                    if(tipoArreglo == Type.INTEGER)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< b;i++)
                                        {
                                            newArray2.add(i,new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,a,b,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.REAL)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< b;i++)
                                        {
                                            newArray2.add(i,new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,a,b,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.CHAR)
                                    {
                                        ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                        ArrayList<Symbol> newArray2 = new ArrayList<>();
                                        for (int i =0 ;i< b;i++)
                                        {
                                            newArray2.add(i,new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                        }
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray1.add(newArray2);
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray1,Type.ARRAY,tipoArreglo,a,b,2,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error al declarar el arreglo " +idArray+" tipo no soportado ",ent.nombre,String.valueOf(lisAct.getStart().getLine()),String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                    }
                                }
                                else
                                {
                                    if(tipoArreglo == Type.INTEGER)
                                    {
                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                        }

                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray,Type.ARRAY,tipoArreglo,a,b,1,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.REAL)
                                    {

                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray,Type.ARRAY,tipoArreglo,a,b,1,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else if(tipoArreglo == Type.CHAR)
                                    {
                                        ArrayList<Symbol> newArray = new ArrayList<>();
                                        for (int i =0 ;i< a;i++)
                                        {
                                            newArray.add(i,new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                        }
                                        int posicion  = ent.ul_pos;
                                        ent.ul_pos ++;
                                        ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray,newArray,Type.ARRAY,tipoArreglo,a,b,1,false,false,posicion);
                                        ent.nuevoArrayS(idArray,nuevo);
                                        RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                                String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error al declarar el arreglo " +idArray+" tipo no soportado ",ent.nombre,String.valueOf(lisAct.getStart().getLine()),String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                    }
                                }
                            }
                        }
                        else
                        {
                            Errores.AgregarError("El arreglo: " +idArray+" ya existe en el ambito",ent.nombre,String.valueOf(lisAct.getStart().getLine()),String.valueOf(lisAct.getStart().getCharPositionInLine()));
                        }
                    }

                }
            }
            //System.out.println(ent.TablaSimboloArrays);
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al declarar Arreglo",ent.nombre,String.valueOf(ctx.t.getStart().getLine()),String.valueOf(ctx.t.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     * ASIGNACION DE ARREGLOS DE UNA DIMENSION
     **/
    public Object visitAsignarArrayUni(JGrammarParser.AsignarArrayUniContext ctx)
    {
        Environment ent = stackEnv.peek();
        String nomArray = ctx.i.getText();
        ArrayList<Symbol> val = new ArrayList<>();
        try
        {
            // parametros  : nombre y lista de valrores
            if(ctx.pos == null)
            {
                for(ParseTree v: ctx.l.children)
                {
                    Object newval = visit(v);
                    if(newval instanceof  Symbol)
                    {
                        val.add((Symbol) newval);
                    }
                }
                int  status = ent.AsignarArrayS(nomArray,val);
                if(status == 0 )
                {
                    return  true;
                }
                else if(status == 1)
                {
                    Errores.AgregarError("El arreglo no existe : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else if(status == 2)
                {
                    Errores.AgregarError("La dimension del arreglo  : " +ctx.i.getText() +" no es valido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 3)
                {
                    Errores.AgregarError("Indice fuera de rango del arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 4)
                {
                    Errores.AgregarError("Tipos  de valores incorrectos : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 5)
                {
                    Errores.AgregarError("Error al asignar el arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 6)
                {
                    Errores.AgregarError("Error al asignar el arreglo : " +ctx.i.getText() +" arreglo sin tamaño definido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                return null;

            }
            else
            {
                //    parametros : nombre y valor
                // obtener posicion del vector
                Object npos = visit(ctx.pos);
                if(npos instanceof  Symbol && ((Symbol) npos).getType() == Type.INTEGER)
                {
                    Symbol updateValue = (Symbol) visit(ctx.val);
                    int[] pos = {(int) ((Symbol) npos).getValue()};
                    int status= ent.AsignarArrayS(nomArray,pos,updateValue);
                    if(status == 0 )
                    {
                        return  true;
                    }
                    else if(status == 1)
                    {
                        Errores.AgregarError("El arreglo no existe : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else if(status == 2)
                    {
                        Errores.AgregarError("La dimension del arreglo  : " +ctx.i.getText() +" no es valido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;

                    }
                    else  if(status == 3)
                    {
                        Errores.AgregarError("Indice fuera de rango del arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 4)
                    {
                        Errores.AgregarError("Tipos  de valores incorrectos : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 5)
                    {
                        Errores.AgregarError("Error alasignar el arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    return  null;
                }
                else
                {
                    Errores.AgregarError("Error al asignar el arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al asignar el arreglo: " +ctx.i.getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }

    /**
     *  ASIGNACION DE ARREGLOS DE BIDIMENSIONALES
     **/
    public Object visitAsignarArrayBid(JGrammarParser.AsignarArrayBidContext ctx)
    {
        Environment ent = stackEnv.peek();
        String nomArray = ctx.i.getText();
        ArrayList <Integer> posiciones = new ArrayList<>();

        try
        {
            // parametros  : nombre, posicion y valor
            // obtener la posicion de la matriz
            for(ParseTree i: ctx.d.children)
            {
                if(!(i instanceof TerminalNodeImpl))
                {
                    Symbol dimension = (Symbol) visit(i);
                    if(dimension.getType()== Type.INTEGER)
                    {
                        posiciones.add((int) dimension.getValue());
                    }
                    else
                    {
                        Errores.AgregarError("Dimension del arreglo no valido",ent.nombre,String.valueOf(dimension.getLine()),String.valueOf(dimension.getColumn()));
                        return  null;
                    }
                }
            }
            // verificar que sea de dos dimensiones
            if(posiciones.size() != 2)
            {
                Errores.AgregarError("Dimension del arreglo no valido",ent.nombre,String.valueOf(ctx.d.getStart().getLine()),String.valueOf(ctx.d.getStart().getCharPositionInLine()));
                return  null;
            }
            else
            {
                Symbol updateValue = (Symbol) visit(ctx.val);
                int[] pos = {posiciones.get(0), posiciones.get(1)};
                int status= ent.AsignarArrayS(nomArray,pos,updateValue);
                if(status == 0 )
                {
                    return  true;
                }
                else if(status == 1)
                {
                    Errores.AgregarError("El arreglo no existe : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else if(status == 2)
                {
                    Errores.AgregarError("La dimension del arreglo  : " +ctx.i.getText() +" no es valido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 3)
                {
                    Errores.AgregarError("Indice fuera de rango del arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                else  if(status == 4)
                {
                    Errores.AgregarError("Tipos  de valores incorrectos : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return   null;
                }
                else  if(status == 5)
                {
                    Errores.AgregarError("Error alasignar el arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return  null;
                }
                return  null;
            }
        }catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al asignar el arreglo: " +ctx.i.getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }

    /**
     *  DECLARARCION ARREGLOS DINAMICOS
     **/
    public Object visitArreglosDinamicos(JGrammarParser.ArreglosDinamicosContext ctx)
    {
        Environment ent = stackEnv.peek();
        // obtener tipo del arreglo
        Type tipoArreglo = visitType(ctx.t);
        try
        {
            int dimension;
            // asignar arreglos
            if(ctx.l !=null)
            {
                for(ParseTree lis: ctx.l.children)
                {
                    if(lis instanceof  JGrammarParser.ListaArraBidContext)
                    {
                        JGrammarParser.ListaArraBidContext lisAct = (JGrammarParser.ListaArraBidContext) lis;
                        // obtener id de arreglo
                        String idArray = lisAct.i.getText();

                        // verificar si ya existe el arreglo
                        if(!ent.TablaSimboloArrays.containsKey(idArray.toUpperCase()))
                        {
                            // verificar  la dimension
                            dimension =0;
                            if(lisAct.d != null)
                            {
                                for(ParseTree i: lisAct.d.children)
                                {
                                    if((i instanceof TerminalNodeImpl))
                                    {
                                        String dospt = i.getText();
                                        dimension = (dospt.equals(":"))?dimension+1: dimension;
                                    }
                                }
                            }
                            // agregar arreglo
                            if(dimension ==2)
                            {
                                if (tipoArreglo == Type.INTEGER)
                                {
                                    ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add(new Symbol(0, 0, "", 0, Type.INTEGER, Dato.PRIMITIVO,-1));
                                    newArray1.add(newArray2);
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(), lisAct.i.getCharPositionInLine(), idArray, newArray1, Type.ARRAY, tipoArreglo, 0, 0, 2, true, false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);

                                }
                                else if (tipoArreglo == Type.REAL)
                                {
                                    ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add( new Symbol(0, 0, "", 0.00000000, Type.REAL, Dato.PRIMITIVO,-1));
                                    newArray1.add(newArray2);
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(), lisAct.i.getCharPositionInLine(), idArray, newArray1, Type.ARRAY, tipoArreglo, 0, 0, 2, true, false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                }
                                else if (tipoArreglo == Type.CHAR)
                                {
                                    ArrayList<ArrayList<Symbol>> newArray1 = new ArrayList<>();
                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                    newArray2.add( new Symbol(0, 0, "", "", Type.CHAR, Dato.PRIMITIVO,-1));
                                    newArray1.add(newArray2);
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(), lisAct.i.getCharPositionInLine(), idArray, newArray1, Type.ARRAY, tipoArreglo, 0, 0, 2, true, false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                }
                                else
                                {
                                    Errores.AgregarError("Error al declarar el arreglo " + idArray + " tipo no soportado ", ent.nombre, String.valueOf(lisAct.getStart().getLine()), String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                    return  null;
                                }
                            }
                            else if(dimension == 1)
                            {
                                //System.out.println("se agrega el arreglo "+idArray+" de dimension 1: D1: "+ c );
                                if (tipoArreglo == Type.INTEGER)
                                {
                                    ArrayList<Symbol> newArray = new ArrayList<>();
                                    newArray.add(new Symbol(0,0,"",0,Type.INTEGER,Dato.PRIMITIVO,-1));
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, 0, 0, 1,true,false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                }
                                else if (tipoArreglo == Type.REAL)
                                {
                                    ArrayList<Symbol> newArray = new ArrayList<>();
                                    newArray.add(new Symbol(0,0,"",0.00000000,Type.REAL,Dato.PRIMITIVO,-1));
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, 0, 0, 1,true,false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                }
                                else if (tipoArreglo == Type.CHAR)
                                {
                                    ArrayList<Symbol> newArray = new ArrayList<>();
                                    newArray.add(new Symbol(0,0,"","",Type.CHAR,Dato.PRIMITIVO,-1));
                                    int posicion  = ent.ul_pos;
                                    ent.ul_pos ++;
                                    ArrayS nuevo = new ArrayS(lisAct.i.getLine(),lisAct.i.getCharPositionInLine(),idArray, newArray, Type.ARRAY, tipoArreglo, 0, 0, 1,true,false,posicion);
                                    ent.nuevoArrayS(idArray, nuevo);
                                    RepSimbolos.AgregarTSimbolo(nuevo.getType().name(),idArray,String.valueOf(lisAct.i.getLine()),
                                            String.valueOf(lisAct.i.getCharPositionInLine()),nuevo.getTypeData().name(),ent.nombre);
                                }
                                else
                                {
                                     Errores.AgregarError("Error al declarar el arreglo " + idArray + " tipo no soportado ", ent.nombre, String.valueOf(lisAct.getStart().getLine()), String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                     return  null;
                                }

                            }
                            else
                            {
                                Errores.AgregarError("Error al declarar el arreglo " + idArray + " Dimension fuera de rango ", ent.nombre, String.valueOf(lisAct.getStart().getLine()), String.valueOf(lisAct.getStart().getCharPositionInLine()));
                                return  null;
                            }

                        }
                        else
                        {
                            Errores.AgregarError("El arreglo: " +idArray+" ya existe en el ambito",ent.nombre,String.valueOf(lisAct.getStart().getLine()),String.valueOf(lisAct.getStart().getCharPositionInLine()));
                        }

                    }

                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al declarar Arreglo",ent.nombre,String.valueOf(ctx.t.getStart().getLine()),String.valueOf(ctx.t.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     * FUNCION ALLOCATE
     **/
    public Object visitFunAllocate(JGrammarParser.FunAllocateContext ctx)
    {
        Environment ent = stackEnv.peek();

        try
        {
            // asignar arreglos
            if(ctx.l !=null)
            {
                for(ParseTree lis: ctx.l.children)
                {
                    if(lis instanceof  JGrammarParser.ListaArraSimpleContext)
                    {
                        JGrammarParser.ListaArraSimpleContext lisAct = (JGrammarParser.ListaArraSimpleContext) lis;
                        // obtener id de arreglo
                        String idArray = lisAct.i.getText();
                        ArrayList<Integer> dimensiones = new ArrayList<>();
                        // verificar las dimensiones del arreglo
                        if(lisAct.d != null)
                        {
                            for(ParseTree i: lisAct.d.children)
                            {
                                if(!(i instanceof TerminalNodeImpl))
                                {
                                    Symbol dimension = (Symbol) visit(i);
                                    if(dimension.getType()== Type.INTEGER)
                                    {
                                       dimensiones.add((int) dimension.getValue());
                                    }
                                    else
                                    {
                                        Errores.AgregarError("Dimension del arreglo no valido: +" +idArray,ent.nombre,String.valueOf(dimension.getLine()),String.valueOf(dimension.getColumn()));
                                        return  null;
                                    }
                                }
                            }
                        }
                        // asginar arreglo
                        ent.FuncAllocate(idArray,dimensiones,String.valueOf(lisAct.i.getLine()),Integer.toString(lisAct.i.getCharPositionInLine()));
                    }

                }
            }


        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al declarar dimensiones del  Arreglo",ent.nombre,String.valueOf(ctx.l.getStart().getLine()),String.valueOf(ctx.l.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     *  FUNCION DEALLOCATE
     **/
    public Object visitFunDeAllocate(JGrammarParser.FunDeAllocateContext ctx)
    {
        Environment ent = stackEnv.peek();

        try
        {
            // asignar arreglos
            if(ctx.l !=null)
            {
                for(ParseTree lis: ctx.l.children)
                {
                    if(lis instanceof  TerminalNodeImpl)
                    {
                        // obtener id de arreglo
                        String idArray = lis.getText();
                        // asginar arreglo
                        ent.FuncDeAllocate(idArray,String.valueOf(ctx.l.getStart().getLine()),Integer.toString(ctx.l.getStart().getCharPositionInLine()));
                    }
                }
            }


        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al declarar dimensiones del  Arreglo",ent.nombre,String.valueOf(ctx.l.getStart().getLine()),String.valueOf(ctx.l.getStart().getCharPositionInLine()));
            return  null;
        }
        return true;
    }

    /**
     * SENTENCIA IF
     **/
    public Object visitSentenciaIf (JGrammarParser.SentenciaIfContext ctx)
    {
        try
        {
            Environment entActual = stackEnv.peek();
            // obtener condicion inicial
            Object conInicial = visit(ctx.ep);
            if(conInicial instanceof  Symbol)
            {
                // inicio de sentencia if
                // verificar que sea de tipo boolean
                if(((Symbol) conInicial).getType() == Type.LOGICAL )
                {
                    // verificar que seaverdadero
                    if((boolean)((Symbol)conInicial).getValue())
                    {
                        Environment nuevoEnt = new Environment(entActual,"IF_"+entActual.nombre+codigo);
                        //entActual.siguiente = nuevoEnt;
                        stackEnv.push(nuevoEnt);
                        if(!nomEntorno.contains("IF_"+entActual.nombre+codigo) )
                        {
                            nomEntorno.add("IF_"+entActual.nombre+codigo);
                            stackEnvC3D.push(nuevoEnt);
                        }
                        Object control = visitLinstrucciones(ctx.bp);
                        if(control instanceof Exit || control instanceof  Cycle)
                        {
                            stackEnv.pop();
                            return  control;
                        }
                        stackEnv.pop();
                    }
                    else
                    {
                        if(ctx.bElse != null)
                        {
                            boolean continuar= true;
                            for(JGrammarParser.ListaElseContext el: ctx.bElse.listaElse() )
                            {
                                    Object result = visitListaElse(el);
                                    if(result != null)
                                    {
                                        if(result instanceof  Exit || result instanceof  Cycle)
                                        {
                                            return  result;
                                        }
                                        continuar = false;
                                        break;
                                    }

                            }
                            if(continuar)
                            {
                                if(ctx.bfinal != null)
                                {
                                    Environment nuevoEnt = new Environment(entActual,"ELSE_"+entActual.nombre+codigo);
                                    //entActual.siguiente = nuevoEnt;
                                    stackEnv.push(nuevoEnt);
                                    if(!nomEntorno.contains("ELSE_"+entActual.nombre+codigo ))
                                    {
                                        nomEntorno.add("ELSE_"+entActual.nombre+codigo);
                                        stackEnvC3D.push(nuevoEnt);
                                    }

                                    Object control = visit(ctx.bfinal);
                                    if(control instanceof Exit || control instanceof  Cycle)
                                    {
                                        stackEnv.pop();
                                        return  control;
                                    }
                                    //System.out.println("Entorno else: " + entActual.nombre);
                                    stackEnv.pop();
                                }
                            }
                        }
                        else
                        {
                            if(ctx.bfinal != null)
                            {
                                Environment nuevoEnt =new Environment(entActual,"ELSE_"+entActual.nombre+codigo);
                                //entActual.siguiente = nuevoEnt;
                                stackEnv.push(nuevoEnt);
                                if(!nomEntorno.contains("ELSE_"+entActual.nombre+codigo) )
                                {
                                    nomEntorno.add("ELSE_"+entActual.nombre+codigo);
                                    stackEnvC3D.push(nuevoEnt);
                                }
                                Object control = visit(ctx.bfinal);
                                if(control instanceof Exit || control instanceof  Cycle)
                                {
                                    stackEnv.pop();
                                    return  control;
                                }
                                stackEnv.pop();
                            }

                        }

                    }
                }
                else
                {
                    Errores.AgregarError("Error en condicion If",entActual.nombre,String.valueOf(ctx.ep.getStart().getLine()),String.valueOf(ctx.ep.getStart().getCharPositionInLine()));
                }

            }
            else
            {
                Errores.AgregarError("Error en condicion If",entActual.nombre,String.valueOf(ctx.ep.getStart().getLine()),String.valueOf(ctx.ep.getStart().getCharPositionInLine()));
            }

        }
        catch ( Exception e)
        {
            System.out.println(e);

        }
        return null;

    }

    /**
     * FRAGMENTO EJECUCION ELSE IF
     * */
    public Object visitListaElse(JGrammarParser.ListaElseContext ctx)
    {
        Environment entActual = stackEnv.peek();
        try{
            Object conInicial = visit(ctx.ep);
            if(conInicial instanceof  Symbol){
                if(((Symbol) conInicial).getType() == Type.LOGICAL )
                {
                    // verificar que seaverdadero
                    if((boolean)((Symbol)conInicial).getValue())
                    {
                        Environment nuevoEnt = new Environment(entActual,"ElSE_IF_"+entActual.nombre+codigo);

                        //entActual.siguiente = nuevoEnt;
                        stackEnv.push(nuevoEnt);
                        if(!nomEntorno.contains("ElSE_IF_"+entActual.nombre+codigo) )
                        {
                            nomEntorno.add("ElSE_IF_"+entActual.nombre+codigo);
                            stackEnvC3D.push(nuevoEnt);
                        }
                        Object control = visitLinstrucciones(ctx.bp);
                        if(control instanceof Exit || control instanceof  Cycle)
                        {
                            stackEnv.pop();
                            return  control;
                        }
                        stackEnv.pop();
                        return  true;
                    }
                    return  null;
                }
                else
                {
                    Errores.AgregarError("Error en condicion Else If",entActual.nombre,String.valueOf(ctx.ep.getStart().getLine()),String.valueOf(ctx.ep.getStart().getCharPositionInLine()));
                    return  null;
                }
            }
            else
            {
                Errores.AgregarError("Error en condicion else If",entActual.nombre,String.valueOf(ctx.ep.getStart().getLine()),String.valueOf(ctx.ep.getStart().getCharPositionInLine()));
                return  null;
            }

        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al ejecutar else if: ",entActual.nombre,String.valueOf(ctx.ELSE().getSymbol().getLine()),String.valueOf(ctx.ELSE().getSymbol().getCharPositionInLine()));
            return  null;
        }
    }
    /**
     *  FUNCION SIZE()
     * */
    public Symbol visitOpSize(JGrammarParser.OpSizeContext ctx)
    {
        Environment ent = stackEnv.peek();
        try
        {
            ArrayS id = ent.BuscarArrays(ctx.i.getText());
            if (id == null)
            {
                Errores.AgregarError("Array no encontrado: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                return null;
            }
            else
            {
                int size = (id.getDim()==2)?(id.getSize1()* id.getSize2()): id.getSize1();
                return  new Symbol(id.getLine(), id.getColumn(), id.getId(),size,Type.INTEGER,Dato.PRIMITIVO,-1);
            }
        }
        catch (Exception e)
        {
            Errores.AgregarError("Array no encontrado: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }

    /**
     * EXPRESIONES LOGICAS
     **/
    public Symbol visitOpLogica(JGrammarParser.OpLogicaContext ctx)
    {
        Environment ent = stackEnv.peek();
        try
        {
            Symbol izq;
            Symbol der;
            der = (Symbol) visit(ctx.right);
            String operacion = ctx.op.getText();

            switch (operacion)
            {
                case ".not." :
                    if(der.getType() == Type.LOGICAL)
                    {
                        boolean nuevoValor = !(boolean) der.getValue();
                        return new Symbol(ctx.right.getStart().getLine(),ctx.right.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                    }
                    else
                    {
                        Errores.AgregarError("Operacion Logica no valida: Error al operar expresiones",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        return  null;
                    }
                case  ".and.":
                    izq = (Symbol) visit(ctx.left);
                    if(izq.getType() == Type.LOGICAL && der.getType() == Type.LOGICAL )
                    {
                        boolean nuevoValor = (boolean) izq.getValue() && (boolean) der.getValue();
                        return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                    }
                    else
                    {
                        Errores.AgregarError("Operacion Logica no valida: Error al operar expresiones ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        return  null;
                    }
                case  ".or.":
                    izq = (Symbol) visit(ctx.left);
                    if(izq.getType() == Type.LOGICAL && der.getType() == Type.LOGICAL )
                    {
                        boolean nuevoValor = (boolean) izq.getValue() || (boolean) der.getValue();
                        return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                    }
                    else
                    {
                        Errores.AgregarError("Operacion Logica no valida: Error al operar expresiones ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        return  null;
                    }
                default:
                   Errores.AgregarError("Operacion Logica no valida: Operador no econtrado ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                   return  null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al realizar operación Lógica ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
            return  null;
        }
    }

    /** EXPRESIONES RELACIONALES
     **/
    public Symbol visitOpRelacional(JGrammarParser.OpRelacionalContext ctx)
    {
        Environment ent = stackEnv.peek();
        Type[][] opRelaciona = {
                // null    // int       // rel     //complex // char    // logical
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // null
                {Type.NULL,Type.INTEGER,Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // int
                {Type.NULL,Type.REAL,   Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // real
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.CHAR,Type.NULL}, // char
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.LOGICAL}, // Logical
        };
        try
        {
            Symbol izq;
            Symbol der;
            der = (Symbol) visit(ctx.right);
            izq = (Symbol) visit(ctx.left);
            String operacion = ctx.op.getText();
            if(der != null && izq != null)
            {
                Type dom = opRelaciona[izq.getType().ordinal()][der.getType().ordinal()];
                switch (operacion)
                {
                    case ".eq." :
                    case "==" :
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            boolean nuevoValor = Double.parseDouble(izq.getValue().toString()) ==  Double.parseDouble(der.getValue().toString());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() == (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else if(dom == Type.LOGICAL)
                        {
                            boolean nuevoValor = (boolean)izq.getValue() == (boolean) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case  "/=":
                    case  ".ne.":
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            boolean nuevoValor = izq.getValue() !=  der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() != (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else if(dom == Type.LOGICAL)
                        {
                            boolean nuevoValor = (boolean)izq.getValue() != (boolean) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case  "<":
                    case  ".lt.":
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            if(izq.getType() == Type.REAL && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(Float)izq.getValue() < (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.REAL && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(Float)izq.getValue() < (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(int)izq.getValue() < (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(int)izq.getValue() < (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() < (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case  ">":
                    case  ".gt.":
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            if(izq.getType() == Type.REAL && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(Float)izq.getValue() > (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.REAL && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(Float)izq.getValue() > (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(int)izq.getValue() > (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(int)izq.getValue() > (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() > (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case  "<=":
                    case  ".le.":
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            if(izq.getType() == Type.REAL && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(Float)izq.getValue() <= (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.REAL && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(Float)izq.getValue() <= (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(int)izq.getValue() <= (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(int)izq.getValue() <= (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() <= (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);

                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case  ">=":
                    case  ".ge.":
                        if(dom == Type.REAL || dom == Type.INTEGER)
                        {
                            if(izq.getType() == Type.REAL && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(Float)izq.getValue() >= (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.REAL && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(Float)izq.getValue() >= (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.INTEGER)
                            {
                                boolean nuevoValor =(int)izq.getValue() >= (int) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                            else if(izq.getType() == Type.INTEGER && der.getType() == Type.REAL)
                            {
                                boolean nuevoValor =(int)izq.getValue() >= (Float) der.getValue();
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                            }
                        }
                        else if(dom == Type.CHAR)
                        {
                            boolean nuevoValor = (char)izq.getValue() >= (char) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.LOGICAL, Dato.PRIMITIVO,-1);
                        }
                        else
                        {
                            Errores.AgregarError("Operacion Relacional no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    default:
                        Errores.AgregarError("Operacion Relacional no valida: Operador no econtrado ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        return  null;
                }

            }else {
                Errores.AgregarError("Operacion Relacional no valido ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                return  null;
            }

        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al operar la expresion relacional ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
            return  null;
        }
    }

    /**
     * EXPRESIONES ARITMETICAS
     **/

    public Symbol visitOpAritmetica(JGrammarParser.OpAritmeticaContext ctx)
    {
        Environment ent = stackEnv.peek();
        Type[][] opAritmetica = {
                // null    // int       // rel     //complex // char    // logical
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // null
                {Type.NULL,Type.INTEGER,Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // int
                {Type.NULL,Type.REAL,   Type.REAL,Type.NULL,Type.NULL,Type.NULL}, // real
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // char
                {Type.NULL,Type.NULL,   Type.NULL,Type.NULL,Type.NULL,Type.NULL}, // complex
        };
        try
        {
            Symbol izq;
            Symbol der;
            der = (Symbol) visit(ctx.right);
            String operacion = ctx.op.getText();
            if(der != null )
            {
                switch (operacion)
                {
                    //POTENCIA
                    case "**" :
                        izq = (Symbol) visit(ctx.left);

                        Type dom = opAritmetica[izq.getType().ordinal()][der.getType().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            double   nuevoValor = Math.pow((int)izq.getValue(),(int) der.getValue());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",(int)nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                        }
                        else if(dom == Type.REAL)
                        {
                            double   nuevoValor = Math.pow(Double.valueOf(izq.getValue().toString()),Double.valueOf(der.getValue().toString()));
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                        }
                        else
                        {
                            Errores.AgregarError("Operacion Aritmetica no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case "*" :
                        izq = (Symbol) visit(ctx.left);
                        dom = opAritmetica[izq.getType().ordinal()][der.getType().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            int   nuevoValor = (int)izq.getValue() *(int) der.getValue();
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                        }
                        else if(dom == Type.REAL)
                        {
                            double   nuevoValor = Double.valueOf(izq.getValue().toString()) *Double.valueOf(der.getValue().toString());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                        }
                        else
                        {
                            Errores.AgregarError("Operacion Aritmetica no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case "/" :
                        izq = (Symbol) visit(ctx.left);
                        dom = opAritmetica[izq.getType().ordinal()][der.getType().ordinal()];
                        if((int) der.getValue() == 0)
                        {
                            Errores.AgregarError("Operacion Aritmetica no valida: Error al dividir por =",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;

                        }
                        if(dom == Type.INTEGER)
                        {
                            int   nuevoValor = ((int)izq.getValue() /(int) der.getValue());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                        }
                        else if(dom == Type.REAL)
                        {
                            double   nuevoValor = Double.valueOf(izq.getValue().toString()) /Double.valueOf(der.getValue().toString());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                        }
                        else
                        {
                            Errores.AgregarError("Operacion Aritmetica no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case "+" :
                        izq = (Symbol) visit(ctx.left);
                        dom = opAritmetica[izq.getType().ordinal()][der.getType().ordinal()];
                        if(dom == Type.INTEGER)
                        {
                            int   nuevoValor = ((int)izq.getValue() +(int) der.getValue());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                        }
                        else if(dom == Type.REAL)
                        {
                            double   nuevoValor = Double.valueOf(izq.getValue().toString()) + Double.valueOf(der.getValue().toString());
                            return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                        }
                        else
                        {
                            Errores.AgregarError("Operacion Aritmetica no valida: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                            return  null;
                        }
                    case "-" :
                        if(ctx.left == null)
                        {
                            if(der.getType() == Type.INTEGER)
                            {
                                int   nuevoValor = -1*(int) der.getValue() ;
                                return new Symbol(ctx.right.getStart().getLine(),ctx.right.getStart().getCharPositionInLine(),"",nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                            }else
                            {
                                double   nuevoValor = -1*Double.valueOf(der.getValue().toString()) ;
                                return new Symbol(ctx.right.getStart().getLine(),ctx.right.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                            }
                        }
                        else
                        {
                            izq = (Symbol) visit(ctx.left);
                            dom = opAritmetica[izq.getType().ordinal()][der.getType().ordinal()];
                            if(dom == Type.INTEGER)
                            {
                                int   nuevoValor = ((int)izq.getValue() -(int) der.getValue());
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.INTEGER, Dato.PRIMITIVO,-1);
                            }
                            else if(dom == Type.REAL)
                            {
                                double   nuevoValor = Double.valueOf(izq.getValue().toString()) - Double.valueOf(der.getValue().toString());
                                return new Symbol(ctx.left.getStart().getLine(),ctx.left.getStart().getCharPositionInLine(),"",nuevoValor,Type.REAL, Dato.PRIMITIVO,-1);
                            }
                            else
                            {
                                Errores.AgregarError("Operacion Aritmetica no valido: error entre tipos",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                                return  null;
                            }
                        }

                    default:
                        Errores.AgregarError("Operacion Aritmetica no valido: Operador no econtrado ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                        return  null;
                }

            }
            else
            {
                Errores.AgregarError("Operacion Aritmetica no valido ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
                return  null;
            }

        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error al realizar operación Aritmetica ",ent.nombre,String.valueOf(ctx.op.getLine()),String.valueOf(ctx.op.getCharPositionInLine()));
            return  null;
        }
    }

    /** ============================== FUNCION IMPRIMIR ==============================
     * */
    // -------------- REALIZADO ------------ A FALTA DE ARREGLOS  y CADENAS
   public Object visitPrint(JGrammarParser.PrintContext ctx)
   {
       Environment ent = stackEnv.peek();
       String consola ="";
        try
        {
            boolean isErrores = false;
            for(ParseTree i : ctx.l.children)
            {
                if(!(i instanceof  TerminalNodeImpl))
                {
                    Object aux = visit(i);
                    //  imprimir los objetos de tipo simbolo
                    if(aux instanceof  Symbol)
                    {
                        if(((Symbol) aux).getValue() == null)
                        {
                            Type caso = ((Symbol) aux).getType();
                            if(caso == Type.INTEGER){
                                consola += "0";
                            }else if(caso == Type.REAL) {
                                consola += "0.00000000";
                            }else if(caso == Type.COMPLEX){
                                consola += "9.192517926E-43,0.00000000";
                            }else if(caso == Type.CHAR){
                                consola += "";
                            }else if(caso == Type.LOGICAL){
                                consola += "F";
                            }
                        }else
                        {
                            Type caso = ((Symbol) aux).getType();
                            if(caso == Type.LOGICAL)
                            {
                                if(((boolean)((Symbol) aux).getValue()))
                                {
                                    consola += "T";
                                }
                                else
                                {
                                    consola += "F";
                                }
                            }
                            else
                            {
                                consola += ((Symbol) aux).getValue().toString();
                            }
                        }
                    }// imprimir los objetos de tipo string
                    else if(aux instanceof  String)
                    {
                        consola += aux.toString();
                    }// imprimir los objetos de tipo Arreglo
                    else if(aux instanceof  ArrayS)
                    {
                        ArrayS arreglo = (ArrayS) aux;
                        boolean allocatable = arreglo.isAllocatable();
                        boolean allocate  = arreglo.isAllocate();
                        int dimension  = arreglo.getDim();
                        // arreglo dinamico
                        if(allocatable)
                        {
                            String auxconsola = "";
                            if(allocate)
                            {
                                // unidimensional
                                if(dimension == 1)
                                {
                                    ArrayList<Symbol> recorrer = (ArrayList<Symbol>) arreglo.getValue();
                                    for(Object val : recorrer)
                                    {
                                        if(val instanceof  Symbol)
                                        {
                                            auxconsola += ((Symbol) val).getValue().toString() +" ";
                                        }
                                    }
                                    consola += auxconsola;
                                }// dos dimensiones
                                else if(dimension == 2 )
                                {
                                    ArrayList <ArrayList<Symbol>> filas = (ArrayList<ArrayList<Symbol>>) arreglo.getValue();
                                    for(ArrayList<Symbol> fil: filas)
                                    {
                                        for(Object val : fil){
                                            if(val instanceof  Symbol)
                                            {
                                                auxconsola += ((Symbol) val).getValue().toString() +" ";
                                            }
                                        }
                                        auxconsola +="\n";
                                    }
                                    consola += auxconsola;
                                }
                            }
                            else
                            {
                                consola += " ";
                            }
                        }
                        else
                        {
                            String auxconsola = "";
                            // unidimensional
                            if(dimension == 1)
                            {
                                ArrayList<Symbol> recorrer = (ArrayList<Symbol>) arreglo.getValue();
                                for(Object val : recorrer)
                                {
                                    if(val instanceof  Symbol)
                                    {
                                        auxconsola += ((Symbol) val).getValue().toString() +" ";
                                    }
                                }
                                consola += auxconsola;
                            }// dos dimensiones
                            else if(dimension == 2 )
                            {
                                ArrayList <ArrayList<Symbol>> filas = (ArrayList<ArrayList<Symbol>>) arreglo.getValue();
                                for(ArrayList<Symbol> fil: filas)
                                {
                                    for(Object val : fil)
                                    {
                                        if(val instanceof  Symbol)
                                        {
                                            auxconsola += ((Symbol) val).getValue().toString() +" ";
                                        }
                                    }
                                    auxconsola +="\n";
                                }
                                consola += auxconsola;
                            }
                        }
                    }
                    else
                    {
                        isErrores = true;
                        Errores.AgregarError("Error en impresion, no se encontro el valor ", ent.nombre,String.valueOf(ctx.l.getStart().getLine()),String.valueOf(ctx.l.getStart().getCharPositionInLine()));

                    }
                }

            }
            if(isErrores)
            {
                return null;
            }
            Global.consola +=consola +"\n";
        }
        catch (Exception e)
        {
            System.out.println(e);
            Errores.AgregarError("Error en impresion", ent.nombre,String.valueOf(ctx.l.getStart().getLine()),String.valueOf(ctx.l.getStart().getCharPositionInLine()));
        }
       return true;
   }
    /**
     * MANEJO DE VARIABLES
     * */

    /**
     * Declaracion de variables
     **/
    // -------------- REALIZADO ------------
    public Object visitDeclaracion(JGrammarParser.DeclaracionContext ctx)
    {
        Type tipoPadre = visitType(ctx.t);
        Environment ent = stackEnv.peek();

        for (int i=0;i<ctx.l.children.size();i++ )
        {
            if(ctx.l.children.get(i) instanceof JGrammarParser.VariablesContext)
            {
                JGrammarParser.VariablesContext v = (JGrammarParser.VariablesContext) ctx.l.children.get(i);
                // verificar si existe la variable
                String id = v.i.getText();
                if(!ent.TablaSimbolo.containsKey(v.i.getText().toUpperCase()))
                {
                    // verificar si su valor es null
                    if(v.e == null)
                    {
                        if(tipoPadre == Type.INTEGER)
                        {
                            int posicion = ent.ul_pos;
                            ent.ul_pos ++;
                            Symbol nuevo = new Symbol(v.i.getLine(),v.i.getLine(),v.i.getText(), 0,tipoPadre,Dato.VARIABLE,posicion);
                            ent.nuevoSimbolo(v.i.getText(), nuevo);
                            RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                    String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                        }
                        else if(tipoPadre == Type.REAL)
                        {
                            int posicion = ent.ul_pos;
                            ent.ul_pos ++;
                            Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(), 0.00000000,tipoPadre,Dato.VARIABLE,posicion);
                            ent.nuevoSimbolo(v.i.getText(), nuevo);
                            RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                    String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                        }
                        else if(tipoPadre == Type.COMPLEX)
                        {
                            int posicion = ent.ul_pos;
                            ent.ul_pos ++;
                            Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),0.00000000 ,tipoPadre,Dato.VARIABLE,posicion);
                            ent.nuevoSimbolo(v.i.getText(), nuevo);
                            RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                    String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                        }
                        else if(tipoPadre == Type.CHAR)
                        {
                            int posicion = ent.ul_pos;
                            ent.ul_pos ++;
                            Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),"" ,tipoPadre,Dato.VARIABLE,posicion);
                            ent.nuevoSimbolo(v.i.getText(), nuevo);
                            RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                    String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                        }
                        else if(tipoPadre == Type.LOGICAL)
                        {
                            int posicion = ent.ul_pos;
                            ent.ul_pos ++;
                            Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),false ,tipoPadre,Dato.VARIABLE,posicion);
                            ent.nuevoSimbolo(v.i.getText(), nuevo);
                            RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                    String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                        }

                    }
                    else
                    {
                        //verificar si es un simbolo
                        if(visit(v.e) instanceof Symbol)
                        {
                            Symbol  nuevoValor = (Symbol) visit(v.e);
                            // verificar si existe
                            if(nuevoValor != null)
                            {
                                // verificar que los tipos coincidan
                                if(nuevoValor.getType() == tipoPadre )
                                {
                                    // verificar si es un primitivo
                                    if(nuevoValor.getValue() != null)
                                    {
                                        int posicion = ent.ul_pos;
                                        ent.ul_pos ++;
                                        Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),nuevoValor.getValue(),tipoPadre,Dato.VARIABLE,posicion);
                                        ent.nuevoSimbolo(v.i.getText(), nuevo);
                                        RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                                String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                                    }
                                    else
                                    {
                                        Errores.AgregarError("No es posible asignar un valor nulo",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));
                                    }

                                }
                                /*else if(tipoPadre == Type.INTEGER  &&  nuevoValor.getType() == Type.REAL )
                                {
                                    // verificar si es un primitivo
                                    if(nuevoValor.getValue() != null){
                                        int nuevoval = (int)Math.round((Double) nuevoValor.getValue());
                                        int posicion = ent.ul_pos;
                                        ent.ul_pos ++;
                                        Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),nuevoval,tipoPadre,Dato.VARIABLE,posicion);
                                        ent.nuevoSimbolo(v.i.getText(), nuevo);
                                        RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                                String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                                    }else{
                                        Errores.AgregarError("No es posible asignar un valor nulo",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));
                                    }

                                }
                                else if(tipoPadre == Type.REAL &&  nuevoValor.getType() == Type.INTEGER )
                                {
                                    // verificar si es un primitivo
                                    if(nuevoValor.getValue() != null){
                                        double nuevoval = Double.parseDouble(nuevoValor.getValue().toString());
                                        int posicion = ent.ul_pos;
                                        ent.ul_pos ++;
                                        Symbol nuevo = new Symbol(v.i.getLine(),v.i.getCharPositionInLine(),v.i.getText(),nuevoval,tipoPadre,Dato.VARIABLE,posicion);
                                        ent.nuevoSimbolo(v.i.getText(), nuevo);
                                        RepSimbolos.AgregarTSimbolo("Primitivo",v.i.getText(),String.valueOf(v.i.getLine()),
                                                String.valueOf(v.i.getLine()),tipoPadre.name(),ent.nombre);
                                    }else{
                                        Errores.AgregarError("No es posible asignar un valor nulo",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));
                                    }

                                }*/
                                else
                                {
                                    Errores.AgregarError("El valor no coincide con el tipo declarado",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));

                                }
                            }
                            else
                            {
                                Errores.AgregarError("No es posible declarar la variable: " +v.i.getText() +" valor no existe",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));

                            }

                        }
                        else
                        {
                            Errores.AgregarError("No es posible asignar el valor a la variable: " +v.i.getText(),ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));
                        }

                    }

                }
                else
                {
                    Errores.AgregarError("La variable: " +v.i.getText()+" ya existe en el ambito",ent.nombre,String.valueOf(v.i.getLine()),String.valueOf(v.i.getCharPositionInLine()));
                }
            }

        }
        return true;
    }

    /**
     * REASIGNACION de variables
     **/

    public Object visitAsignacionvar(JGrammarParser.AsignacionvarContext ctx)
    {
        Environment ent = stackEnv.peek();
        try
        {

            Object val = visit(ctx.l);
            if(val instanceof  Symbol)
            {
                boolean status = ent.AsignarVar(ctx.i.getText(), (Symbol) val);
                if (!status)
                {
                    Errores.AgregarError("Variable no encontrado: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return null;
                }
                else
                {
                    return  true;
                }
            }
            else if(val instanceof  ArrayS)
            {
                if(((ArrayS) val).getDim() == 1)
                {
                    ArrayList <Symbol> aux = (ArrayList<Symbol>) ((ArrayS) val).getValue();
                    int status = ent.AsignarArrayS(ctx.i.getText(),aux);
                    if(status == 0 )
                    {
                        return  true;
                    }
                    else if(status == 1)
                    {
                        Errores.AgregarError("El arreglo no existe : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else if(status == 2)
                    {
                        Errores.AgregarError("La dimension del arreglo  : " +ctx.i.getText() +" no es valido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 3)
                    {
                        Errores.AgregarError("Indice fuera de rango del arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 4)
                    {
                        Errores.AgregarError("Tipos  de valores incorrectos : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 5)
                    {
                        Errores.AgregarError("Error al asignar el arreglo : " +ctx.i.getText() ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    else  if(status == 6)
                    {
                        Errores.AgregarError("Error al asignar el arreglo : " +ctx.i.getText() +" arreglo sin tamaño definido" ,ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                        return  null;
                    }
                    return null;
                }
                else
                {
                    ent.AsignarArraySBD(ctx.i.getText(), (ArrayS) val,ent.nombre,String.valueOf(ctx.i.getLine())
                            ,String.valueOf(ctx.i.getCharPositionInLine())) ;
                    return  true;
                }
            }
            Errores.AgregarError("Error al asignar la variable: " +ctx.i.getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return null;
        }
        catch (Exception e)
        {
            Errores.AgregarError("Ocurrio un error al asignar la variable: " +ctx.i.getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }

    /**
     * TIPO DE DATOS QUE MANEJA FORTRAN
     * */
    // -------------- REALIZADO ------------
    public Type visitType(JGrammarParser.TipoContext ctx)
    {
        String tipo = ctx.getText().toLowerCase();
        switch (tipo)
        {
            case "integer" : return Type.INTEGER;
            case "real" : return Type.REAL;
            case "complex" : return Type.COMPLEX;
            case "character" : return Type.CHAR;
            case "logical" : return Type.LOGICAL;
            default: Errores.AgregarError("Error al declarar tipo: " + ctx.getText(),"global",String.valueOf(ctx.getStart().getLine()),String.valueOf(ctx.getStart().getCharPositionInLine()));
        }

        return Type.NULL;
    }
   /**  DATOS PRIMITIVOS
    *   retorna un simbolo con las propiedades: linea, columna, id ="", valor, tipo, datoprimitivo
    * */

    public Symbol visitInteger(JGrammarParser.IntegerContext ctx)
    {
        return new Symbol(ctx.getStart().getLine(),ctx.getStart().getCharPositionInLine(),"",Integer.valueOf(ctx.getText()),Type.INTEGER, Dato.PRIMITIVO,-1);
    }

    public Symbol visitFloat(JGrammarParser.FloatContext ctx)
    {
        return new Symbol(ctx.getStart().getLine(),ctx.getStart().getCharPositionInLine(),"",Double.valueOf(ctx.getText()),Type.REAL,Dato.PRIMITIVO,-1);
    }

    public Symbol visitComplex(JGrammarParser.ComplexContext ctx)
    {
        return new Symbol(ctx.getStart().getLine(),ctx.getStart().getCharPositionInLine(),"",Float.valueOf(ctx.getText()),Type.INTEGER,Dato.PRIMITIVO,-1);
    }

    public Symbol visitChar(JGrammarParser.CharContext ctx)
    {
        return new Symbol(ctx.getStart().getLine(),ctx.getStart().getCharPositionInLine(),"",ctx.getText().charAt(1),Type.CHAR,Dato.PRIMITIVO,-1);
    }

    public Symbol visitLogico(JGrammarParser.LogicoContext ctx)
    {

        if(ctx.getText().equals(".true.")){
            return new Symbol(ctx.getStart().getLine(),ctx.getStart().getCharPositionInLine(),"",true,Type.LOGICAL,Dato.PRIMITIVO,-1);
        }else {

            return new Symbol(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), "", false, Type.LOGICAL, Dato.PRIMITIVO,-1);
        }
    }

    public Object visitVarExp(JGrammarParser.VarExpContext ctx)
    {
        Environment ent = stackEnv.peek();
        // verificar si es un objeto de tipo Simbolo
        Symbol id = ent.Buscar(ctx.ID().getText());
        if(id != null)
        {
            return  id;
        }
        // buscar si es un objeto de tipo arreglos
        ArrayS arreglo = ent.BuscarArrays(ctx.ID().getText());
        if(arreglo != null)
        {

            return  arreglo;
        }
        Errores.AgregarError("Variable no encontrado: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
        return null;
    }

    public String visitCad(JGrammarParser.CadContext ctx)
    {
        String str = ctx.getText();
        return str.substring(1, str.length() - 1);
    }

    public Symbol visitArrUniExp(JGrammarParser.ArrUniExpContext ctx)
    {
        Environment ent = stackEnv.peek();
        // obtener posicion
        Object posaux = visit(ctx.pos);
        if(posaux instanceof  Symbol)
        {
            Symbol id = ent.ObtenerValorArrayS(ctx.ID().getText(), (Integer) ((Symbol) posaux).getValue());
            if (id == null)
            {
                Errores.AgregarError("Error al acceder a arreglo: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                return null;
            }
            else
            {
                return  id;
            }
        }
        else
        {
            Errores.AgregarError("Indice no valido para el arreglo: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }
    public Symbol visitArrBiExp(JGrammarParser.ArrBiExpContext ctx)
    {
        Environment ent = stackEnv.peek();
        ArrayList<Integer> posiciones = new ArrayList<>();
        // obtener posicion
        try
        {
            for(ParseTree i: ctx.pos.children)
            {
                if(!(i instanceof TerminalNodeImpl))
                {
                    Symbol dimension = (Symbol) visit(i);
                    if(dimension.getType()== Type.INTEGER)
                    {
                        posiciones.add((int) dimension.getValue());
                    }
                    else
                    {
                        Errores.AgregarError("Dimension del arreglo: "+ctx.ID().getText()+" no valido",ent.nombre,String.valueOf(dimension.getLine()),String.valueOf(dimension.getColumn()));
                        return  null;
                    }
                }
            }
            if(posiciones.size() == 2)
            {
                int[] di = {posiciones.get(0),posiciones.get(1)};
                Symbol id = ent.ObtenerValorArraySBid(ctx.ID().getText(),di);
                if (id == null)
                {
                    Errores.AgregarError("Error al acceder a arreglo: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    return null;
                }
                else
                {
                    return  id;
                }
            }
            else
            {
                Errores.AgregarError("Dimension del arreglo: "+ctx.ID().getText()+" no valido",ent.nombre,String.valueOf(ctx.pos.getStart().getLine()),String.valueOf(ctx.pos.getStart().getCharPositionInLine()));
                return  null;
            }
        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al acceder a arreglo: " +ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return null;
        }
    }
    public Symbol visitParenExpr(JGrammarParser.ParenExprContext ctx)
    {
        Environment ent = stackEnv.peek();
        // obtener posicion
        try
        {
           Symbol expr = (Symbol) visit(ctx.expresion());
            if(expr != null)
            {
                return  expr;

            }
            else
            {
                Errores.AgregarError("Error al ejecutar expresion ( ):",ent.nombre,String.valueOf(ctx.PARDER().getSymbol().getLine()),String.valueOf(ctx.PARDER().getSymbol().getCharPositionInLine()));
                return  null;
            }
        }
        catch (Exception e)
        {
            Errores.AgregarError("Error al ejecutar expresion ( ):",ent.nombre,String.valueOf(ctx.PARDER().getSymbol().getLine()),String.valueOf(ctx.PARDER().getSymbol().getCharPositionInLine()));
            return null;
        }
    }
    //ID PARIZQ listaExpr PARDER
    public Object visitCallFunction(JGrammarParser.CallFunctionContext ctx)
    {
        Environment ent  = stackEnv.peek();
        try
        {
            // buscar la subrutina
            Symbol FuncSymbol = ent.Buscar(ctx.ID().getText() +Type.FUNCION.name());
            if(FuncSymbol != null)
            {

                Environment envFun = new Environment(ent,"FN_"+ctx.ID().getText());
                Funciones Fnejecutar = (Funciones) FuncSymbol.getValue();
                Fnejecutar.setEntorno(envFun);
                // Id variable a retornar
                String idRetornar = "";
                for(Symbol e : Fnejecutar.parametros)
                {
                    if(e.getDato() == Dato.RETORNO)
                    {
                        idRetornar = e.getId();
                        //Fnejecutar.parametros.remove(e);
                        break;
                    }
                }
                // INICIAR CON  CREAR UN NUEVO CONTEXTO
                //ent.siguiente = envFun;
                stackEnv.push(envFun);
                if(!nomEntorno.contains("FN_"+ctx.ID().getText()) )
                {
                    nomEntorno.add("FN_"+ctx.ID().getText());
                    stackEnvC3D.push(envFun);
                }
                // verificar los parametros
                // parametros -> contiene los ids de los parametros en la declaracion
                if(Fnejecutar.parametros.size()-1 == ctx.listaExpr().expresion().size() &&
                        Fnejecutar.parametros.size()-1 == Fnejecutar.lista_parametros.getChildCount())
                {

                    // asignar parametros
                    for(int i = 0; i<ctx.listaExpr().expresion().size();i++)
                    {

                        Symbol paramEnviado = Fnejecutar.parametros.get(i); // parametro enviad
                        // parametro a declarar
                        JGrammarParser.ParametrosContext auxnuevo = (JGrammarParser.ParametrosContext) Fnejecutar.lista_parametros.getChild(i);
                        String nuevo_nombre =  auxnuevo.var.getText();
                        Type nuevo_tipo  = visitType(auxnuevo.tipo());
                        // verificar si no es un arreglo
                        if(auxnuevo.listaExpr() != null)
                        {
                            // verificar si es el mismo nombre
                            if(paramEnviado.getId().toLowerCase().equals(nuevo_nombre.toLowerCase()))
                            {
                                // verificar que el tipo sea el mismo declarado y enviado
                                Object arrayAux = visit(ctx.listaExpr().expresion().get(i));
                                if(arrayAux instanceof  ArrayS  ){
                                    ArrayS paramAux = (ArrayS) arrayAux;
                                    if(nuevo_tipo == paramAux.getTypeData())
                                    {
                                        // verificar si es de dos  o una dimensiones
                                        int filas = -1;
                                        int columnas = -1;
                                        int dimension=0 ;
                                        for(JGrammarParser.ExpresionContext dim : auxnuevo.listaExpr().expresion()){
                                            dimension = dimension +1;
                                            Object dmActual = visit(dim);
                                            if(dmActual instanceof Symbol && ((Symbol) dmActual).getType() == Type.INTEGER)
                                            {
                                                filas = (filas == -1)? (int)(((Symbol) dmActual).getValue()):filas;
                                                columnas = (filas != -1)? (int)(((Symbol) dmActual).getValue()):-1;
                                            }
                                            else
                                            {
                                                Errores.AgregarError("Error al asignar arreglo, dimensiones no validas: "+ nuevo_nombre,
                                                        ent.nombre,String.valueOf(dim.getStart().getLine()),String.valueOf(dim.getStart().getCharPositionInLine()));

                                            }

                                        }
                                        if(dimension == paramAux.getDim() && dimension == 1)
                                        {
                                            // comprobar el tamaño
                                            if(filas <=paramAux.getSize1() )
                                            {
                                                ArrayList<Symbol> newArray2 = new ArrayList<>();
                                                ArrayList<Symbol> aux = (ArrayList<Symbol>) paramAux.getValue();
                                                for(int k=0;k<filas;k++)
                                                {
                                                    newArray2.add(aux.get(k));
                                                }
                                                int posicion  = envFun.ul_pos;
                                                envFun.ul_pos ++;
                                                ArrayS nuevo = new ArrayS(auxnuevo.ID().getSymbol().getLine(),auxnuevo.ID().getSymbol().getCharPositionInLine()
                                                        ,paramEnviado.getId(),newArray2,paramAux.getType(),paramAux.getTypeData(),filas,0,1,false,false,posicion);
                                                envFun.nuevoArrayS(paramEnviado.getId(),nuevo);

                                                RepSimbolos.AgregarTSimbolo("Array",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                                        ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                                        nuevo_tipo.name(),envFun.nombre);

                                            }
                                            else
                                            {
                                                Errores.AgregarError("Error al asignar arreglo, indice fuera de rango: "+ auxnuevo.ID().getText(),
                                                        ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                            }
                                        }// dimension  2
                                        else if(dimension == paramAux.getDim() && dimension == 2)
                                        {
                                            // comprobar el tamaño
                                            if(filas <=paramAux.getSize1() && columnas <= paramAux.getSize2() )
                                            {
                                                ArrayList<ArrayList<Symbol>> auxiliar = (ArrayList<ArrayList<Symbol>>) paramAux.getValue();

                                                ArrayList<ArrayList<Symbol>> newArray = new ArrayList<>();
                                                for(int k=0;k<filas;k++)
                                                {
                                                    ArrayList<Symbol> newArray2 = new ArrayList<>();
                                                    for(int c = 0; c<columnas;c++)
                                                    {
                                                        newArray2.add(auxiliar.get(k).get(c));
                                                    }
                                                    newArray.add(newArray2);
                                                }
                                                int posicion  = envFun.ul_pos;
                                                envFun.ul_pos ++;
                                                ArrayS nuevo = new ArrayS(auxnuevo.ID().getSymbol().getLine(),auxnuevo.ID().getSymbol().getCharPositionInLine()
                                                        ,paramEnviado.getId(),newArray,paramAux.getType(),paramAux.getTypeData(),filas,columnas,2,false,false,posicion);
                                                envFun.nuevoArrayS(paramEnviado.getId(),nuevo);

                                                RepSimbolos.AgregarTSimbolo("Array",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                                        ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                                        nuevo_tipo.name(),envFun.nombre);

                                            }
                                            else
                                            {
                                                Errores.AgregarError("Error al asignar arreglo, indice fuera de rango: "+ auxnuevo.ID().getText(),
                                                        ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                            }

                                        }
                                        else
                                        {
                                            Errores.AgregarError("Error,  no coinciden las dimensiones del arreglo: "+ auxnuevo.ID().getText(),
                                                    ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                        }

                                    }
                                    else
                                    {
                                        Errores.AgregarError("Error, Tipos de datos no coinciden: "+ auxnuevo.tipo().getText(),ent.nombre,String.valueOf(auxnuevo.tipo().getStart().getLine()),String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()));
                                    }

                                }
                                else
                                {
                                    Errores.AgregarError("Error, Parametro no es  un arreglo: "+ auxnuevo.ID().getText(),
                                            ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                                }

                            }
                            else
                            {
                                Errores.AgregarError("Error, nombre de parametros no coinciden: "+ auxnuevo.ID().getText(),ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                            }


                        }//  simbolo
                        else
                        {
                            // verificar si es el mismo nombre
                            if(paramEnviado.getId().toLowerCase().equals(nuevo_nombre.toLowerCase()))
                            {
                                // verificar que el tipo sea el mismo declarado y enviado
                                Symbol paramAux = (Symbol) visit(ctx.listaExpr().expresion().get(i));
                                if(nuevo_tipo == paramAux.getType())
                                {
                                    paramEnviado.setValue(paramAux.getValue());
                                    paramEnviado.setType(paramAux.getType());
                                    paramEnviado.setLine(auxnuevo.var.getLine());
                                    paramEnviado.setColumn(auxnuevo.var.getCharPositionInLine());
                                    paramEnviado.setDato(Dato.PRIMITIVO);
                                    int posicion  = envFun.ul_pos;
                                    envFun.ul_pos ++;
                                    paramEnviado.setPos(posicion);
                                    envFun.nuevoSimbolo(paramEnviado.getId(),paramEnviado);
                                    RepSimbolos.AgregarTSimbolo("Primitivo",paramEnviado.getId(),String.valueOf(auxnuevo.tipo().getStart().getLine())
                                            ,String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()),
                                            nuevo_tipo.name(),envFun.nombre);
                                }
                                else
                                {
                                    Errores.AgregarError("Error, Tipos no coinciden: "+ auxnuevo.tipo().getText(),ent.nombre,String.valueOf(auxnuevo.tipo().getStart().getLine()),String.valueOf(auxnuevo.tipo().getStart().getCharPositionInLine()));
                                }

                            }
                            else
                            {
                                Errores.AgregarError("Error, nombre de parametros no coinciden: "+ auxnuevo.ID().getText(),ent.nombre,String.valueOf(auxnuevo.ID().getSymbol().getLine()),String.valueOf(auxnuevo.ID().getSymbol().getCharPositionInLine()));
                            }
                        }

                    }

                    visitLinstrucciones((JGrammarParser.LinstruccionesContext) Fnejecutar.lista_instrucciones);
                    Object ret = envFun.BuscarArrays(idRetornar);
                    if(ret != null)
                    {
                        stackEnv.pop();
                        return (ArrayS)ret;
                    }
                    ret = envFun.Buscar(idRetornar);
                    if(ret!=null)
                    {
                        stackEnv.pop();
                        return (Symbol)ret;
                    }
                    else
                    {
                        Errores.AgregarError("Error en funcion, valor de retorno no econtrado : "+ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                    }
                    stackEnv.pop();
                    //System.out.println("entrando a sub rutina");
                }
                else
                {
                    Errores.AgregarError("Error al llamar funcion: "+ctx.ID().getText() +" Cantidad de parametros incorrectos",ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                }

                return null;
            }
            else
            {
                Errores.AgregarError("Error, funcion no encontrado: "+ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
                return  null;
            }
        }
        catch (Exception e )
        {
            System.out.println(e);
            Errores.AgregarError("Error al ejecutar la funcion: "+ctx.ID().getText(),ent.nombre,String.valueOf(ctx.ID().getSymbol().getLine()),String.valueOf(ctx.ID().getSymbol().getCharPositionInLine()));
            return  null;
        }

    }

    // metodo para verificar los entornos agregados
    public boolean verificarEntorno(Environment ent)
    {
        for(Environment s: stackEnvC3D)
        {
            if(s.equals(ent))
                return  true;
        }
        return  false;
    }


}

