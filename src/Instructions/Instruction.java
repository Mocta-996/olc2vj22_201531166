package Instructions;
import Entorno.*;

public abstract class Instruction {
    public int row;
    public int column;

    public Instruction(int r, int c) {
        this.row = row;
        this.column = column;
    }
    public abstract Object execute(Environment table);
}
