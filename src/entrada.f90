subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n , m )

  integer :: i, j

do i = 1 , 7 , 1
    do j = 1 , 7 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            a [ i , j ] = 1
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            a [ i , j ] = 2
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            a [ i , j ] = 3
       end if
    end do
end do


  print*, a
  print*, n, " SUBRUTINA ", m
end subroutine print_matrix



program myProgram
implicit none

integer :: i, j
integer :: arr ( 6 , 6 )
do i = 1 , 7 , 1
    do j = 1 , 7 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            arr [ i , j ] = 4
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            arr [ i , j ] = 5
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            arr [ i , j ] = 6
       end if
    end do
end do

print *, arr
call print_matrix ( 6 , 6 , arr )
print *, arr

end program myProgram


!===================================  ORDENA EL ARREGLO =============================================
program compi
    implicit none
    integer :: i, j
    integer, dimension(10) :: array
    integer, dimension(10) :: array2
    array = (/2,5,7,6,8,1,9,6,8,3/)
    array2 = array
    print *, array
    print *, array2
end program compi
===========================================================================================0
!prueba funcion
function vector_norm(n) result(array)
  implicit none
  integer, intent(in) :: n
  integer, dimension(10) :: array
  array = (/2,5,7,6,8,1,9,6,8,3/)
  print*, 'Fin de Bloque: '', n
end function vector_norm

program run_fcn
  implicit none
  print *, 'Vector retorno = ', vector_norm(1)
end program run_fcn

program run_fcn
  implicit none
  integer, dimension(10) :: array
  array = (/2,5,7,6,8,1,9,6,8,3/)
  print*, 'Fin de Bloque: '', n
end program run_fcn

!========================================= ENTRADA BASICO DE FUNCIONES

function vector_norm(n,vec) result(norm)
  implicit none
  integer, intent(in) :: n
  real, intent(in) :: vec(n)
  real :: norm
  print*, 'INGRESANDO A LA FUNCION vector_norm'
  norm = 10
  print*, 'valor de retorno: ', norm
end function vector_norm

program run_fcn
  implicit none
  real :: v
  real :: vector_norm
  print*, 'hola'
end program run_fcn

=============================================

subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n , m )
  print*,'INICION DE SUBRUTINA'
  integer :: i, j
  print*, a
  print*, n, " SUBRUTINA ", m
  print*,'FIN DE SUBRUTINA'
end subroutine print_matrix

program myProgram
implicit none
print *, 'BLOQUE PRINCIPAL'
integer :: i, j
integer :: arr ( 6 , 6 )
print *, arr
call print_matrix ( 6 , 6 , arr )
end program myProgram


==========================
program h
implicit none
integer :: a =3+2*2,b
b=3
if(a>3 .and. a<=10) then
print *, a+3
    if(b==3) then
    print *, 6
    end if
else if(a<=3 ) then
    print *, 7
else if(a<=4) then
    print *, 8
else if(a<=5) then
    print *, 9
end if
end program h





-------------------
program Main
 implicit none
 integer :: i
 do i =1,10,1
    if(i == 5) then
        exit
    end if
    print*,i
 end do
end program Main
----------------

function fun(v, w, num) result(mat)
implicit none
integer, intent(in):: v
integer, intent(in):: w
integer, intent(in):: num
integer :: mat(v,w)
integer :: i = 0, j = 0

do i = 1, v + 1, 1
do j = 1, w + 1, 1

	mat[i,j] = num
end do
end do

end function fun

program prog
implicit none

integer :: a = 10, b = 10
integer, dimension (a,b) :: matriz, matriz2
matriz = fun(a,b, 8)
matriz2 = matriz
print *, matriz, 'c', matriz2
matriz2 = fun(a, b, 5)
print *, 'c', matriz, 'c', matriz2

end program prog

*********** salida
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
'c'
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}

'c'
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
{8, 8, 8, 8, 8, 8, 8, 8, 8, 8}
'c'
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}
{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}

------------------------------
program h
implicit none
integer :: a =3+2*2, b , c
print *,'valor original de a  = 7: c3d: ', a
print *,'valor original de b  = 0: c3d: ', b
print *,'valor original de c  = 0: c3d: ', c
b=2
c = 3.5
print *,'esto es una suma  = 12.5: c3d :', a +b +c +200
print *,'esto es una operacion unaria  = -7 : c3d :', -a
end program h

=========================================
program run_fcn
                    implicit none
                    real, dimension(2) :: numbers
                    ! solamente para una dimensión es como sigue.
                    numbers = (/1.5, 3.2,4.5,0.9,7.2 /)
                    print*,"numbers: ",numbers
                    ! declaración de arreglos
                    integer, allocatable :: array1(:)
                    integer, allocatable :: array2(:,:)

                    ! se asigna tamaño para el arreglo
                    allocate(array1(10))
                    ! LLENO ARRAY 1
                    integer::W=1
                    do while (w < 5)
                        array1[W] = 8
                        print*,"(",w,")"," = ",array1[W]
                        w= w  + 1
                    end do
                    print*, "TAMANO 1: " ,size(array1)

                    allocate(array2(2,4))
                    ! LLENO ARRAY 2
                    integer :: i=1
                    integer::j=1
                    do while (i < 3)
                        j=1
                        do while (j < 5)
                            array2[i,j] = 5.0
                            print*,"(",i,",",j,")"," = ",array2[i,j]
                            j = j  + 1
                        end do
                        i = i + 1
                    end do
                    print*, "TAMANO 2: ", size(array2)

                    print*, "array1:", array1
                    print*, "array2:", array2
                    ! se desasigna el arreglo para utilizarlo más adelante con otro tamaño
                    deallocate(array1)
                    deallocate(array2)
                    print*,"----------------------------------------------------"
                    allocate(ARRAY1(20))
                    ARRAY1[15]=5
                    print*, ARRAY1[15]
                    print*, "TAMANO 1 segunda vuelta: ", size(ARRAY1)
                    allocate(ARRAY2(20,20))
                    ARRAY2[15,15]=5
                    print*, ARRAY2[15,15]
                    print*, "TAMANO 2 segunda vuelta: ", size(ARRAY2)
                	print*,'hola',"hola"
                    print*, "array1:  ", array1
                    print*, "array2:  ", array2
end program run_fcn

---------------------------------------- ARREGLOS Y SUB RUTINAS -----------------------------------------
subroutine print_matrix ( n , m , a )
  implicit none
  integer, intent(in) :: n
  integer, intent(in) :: m
  integer, intent (in) :: a ( n , m )
  integer :: i, j
do i = 1 , 6 , 1
    do j = 1 , 6 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            a [ i , j ] = 1
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            a [ i , j ] = 2
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            a [ i , j ] = 3
       end if
    end do
end do
  print*, a
  print*, n, " SUBRUTINA ", m
end subroutine print_matrix

program myProgram
implicit none
integer :: i, j
integer :: arr ( 6 , 6 )
do i = 1 , 6 , 1
    do j = 1 , 6 , 1
        if ( i .eq. 4 .or. j .eq. 4 ) then
            arr [ i , j ] = 4
        else if ( i .eq. 5 .or. j .eq. 5 ) then
            arr [ i , j ] = 5
        else if ( i .eq. 6 .or. j .eq. 6 ) then
            arr [ i , j ] = 6
       end if
    end do
end do
print *, arr
call print_matrix ( 6 , 6 , arr )
print *, arr
end program myProgram


------------------------------- CICLO WHILE ------------------------------
program Main
implicit none
    integer :: i
    integer :: j
    i = 1
    j = 5
    do while (i < j)
        print *, i
        i = i + 1
    end do
end program Main

program sum
    integer :: i, j
    logical :: a = .false.
    j = 5
    i = 1
        do while (.not. a)
            print *, i
            i = i + 1
            if(i == 10) then
                exit
            end if
        end do
    print *, 'ciclo do while'
        do while (i < j)
                print *, i
                i = i + 1
        end do
end program sum


program compi
    implicit none
    integer :: i , j
    integer, dimension( 10 ) :: array
    array = (/ 2 , 5 , 7 , 6 , 8 , 1 , 9 , 6 , 8 , 3 /)

    do i = 1, size ( array ) - 1 , 1
        do j = i + 1, size ( array ) , 1
            if (array[ i ] > array [ j ] ) then
                integer :: aux = array [ i ]
                array [ i ] = array [ j ]
                array [ j ] = aux
            end if
        end do
    end do
    do i = 1, size (array)
        print*, array [ i ]
    end do
end program compi



function suma(a,b) result(total)
  implicit none
  integer, intent(in) :: a
  integer, intent(in) :: b
  integer :: total
  print*, 'accediento a la funcion suma'
  total = a +b
end function suma

program run_fcn
    implicit none
    integer :: i
    integer :: j
    i = 10
    j = 20
    print *, 'el total de la suma es', suma(i,j)
end program run_fcn

int VAR;
for (VAR = 0; VAR<10; VAR++ ) {
    printf("%c",10);
    printf("%d", (int) STACK[(int)VAR]);
    printf("%c",10);
}