package Error;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static Main.Global.*;

public class Errores {
    public String getDescripcion() {
        return descripcion;
    }

    public String getAmbito() {
        return ambito;
    }

    public String getLinea() {
        return linea;
    }

    public String getColumna() {
        return columna;
    }

    public String getFecha() {
        return fecha;
    }

    private  String descripcion;
    private String ambito;
    private String linea;
    private String columna;
    private String fecha;

    /*public Errores(String s, String global, String s1, String s2, String fecha) {
    }*/

    public Errores(String descripcion, String ambito , String linea, String columna, String fecha){
        super();
        this.descripcion  =descripcion;
        this.ambito    =ambito;
        this.linea   = linea;
        this.columna = columna;
        this.fecha =fecha;
    }

    public static  void AgregarError(String descripcion, String ambito , String linea, String columna){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        Errores addError = new Errores(descripcion,ambito,linea,columna,dtf.format(LocalDateTime.now()));
        _errores.add(addError);

    }

    public static  void AgregarErrorLexico(String descripcion, String ambito , String linea, String columna){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        Errores addError = new Errores(descripcion,ambito,linea,columna,dtf.format(LocalDateTime.now()));
        _erroresLexicos.add(addError);

    }
    public static  void AgregarErrorSintactico(String descripcion, String ambito , String linea, String columna){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        Errores addError = new Errores(descripcion,ambito,linea,columna,dtf.format(LocalDateTime.now()));
        _erroresSintacticos.add(addError);

    }
}
