grammar JGrammar;

// Tokens
PUNTO:          '.';
PTCOMA:         ';';
PARIZQ:         '(';
PARDER:         ')';
LLAVEIZQ:       '{';
LLAVEDER:       '}';
COMA:           ',';
CORIZQ:         '[';
CORDER:         ']';
KLEENE:         '?';
IGUAL:          '=';
DOSPUNTOS:      ':';
EXCLA:          '!';

//  PALABRAS RESERVADAS
TRUE:           'true';
FALSE:          'false';
PRINT:          'println!';
POW:            'pow';
POWF:           'powf';
WHILE:          'while';


// OPERACIONES ARITMETICAS
MODULO:         '%';
POR:            '*';
DIVISION:       '/';
SUMA:           '+';
RESTA:          '-';
// OPERACIONES RELACIONALES
IGUALACION:     '==';
DIFERENCIACION:  '!=';
MENORQUE:       '<';
MAYORQUE:       '>';
MENORIGUAL:     '<=';
MAYORIGUAL:     '>=';

// OPREACIONES LOGICAS
AND:            '&&';
OR:             '||';
NOT:            N O T;

// TIPOS DE DATOS
RENTERO:        I N T;
RFLOAT:         'float';
RBOOL:          'bool';
RSTRING:        'string';
RSTR:           '&str';
RUSIZE:         'usize';
RCHAR:          'char';


// FUNCIONES NATIVAS
RABS:           'abs()';
RSQRT:          'sqrt()';
RTOSTRING:      'to_string()';
RTOOWNED:       'to_owned()';
RCLONE:         'clone()';

// PALABRAS RESERVADAS  DECLARACION
LET:            'let';
MUT:            'mut';
AS:             'as';
IF:             'if';
ELSE:           'else';
MATCH:          'match';

// primitivos
INTEGER:        [0-9]+;
FLOAT:          ('0'..'9')+ ('.' ('0'..'9')+)?;
STRING:         '"' (~["\r\n] | '""')* '"' ;
CHAR:           '\'' .*? '\'' ;
ID:             [a-zA-Z_] [a-zA-Z0-9_]*;

// ADICIONALES
WHITESPACE:             [ \\\r\n\t]+ -> skip;   //ESPACIOS EN BLANCO Y SALTOS DE LINEA
//COMENTARIO_BLOQUE :     '/*' .*? '*/' -> skip;  // comentario multilinea
COMENTARIO_LINEA :      '//' ~[\r\n]* -> skip;  // comentario linea


fragment
ESC_SEQ
    :   '\\' ('\\'|'@'|'['|']'|'.'|'#'|'+'|'-'|'!'|':'|' ')
    ;
// CASE INSENSITIVE
fragment A:('a'|'A');
fragment B:('b'|'B');
fragment C:('c'|'C');
fragment D:('d'|'D');
fragment E:('e'|'E');
fragment F:('f'|'F');
fragment G:('g'|'G');
fragment H:('h'|'H');
fragment I:('i'|'I');
fragment J:('j'|'J');
fragment K:('k'|'K');
fragment L:('l'|'L');
fragment M:('m'|'M');
fragment N:('n'|'N');
fragment O:('o'|'O');
fragment P:('p'|'P');
fragment Q:('q'|'Q');
fragment R:('r'|'R');
fragment S:('s'|'S');
fragment T:('t'|'T');
fragment U:('u'|'U');
fragment V:('v'|'V');
fragment W:('w'|'W');
fragment X:('x'|'X');
fragment Y:('y'|'Y');
fragment Z:('z'|'Z');




start : linstrucciones EOF;

linstrucciones : (instrucciones)*
        ;

instrucciones : block #blck
        | declaration #decl
        ;

block : LLAVEIZQ linstrucciones LLAVEDER ;

declaration : tipo ID IGUAL expr PTCOMA ;

tipo :RENTERO
    | RSTRING
    ;

expr : left=expr op=(POR|DIVISION) right=expr #opExpr
   | left=expr op=(SUMA|RESTA) right=expr  #opExpr
   | PARIZQ expr PARDER                       #parenExpr
   | atom=INTEGER                           #atomExpr
   | str=STRING                         #strExpr
   ;